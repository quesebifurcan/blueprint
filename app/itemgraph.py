import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.group import Group


class TreeWidgetItem(QTreeWidgetItem):

    def __init__(self, parent=None):
        super(TreeWidgetItem, self).__init__(parent)
        self.item = None


class ItemGraph(QTreeWidget):

    def __init__(self, parent=None):
        super(ItemGraph, self).__init__(parent)
        self.scene = None
        self.setIndentation(15)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        font = self.font()
        font.setPointSize(11)
        self.setFont(font)
        self.setAlternatingRowColors(True)

        self.insert_root_item()
        self.itemSelectionChanged.connect(self.update_scene_selection)
        self.groups_disabled = True
    
    def set_groups_disabled(self, value):
        self.groups_disabled = value

    def insert_root_item(self):
        self.root_item = QTreeWidgetItem()
        self.root_item.setFlags(Qt.ItemIsEnabled)
        self.insertTopLevelItem(0, self.root_item)
        self.root_item.setText(0, "Scene")
        self.root_item.item = self.scene

    def update_graph(self):
        self.blockSignals(True)
        self.takeTopLevelItem(0)
        self.insert_root_item()
        self.add_items()
        self.update_graph_selection()
        self.blockSignals(False)
        
    def add_items(self):
        items = self.scene.items()
        top_level_items = []
        for i in items:
            if not i.parentItem() and isinstance(i, self.scene.item_classes):
                if isinstance(i, QGraphicsItemGroup) and not i.childItems():
                    pass
                else:
                    top_level_items.append(i)
        top_level_items.sort(key=lambda item: item.pos().x())
        for item in top_level_items:
            self.insert_item(self.root_item, item)
        self.expandAll()

    def insert_item(self, parent, item):
        new = QTreeWidgetItem([item.__class__.__name__])
        new.item = item
        parent.addChild(new)
        if item.childItems():
            for child in item.childItems():
                self.insert_item(new, child)

    def update_graph_selection(self):
        sel = self.scene.all_selected_items()
        it = QTreeWidgetItemIterator(self)
        while it.value():
            item = it.value()
            scene_item = item.item
            if hasattr(scene_item, 'isSelected'):
                if scene_item in sel:
                    item.setSelected(True)
                else:
                    item.setSelected(False)
            it += 1

    def update_scene_selection(self):
        sel = [item.item for item in self.selectedItems()]
        for item in self.scene.items():
            if item in sel:
                item.setSelected(True)
            else:
                item.setSelected(False)


        

        

    
