import sys
import time

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class StatusBar(QStatusBar):

    def __init__(self, parent=None):
        super(StatusBar, self).__init__(parent)
        self.showMessage("...testing")

