import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene import Scene
from view import View
from dockwidget import DockWidget
from statusbar import StatusBar
from mousemodeselectormenu import MouseModeSelectorMenu
from properties_editor.properties_editor_widget import PropertiesEditorWidget
from itemselectionfilter import ItemSelectionFilter
from itemgraph import ItemGraph
from undoview import UndoView
from pageview import PageView

from scene_items.line import HorizontalLine
from scene_items.ramp import Ramp
from scene_items.rectitem import RectItem
from scene_items.textitem import TextItem, TextItemAnchor
from scene_items.timetext import TimeText, TimeTextAnchor
from scene_items.pathitem import PathItem
from scene_items.grid import Grid

from commands.set_state_command import SetStateCommand
from scene_states.insert_item_state import InsertItemState
from scene_states.insert_pathitem_state import InsertPathItemState


class MainWindow(QMainWindow):

    ### INITIALIZERS ###

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.view = View()
        self.pageview = PageView()

        self.scene = Scene(self)
        self.scene.view = self.view
        self.pageview.scene = self.scene
        self.scene.pageview = self.pageview

        self.tabWidget = QTabWidget()
        self.tabWidget.addTab(self.view, 'Scene View')
        self.tabWidget.addTab(self.pageview, 'Page View')
        self.tabWidget.setTabShape(QTabWidget.Triangular)
        self.setCentralWidget(self.tabWidget)

        self.view.setScene(self.scene)
        self.view.setRenderHint(QPainter.Antialiasing)
        self.view.setRenderHint(QPainter.TextAntialiasing)

        for page in self.pageview.pages:
            page.setScene(self.scene)

        self.itemGraph = ItemGraph()
        self.itemGraph.scene = self.scene
        self.itemGraphDock = DockWidget("Item Graph")
        self.itemGraphDock.setObjectName("Item Graph")
        self.itemGraphDock.setWidget(self.itemGraph)
        self.addDockWidget(Qt.RightDockWidgetArea, self.itemGraphDock)

        self.selectionFilter = ItemSelectionFilter()
        self.selectionFilter.scene = self.scene
        self.selectionFilterDock = DockWidget("Selection Filter")
        self.selectionFilterDock.setObjectName("Selection Filter")
        self.selectionFilterDock.setWidget(self.selectionFilter)
        self.addDockWidget(Qt.RightDockWidgetArea, self.selectionFilterDock)

        self.propertiesEditorWidget = PropertiesEditorWidget(scene=self.scene)
        self.propertiesEditorDock = DockWidget("Properties Editor")
        self.propertiesEditorDock.setObjectName("Properties Editor")
        self.propertiesEditorDock.setWidget(self.propertiesEditorWidget)
        self.addDockWidget(Qt.RightDockWidgetArea, self.propertiesEditorDock)

        self.dockWindows = (self.itemGraphDock,
                            self.selectionFilterDock,
                            self.propertiesEditorDock)

        self.setStatusBar(StatusBar())

        self.undoStack = QUndoStack()
        self.scene.undoStack = QUndoStack()
        self.undoView = UndoView(self.undoStack)
        self.setContextMenuPolicy(Qt.CustomContextMenu)

        ### LAYOUT ###
        self.is_full_screen = False
        self.view.setContentsMargins(20, 20, 20, 20)
        self.pageview.setContentsMargins(20, 20, 20, 20)
        self.set_printable_scene_rect(0, 0, 8000, 1000)

        # initialize selectionFilter
        self.selectionFilter.setEnabled(False)
        self.menu = MouseModeSelectorMenu(scene=self.scene)
        self.init_menu_bar()
        self.init_context_menu_actions()

        ### SIGNALS ### 
        # TODO: update selectively
        self.undoStack.indexChanged.connect(self.scene.update)
        self.customContextMenuRequested.connect(self.show_mouse_mode_selector)
        self.scene.selectionChanged.connect(self.itemGraph.update_graph)
        self.scene.selectionChanged.connect(self.selectionFilter.update_filter)
        self.scene.selectionChanged.connect(
            self.propertiesEditorWidget.update_selection)

        self.undoView.show()

    def createAction(self, text, slot=None, shortcut=None, tip=None):
        action = QAction(text, self)
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        action.triggered.connect(slot)
        return action

    def create_view_action(self, description, window, menu):
        action = QAction(description, self)
        action.toggled.connect(
            lambda value: self.show_window(value, window))
        action.setCheckable(True)
        action.setChecked(True)
        menu.addAction(action)

    def init_menu_bar(self):

        ### MENUS ### 
        file_menu = self.menuBar().addMenu('&File')
        edit_menu = self.menuBar().addMenu('&Edit')
        view_menu = self.menuBar().addMenu('&View')
        tools_menu = self.menuBar().addMenu('&Tools')
        help_menu = self.menuBar().addMenu('&Help')

        new = QAction('&New', self)
        new.setShortcut('Ctrl+N')
        file_menu.addAction(new)

        descriptions = ('Properties Editor',
                        'Item Graph',
                        'Item Selection Filter',
                        'Undo/Redo')
        windows = (self.propertiesEditorDock,
                   self.itemGraphDock,
                   self.selectionFilterDock,
                   self.undoView)
        for description, window in zip(descriptions, windows):
            self.create_view_action(description, window, view_menu)

        open_file = self.createAction(
            "Open...",
            self.show_load_dialog,
            tip="Open File....")
        open_file.setShortcut('Ctrl+O')
        file_menu.addAction(open_file)

        save = QAction('&Save', self)
        save.setShortcut('Ctrl+S')
        file_menu.addAction(save)

        save_as = self.createAction(
            "Save as...",
            self.show_save_dialog,
            tip="Save Document as...")
        save_as.setShortcut('Ctrl+Shift+S')
        file_menu.addAction(save_as) 

        add_grid = QAction('&Add Grid', self)
        add_grid.triggered.connect(self.add_grid)
        edit_menu.addAction(add_grid)

        remove_grid_menu = QMenu('Remove Grid', self)
        remove_grid_menu.aboutToShow.connect(self.build_remove_grid_menu)
        edit_menu.addMenu(remove_grid_menu)

        tools_menu.addMenu('&Set Mouse Mode...')

        save_selection = QAction('&Save selection as Custom Item', self)
        save_selection.triggered.connect(self.save_selection)
        tools_menu.addAction(save_selection)

        run_script = QAction('&Run Script...', self)
        run_script.triggered.connect(self.run_script)
        tools_menu.addAction(run_script)

    def trigger_insert_item(self, item, insert_state=InsertItemState):
        state = lambda scene: insert_state(scene=scene, item=item)
        self.scene.state = state

    def init_context_menu_actions(self):
        self.menu.addAction(QAction("Zoom in", self))
        self.menu.addAction(QAction("Zoom out", self))
        self.menu.addSeparator()
        self.menu.addAction(QAction("Delete", self))
        copy_action = self.createAction(
            "Copy",
            self.scene.copy_selection,
            tip="Copy selected items")
        self.menu.addAction(copy_action)
        self.menu.addSeparator()
        self.menu.insert_submenu = self.menu.addMenu("Insert...")

        insert_line_action = self.createAction(
            "Insert Line",
            lambda: self.trigger_insert_item(HorizontalLine),
            tip="Insert new Line at cursor position")
        self.menu.insert_submenu.addAction(insert_line_action)

        insert_ramp_action = self.createAction(
            "Insert Ramp",
            lambda: self.trigger_insert_item(Ramp),
            tip="Insert new Ramp at cursor position")
        self.menu.insert_submenu.addAction(insert_ramp_action)

        insert_rect_action = self.createAction(
            "Insert Rectangle",
            lambda: self.trigger_insert_item(RectItem),
            tip="Insert new Rectangle at cursor position")
        self.menu.insert_submenu.addAction(insert_rect_action)

        insert_text_action = self.createAction(
            "Insert Text",
            lambda: self.trigger_insert_item(TextItemAnchor),
            tip="Insert new Text at cursor position")
        self.menu.insert_submenu.addAction(insert_text_action)

        insert_timetext_action = self.createAction(
            "Insert Timetext",
            lambda: self.trigger_insert_item(TimeTextAnchor),
            tip="Insert new Timetext at cursor position")
        self.menu.insert_submenu.addAction(insert_timetext_action)

        insert_pathitem_action = self.createAction(
            "Insert Pathitem",
            lambda: self.trigger_insert_item(PathItem, InsertPathItemState),
            tip="Insert new Pathitem at cursor position")
        self.menu.insert_submenu.addAction(insert_pathitem_action)

    def build_remove_grid_menu(self):
        for i, grid in enumerate(self.scene.grids):
            remove_grid = QAction('&#1', self)
            # remove_grid.triggered.connect(...)
            remove_grid_menu.addAction(remove_grid)

    ### PUBLIC METHODS ###

    def set_printable_scene_rect(self, x=0, y=0, width=8000, height=1000):
        self.scene.setSceneRect(x, y, width, height)
        self.view.setSceneRect(-20, -30, width+40, height+40)
        self.scene.scene_border.draw_lines()

    def show_window(self, value, window):
        if value:
            window.show()
        else:
            window.hide()

    def add_grid(self):
        grid = Grid()
        self.scene.addGrid(grid)
        self.propertiesEditorWidget.add_grid(grid)

    def save_selection(self):
        # TODO
        pass

    def run_script(self):
        import utils
        start_path = '/Users/fredrik/repos/timeline/experiments/script_tests/'
        filename = str(QFileDialog.getOpenFileName(self, 'Run Script', start_path))
        if filename.endswith('.py'):
            execfile(filename, {'scene': self.scene, 'utils': utils, 'hor': HorizontalLine})

    def save_settings(self, filename='settings.ini'):
        settings = QSettings(filename, QSettings.IniFormat)
        settings.setValue("geometry", self.saveGeometry());
        settings.setValue("windowState", self.saveState());
        return settings

    def restore_settings(self, filename='settings.ini'):
        if filename:
            settings = QSettings(filename, QSettings.IniFormat)
            self.restoreGeometry(settings.value("geometry").toByteArray())
            self.restoreState(settings.value("windowState").toByteArray())
            return settings

    def save_file(self, filename='data.json'):
        import json
        from save_and_load import SceneEncoder, GridEncoder
        with open(filename, 'w') as outfile:
            # TODO: dump settings here or somewhere else?
            # TODO: collect scene items and grid in a list
            json.dump(self.scene, outfile, cls=SceneEncoder, indent=2)
            json.dump(self.scene.grids, outfile, cls=GridEncoder, indent=2)

    def load_file(self, filename='data.json'):
        import json
        from save_and_load import Decoder
        with open(filename, 'r') as infile:
            items = json.load(infile, cls=Decoder)
        for item in items:
            self.scene.addItem(item)

    def show_save_dialog(self):
        import os
        filename = str(QFileDialog.getSaveFileName(
            self,
            'Save File',
            '/Users/fredrik/Desktop'))
        if filename:
            basename = os.path.splitext(filename)[0]
            filename = '.'.join([basename, 'json'])
            self.save_file(filename)

    def show_load_dialog(self):
        import os
        filename = str(QFileDialog.getOpenFileName(
            self,
            'Open File',
            '/Users/fredrik/Desktop'))
        if os.path.splitext(filename)[1] == '.json':
            self.load_file(filename)

    def show_mouse_mode_selector(self, pos):
        self.menu.exec_(self.mapToGlobal(pos))

    def hide_dock_windows(self):
        for dock in self.dockWindows:
            dock.hide()
        
    def show_dock_windows(self):
        for dock in self.dockWindows:
            dock.show()

    def toggle_fullscreen(self, e):
        if e.modifiers() == Qt.ControlModifier and e.key() == Qt.Key_F:
            if self.is_full_screen:
                self.setWindowState(Qt.WindowActive)
                self.is_full_screen = False
            else:
                self.setWindowState(Qt.WindowFullScreen)
                self.is_full_screen = True

    def open_repeat_dialog(self):
        from repeatdialog import RepeatDialog
        rep = RepeatDialog(scene=self.scene, parent=self)
        rep.scene = self.scene
        rep.exec_()

    def open_sequential_editor(self):
        print 'open sequential'
        from sequential_editors.sequential_editor import SequentialEditorDialog
        seq = SequentialEditorDialog(scene=self.scene, parent=self)
        seq.scene = self.scene
        seq.exec_()

    def open_transition_editor(self):
        from sequential_editors.transition_editor import TransitionEditorDialog
        trans = TransitionEditorDialog(scene=self.scene, parent=self)
        trans.exec_()

    ### OVERRIDDEN METHODS ###

    def keyPressEvent(self, e):
        from save_and_load import SceneEncoder
        import json
        print 'key pressed'
        self.toggle_fullscreen(e)
        if e.key() == Qt.Key_U:
            self.itemGraph.update_graph()
        elif e.key() == Qt.Key_J:
            self.pageview.render_to_image()
            self.pageview.export_pdf()
        elif e.key() == Qt.Key_R:
            self.open_repeat_dialog()
        elif e.key() == Qt.Key_E:
            print 'lkjsdf'
            self.open_sequential_editor()
        elif e.key() == Qt.Key_T:
            self.open_transition_editor()
        elif e.key() == Qt.Key_V:
            fn = QFileDialog.getSaveFileName(self, 'Save', '', filter='*.txt')
            fn = 'data' + '.txt'
            with open(fn, 'w') as outfile:
                json.dump(self.scene, outfile, cls=SceneEncoder, indent=4)
        super(MainWindow, self).keyPressEvent(e)

    def close(self):
        self.undoView.hide()
        super(MainWindow, self).close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    m = MainWindow()
    m.show()
    m.activateWindow()
    m.raise_()
    sys.exit(app.exec_())
