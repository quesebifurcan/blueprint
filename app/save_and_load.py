import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

###########################################################################
### ENCODERS
###########################################################################

import json
from collections import OrderedDict

from scene_items.group import Group
from scene_items.line import HorizontalLine, LineNode
from scene_items.textitem import TextItem
from scene_items.timetext import TimeText
from scene_items.ramp import Ramp, RampNode
from scene_items.rectitem import RectItem

from scene import item_classes

class Encoder(json.JSONEncoder):

    item_classes = item_classes()

    def default(self, obj):
        if hasattr(obj, 'properties'):
            if isinstance(obj, Group):
                if not obj.childItems():
                    return None
            d = OrderedDict()
            d['class'] = obj.__class__.__name__
            for p in obj.properties():
                if hasattr(self, p):
                    # convert non-trivial attributes
                    getter = getattr(self, p)
                    value = getter(obj)
                else:
                    value = getattr(obj, p)
                d[p] = value

            # scene has no childItems
            if obj.childItems() and not 'nodes' in obj.properties():
                d['child_items'] = self.child_items(obj)
            return d

    def scene_items(self, obj):
        pass

    def child_items(self, obj):
        coll = []
        for child in obj.childItems():
            curr = self.default(child)
            if curr:
                coll.append(curr)
        return coll

    def nodes(self, obj):
        coll = []
        for node in obj.nodes:
            coll.append(self.default(node))
        return coll

    def pos(self, obj):
        return (obj.pos().x(), obj.pos().y())
    
    def color(self, obj):
        return str(obj.color.name())

    def text(self, obj):
        return str(obj.text)

    def fontfamily(self, obj):
        return str(obj.fontfamily)
        


class SceneEncoder(Encoder):

    item_classes = item_classes()

    def default(self, obj):
        return self.get_top_level_scene_items(obj)

    def get_top_level_scene_items(self, obj):
        coll = []
        if obj.items():
            for item in obj.items():
                if not item.parentItem():
                    if isinstance(item, self.item_classes):
                        curr = Encoder.default(Encoder(), item)
                        coll.append(curr)

        return coll

class SettingsEncoder(Encoder):

    def default(self, obj):
        pass


class GridEncoder(Encoder):

    def default(self, obj):
        return self.decode_grids(obj)

    def decode_grids(self, obj):
        d = OrderedDict()
        d['class'] = obj.__class__.__name__
        for p in obj.properties():
            value = getattr(obj, p)
            d[p] = value
        return d


###########################################################################
### DECODERS
###########################################################################

from scene_items.line import HorizontalLine, LineNode
from scene_items.ramp import Ramp, RampNode


class Decoder(json.JSONDecoder):

    def decode(self, obj):

        if isinstance(obj, str):
            data = super(Decoder, self).decode(obj)
        else:
            data = obj

        if isinstance(data, list):
            coll = []
            for elt in data:
                coll.append(self.decode(elt))
            return coll

        else:

            cls = globals()[data.pop('class')]

            if 'nodes' in data.keys():
                nodes = [self.decode(node) for node in (data.pop('nodes'))]
                instance = cls(nodes=nodes)
            else:
                instance = cls()

            for p in data.keys():
                if hasattr(self, p):
                    setter = getattr(self, p)
                    setter(instance, data[p])
                else:
                    setattr(instance, p, data[p])

        # from group import Group

        # if isinstance(instance, Group):
        #     self.child_items(instance, data['child_items'])

            return instance

    def color(self, obj, value):
        obj.color = QColor(value)

    def pos(self, obj, value):
        pos = QPointF(value[0], value[1])
        obj.setPos(pos)

    def child_items(self, obj, value):
        for child in value:
            child = self.decode(child)
            child.setParentItem(obj)
        

class LineDecoder(Decoder):

    def decode(self, obj):
        children = self.decode_child_items(obj)
        d = obj[1]
        instance = Line(width=d['width'],
                        length=d['length'])
        instance.setX(d['position'][0])
        instance.setY(d['position'][1])
        for child in children:
            child.setParentItem(instance)
        return instance

def decode_scene(scene, items):

    decoder = Decoder()

    with open('data.txt', 'r') as infile:
        objs = json.load(infile)
        for obj in objs:
            obj = decoder.decode(obj)
            # group: get child_items
            if obj.childItems():
                for child in obj.childItems():
                    child = decoder.decode(child)
                    scene.addItem(child)
            scene.addItem(obj)


class SceneDecoder(Decoder):

    def decode(self, obj):
        children = self.decode_child_items(obj)
        instance = Scene()
        for child in children:
            instance.addItem(child)
        rect = obj[1]['scene_rect']
        instance.setSceneRect(rect[0], rect[1], rect[2], rect[3])
        return instance

# def save_to_json():
#     fn = QFileDialog.getSaveFileName(self, 'Save', '', filter='*.txt')
#     fn = 'data' + '.txt'
#     with open(fn, 'w') as outfile:
#         json.dump(self.scene, outfile, cls=Encoder, indent=4)

# def read_from_json():
#     with open('data.txt', 'r') as infile:
#         objects = json.load(infile, cls=Decoder)
            




