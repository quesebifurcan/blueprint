from PyQt4.QtGui import *
from PyQt4.QtCore import *

from default_state import DefaultState


class InsertItemState(object):

    def __init__(self, scene, item=None):
        self.scene = scene
        self.current_insert = None
        self.click_pos = None
        self.item = item
        self.scene.activate_insert_cursor()
        self.scene.parent().statusBar().showMessage(
            "Insert " + self.item().__class__.__name__)

    def get_cursor_scene_position(self):
        cursor = QCursor()
        view_pos = self.scene.view.mapFromGlobal(cursor.pos())
        scene_pos = self.scene.view.mapToScene(view_pos)
        return scene_pos

    def mousePressEvent(self, e):
        self.scene.view.setDragMode(QGraphicsView.NoDrag)
        self.current_insert = self.item()
        self.scene.addItem(self.current_insert)
        if isinstance(self.current_insert, self.scene.stretchable_item_classes):
            self.current_insert.dx = 1
            self.current_insert.dy = 1
        self.current_insert.setPos(e.scenePos())
        self.click_pos = e.scenePos()

    def mouseMoveEvent(self, e):
        from scene_items.ramp import Ramp
        if self.current_insert:
            if isinstance(self.current_insert, self.scene.stretchable_item_classes):
                old, new = self.click_pos, e.scenePos()
                x_diff = new.x() - old.x()
                y_diff = new.y() - old.y()
                # use minimum size of object to filter signals
                self.current_insert.dx = self.current_insert.dx + x_diff
                if isinstance(self.current_insert, Ramp):
                    self.current_insert.dy -= y_diff
                else:
                    self.current_insert.dy += y_diff
            self.click_pos = e.scenePos()
            self.current_insert.update()

    def mouseReleaseEvent(self, e):
        self.current_insert = None

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.return_to_default_state()
            
    def return_to_default_state(self):
        self.scene.view.setDragMode(QGraphicsView.RubberBandDrag)
        self.scene.state = DefaultState
        self.scene.reset_cursor()
        
