from PyQt4.QtGui import *
from PyQt4.QtCore import *


class DefaultState(object):

    def __init__(self, scene):
        self.cls = scene.__class__
        self.scene = scene
        self.scene.parent().statusBar().showMessage("Normal Mode")

    def mousePressEvent(self, e):
        super(self.cls, self.scene).mousePressEvent(e)

    def mouseMoveEvent(self, e):
        super(self.cls, self.scene).mouseMoveEvent(e)

    def mouseReleaseEvent(self, e):
        super(self.cls, self.scene).mouseReleaseEvent(e)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_G:
            self.scene.group_mode_changed.emit(False)
        elif e.key() == Qt.Key_Escape:
            self.scene.group_mode_changed.emit(True)
        elif e.key() == Qt.Key_A:
            self.scene.select_all_items()
        elif e.key() == Qt.Key_8:
            self.scene.state = InsertItemState
        elif e.key() == Qt.Key_7:
            self.scene.copy_selection()
        elif e.key() == Qt.Key_9:
            self.scene.insert_mode = DefaultState
        elif e.key() == Qt.Key_D:
            self.scene.delete_selected_items()
        elif e.key() == Qt.Key_2:
            self.scene.delete_selected_groups()
        elif e.key() == Qt.Key_1:
            self.scene.group_selected_items()
        elif e.key() == Qt.Key_3:
            self.scene.join_nested_groups()
        # elif e.key() == Qt.Key_L:
        #     self.scene.align_items_horizontally()
        # elif e.key() == Qt.Key_J:
        #     self.scene.align_items_vertically()
        elif e.key() == Qt.Key_P:
            self.scene.parent().propertiesEditorDock.show()
        elif e.key() == Qt.Key_N:
            self.scene.pageview.render_to_image()
            self.scene.pageview.export_pdf()
        else:
            super(self.scene.__class__, self.scene).keyPressEvent(e)

