from PyQt4.QtGui import *
from PyQt4.QtCore import *

from default_state import DefaultState


class InsertPathItemState(object):

    def __init__(self, scene, item=None):
        self.scene = scene
        self.current_insert = None
        self.click_pos = None
        self.item = item()
        self.scene.activate_insert_cursor()
        self.scene.parent().statusBar().showMessage(
            "Insert " + self.item.__class__.__name__)

    def mousePressEvent(self, e):
        if not self.current_insert:
            self.scene.addItem(self.item)
            self.item.setPos(e.scenePos())
            self.item.add_point(QPointF())
            self.current_insert = self.item
            self.item.show()
        else:
            pos = e.scenePos()
            self.item.add_point(self.item.mapFromScene(pos))

    def mouseMoveEvent(self, e):
        pass

    def mouseReleaseEvent(self, e):
        pass

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.return_to_default_state()

    def return_to_default_state(self):
        self.scene.view.setDragMode(QGraphicsView.RubberBandDrag)
        self.scene.state = DefaultState
        self.scene.reset_cursor()
