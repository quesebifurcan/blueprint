from PyQt4.QtGui import *
from PyQt4.QtCore import *

from default_state import DefaultState

class TextEditState(object):

    def __init__(self, scene):
        self.cls = scene.__class__
        self.scene = scene
        self.scene.parent().statusBar().showMessage("Edit text")

    def mousePressEvent(self, e):
        super(self.cls, self.scene).mousePressEvent(e)

    def mouseMoveEvent(self, e):
        super(self.cls, self.scene).mouseMoveEvent(e)

    def mouseReleaseEvent(self, e):
        super(self.cls, self.scene).mouseReleaseEvent(e)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.return_to_default_state()
        else:
            super(self.cls, self.scene).keyPressEvent(e)
            
    def return_to_default_state(self):
        self.scene.state = DefaultState
