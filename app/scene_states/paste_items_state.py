from PyQt4.QtGui import *
from PyQt4.QtCore import *

from default_state import DefaultState

from scene_items.group import Group


class PasteItemsState(object):

    def __init__(self, scene, group):
        self.scene = scene
        self.group = group

    def update_status_message(self, pos):
        x, y = str(pos.x()), str(pos.y())
        msg = ' '.join(['Paste items at', x, y])
        self.scene.parent().statusBar().showMessage(msg)

    def mousePressEvent(self, e):
        self.scene.view.setDragMode(QGraphicsView.NoDrag)
        self.scene.addItem(self.group)
        self.group.setPos(e.scenePos())

    def mouseMoveEvent(self, e):
        from scene_items.group import Group
        self.update_status_message(e.scenePos())
        self.group.setPos(e.scenePos())

    def mouseReleaseEvent(self, e):
        super(self.scene.__class__, self.scene).mouseReleaseEvent(e)
        self.return_to_default_state()

    def return_to_default_state(self):
        self.scene.view.setDragMode(QGraphicsView.RubberBandDrag)
        self.scene.state = DefaultState
        self.scene.reset_cursor()
    

