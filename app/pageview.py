from __future__ import division

from PyQt4.QtGui import *
from PyQt4.QtCore import *

import utils

from scene_items.layoutmarkers import PageMarker, SystemMarker


class PageView(QGraphicsView):

    def __init__(self, parent=None):
        super(PageView, self).__init__(parent)

        self.scene = None

        self.printer = None
        self.pages = []
        self.views = []

        self.systems_per_page = 2
        self.display = QGraphicsScene()
        self.setScene(self.display)

    def set_page_layout(self, size, orientation, systems_per_page):
        self.printer = QPrinter(QPrinter.HighResolution)
        self.printer.setPageSize(size)
        self.printer.setOrientation(orientation)
        self.printer.setOutputFormat(QPrinter.PdfFormat)
        self.systems_per_page = systems_per_page

    def source_rects_from_auto_divisions(self):
        coll = []
        width = self.source_rect_width
        height = self.scene.sceneRect().height()
        offset = 0
        while offset < self.scene.sceneRect().width():
            rect = QRectF(offset, 0, width, height)
            coll.append(rect)
            offset += width
        return coll

    def autoset_page_markers(self, systems_per_page=3):
        self.systems_per_page = systems_per_page
        if self.scene.markers:
            for m in self.scene.markers:
                self.scene.removeItem(m)
            self.scene.markers = []
        for rect in self.source_rects_from_auto_divisions():
            marker = PageMarker()
            marker.setPos(rect.x(), -35)
            self.scene.addItem(marker)

    def export_pdf(self, filename="testing.pdf"):

        self.printer.setOutputFileName(filename)
        p = QPainter(self.printer)
        i = 0

        for target, source in zip(self.target_rects, self.source_rects):
            self.scene.render(p, target, source, Qt.KeepAspectRatio)
            if (i+1) % self.systems_per_page == 0:
                self.printer.newPage()
            i += 1

        p.end()

    def render_to_image(self):

        if not self.printer:
            self.printer = QPrinter()
            self.set_page_layout(
                QPrinter.A4, QPrinter.Landscape, systems_per_page=1)
            self.autoset_page_markers(systems_per_page=1)

        self.setBackgroundBrush(QBrush(QColor('Gainsboro')))

        paper_size = utils.page_size_to_page_view_paper_size(self.printer.pageRect())
        margins = paper_size.height() / 100 
        sys_count = self.systems_per_page
        source_rect_groups = list(utils.grouper(sys_count, self.source_rects))
        curr_x_pos = 0

        for group in source_rect_groups:
            # draw background rect ("page")
            page = QGraphicsRectItem()
            page.setRect(paper_size)
            page.setBrush(QBrush(QColor('white')))
            page.setPen(QPen(QColor('Gainsboro').darker()))

            page.setPos(curr_x_pos + margins, margins)
            self.display.addItem(page)

            curr_x_pos += (paper_size.width() + margins)

            # initiate variables for drawing of systems
            system_width = paper_size.width() - (margins * 2)
            system_height = (paper_size.height() - (margins * 1)) / sys_count

            x_pos = page.pos().x() + margins
            y_pos = page.pos().y() + margins

            for system in group:
                if system:
                    # create image 
                    image = QImage(QSize(system_width, (system_height - margins)), 
                                   QImage.Format_ARGB32)
                    image.fill(QColor('white'))

                    # render scene selection to image
                    p = QPainter(image)
                    p.setRenderHint(QPainter.Antialiasing)
                    p.setRenderHint(QPainter.TextAntialiasing)
                    p.setRenderHint(QPainter.SmoothPixmapTransform)
                    target_rect = QRectF(0, 0, image.width(), image.height())
                    if target_rect:
                        self.scene.render(p, target_rect, system, Qt.KeepAspectRatio)
                    p.end()

                    # convert image to pixmap-item and add to scene
                    pixmap = QPixmap.fromImage(image)
                    item = QGraphicsPixmapItem(pixmap)
                    item.setPos(x_pos, y_pos)
                    self.display.addItem(item)
                    y_pos += system_height

        initial_view = QRectF(0, 0, 3000, 2200)
        # self.fitInView(initial_view, Qt.KeepAspectRatio)
        self.setSceneRect(0, 0, 10000, 10000)


    def update_views(self):
        pass

    @property
    def margins(self):
        return self.printer.pageRect().height() / 100

    @property
    def target_system_height(self):
        height = self.printer.pageRect().height() / self.systems_per_page
        return height

    @property
    def target_system_width(self):
        return self.printer.pageRect().width()

    @property
    def source_rect_width(self):
        ratio = self.target_system_width / self.target_system_height
        width = self.scene.sceneRect().height() * ratio
        return width

    @property
    def source_rects(self):
        if len(self.scene.markers) <= 1:
            # ...the entire scene fits on one system
            return [self.scene.sceneRect()]
        rects = []
        for curr, nxt in zip(self.scene.markers, self.scene.markers[1:]):
            x = curr.pos().x()
            y = 0
            width = nxt.pos().x() - x
            height = self.scene.sceneRect().height()
            rects.append(QRectF(x, y, width, height))
        return rects

    @property
    def target_rects(self):
        # source_rects, page_height, margins
        longest = max(self.source_rects, key=lambda r: r.width()).width()
        height = self.target_system_height
        margins = self.margins
        y_offset = 0
        target_rects = [] 
        for i, rect in enumerate(self.source_rects):
            ratio = rect.width() / longest
            width = self.target_system_width * ratio

            target = QRectF(0, y_offset, width, height)
            target_rects.append(target)

            y_offset += (height + margins)
            if (i+1) % self.systems_per_page == 0:
                y_offset = 0
            i += 1
        return target_rects


class Page(QImage):

    def __init__(self, parent=None):
        super(Page, self).__init__(parent)


