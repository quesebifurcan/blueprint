from PyQt4.QtCore import pyqtRemoveInputHook
from pdb import set_trace

def debug_trace():
    pyqtRemoveInputHook()
    set_trace()
