from PyQt4.QtGui import *
from PyQt4.QtCore import *

from commands.set_state_command import SetStateCommand
from scene_states.insert_item_state import InsertItemState

class MouseModeSelectorMenu(QMenu):

    def __init__(self, scene=None, parent=None):
        super(MouseModeSelectorMenu, self).__init__(parent)
        self.scene = scene
