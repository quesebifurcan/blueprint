import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class UndoView(QUndoView):

    def __init__(self, parent=None):
        super(UndoView, self).__init__(parent)
        self.setWindowTitle("Undo")
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.setAlternatingRowColors(True)
        self.pos_offset = 12

    def show(self):
        pos = QCursor().pos()
        self.move(pos.x() - self.pos_offset,
                  pos.y() - self.pos_offset)
        super(UndoView, self).show()
        self.activateWindow()
        self.raise_()

