from PyQt4.QtGui import *
from PyQt4.QtCore import *

class ScaleVerticallyCommand(QUndoCommand):

    def __init__(self, item, amount):
        self.item = item
        self.amount = amount
        super(ScaleVerticallyCommand, self).__init__(
            "Item scaled vertically")

    def undo(self):
        self.item.scale_vertically(self.amount * -1)

    def redo(self):
        self.item.scale_vertically(self.amount)

class ResizeFromLeftAndScaleCommand(QUndoCommand):

    def __init__(self, item, amount):
        self.item = item
        self.amount = amount
        super(ResizeFromLeftAndScaleCommand, self).__init__(
            "Item " + item.__class__.__name__ + " scaled") 

    def undo(self):
        self.item.resize_from_left_and_scale(self.amount * -1)

    def redo(self):
        self.item.resize_from_left_and_scale(self.amount)


class ResizeFromRightAndScaleCommand(QUndoCommand):

    def __init__(self, item, amount):
        self.item = item
        self.amount = amount
        super(ResizeFromRightAndScaleCommand, self).__init__(
            "Item " + item.__class__.__name__ + " scaled") 

    def undo(self):
        self.item.resize_from_right_and_scale(self.amount * -1)

    def redo(self):
        self.item.resize_from_right_and_scale(self.amount)
