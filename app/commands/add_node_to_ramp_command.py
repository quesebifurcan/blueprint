from PyQt4.QtGui import *
from PyQt4.QtCore import *

class AddNodeToRampCommand(QUndoCommand):

    def __init__(self, ramp, item, pos):
        self.ramp = ramp
        self.item = item
        self.pos = pos
        super(InsertItemCommand, self).__init__(
            "Add Node to Ramp at pos " + str(self.pos))

    def undo(self):
        self.ramp.scene().removeItem(self.item)

    def redo(self):
        self.item.setParentItem(self.ramp)
        self.item.setPos(self.pos)
