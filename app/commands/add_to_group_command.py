from PyQt4.QtGui import *
from PyQt4.QtCore import *

class AddToGroupCommand(QUndoCommand):

    def __init__(self, group, item):
        self.group = group
        self.item = item
        super(AddToGroupCommand, self).__init__(
            "Add " + item.__class__.__name__ \
            + " to " + group.__class__.__name__)
            
    def undo(self):
        self.group.removeFromGroup(self.item)

    def redo(self):
        self.group.addToGroup(self.item)


class RemoveFromGroupCommand(QUndoCommand):

    def __init__(self, group, item):
        self.group = group
        self.item = item
        super(RemoveFromGroupCommand, self).__init__(
            "Remove " + item.__class__.__name__ \
            + " from " + group.__class__.__name__)
            
    def undo(self):
        self.group.addToGroup(self.item)

    def redo(self):
        self.group.removeFromGroup(self.item)
        self.group.scene().removeItem(self.item)
        self.group.scene().addItem(self.item)
