from PyQt4.QtGui import *
from PyQt4.QtCore import *

class MoveItemCommand(QUndoCommand):

    def __init__(self, item, new_pos=None):
        super(MoveItemCommand, self).__init__(
            "Move " + item.__class__.__name__)
        self.item = item
        self.old_pos = item.pos()
        self.new_pos = new_pos

    def undo(self):
        self.item.setPos(self.old_pos)

    def redo(self):
        self.item.setPos(self.new_pos)



