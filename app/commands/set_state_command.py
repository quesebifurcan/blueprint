from PyQt4.QtGui import *
from PyQt4.QtCore import *

class SetStateCommand(QUndoCommand):

    def __init__(self, scene, state): 
        self.scene = scene
        self.old_state = scene.state
        self.new_state = state
        super(SetStateCommand, self).__init__()
            
    def undo(self):
        self.scene.state = self.old_state

    def redo(self):
        self.scene.state = self.new_state
