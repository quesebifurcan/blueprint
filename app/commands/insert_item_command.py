from PyQt4.QtGui import *
from PyQt4.QtCore import *

class InsertItemCommand(QUndoCommand):

    def __init__(self, scene, item, pos):
        self.scene = scene
        self.item = item
        self.pos = pos
        super(InsertItemCommand, self).__init__(
            "Insert " + self.item.__class__.__name__)

    def undo(self):
        self.scene.removeItem(self.item)

    def redo(self):
        self.scene.addItem(self.item)
        self.item.setPos(self.pos)
