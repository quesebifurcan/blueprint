from PyQt4.QtGui import *
from PyQt4.QtCore import *


class SetPropertyCommand(QUndoCommand):

    def __init__(self, items, property_, new):
        self.items = items
        self.property_ = property_
        self.new = new
        self.old = []
        for item in items:
            self.old.append(getattr(item, property_))
        self.allow_merge = False
        description = 'Set ' + property_ 
        super(SetPropertyCommand, self).__init__(description)

    def undo(self):
        for item, value in zip(self.items, self.old):
            setattr(item, self.property_, value)

    def redo(self):
        for item in self.items:
            setattr(item, self.property_, self.new)

    def mergeWith(self, other):
        if self.property_ == other.property_ and self.allow_merge:
            self.new = other.new
            return True
        else:
            return False

