from PyQt4.QtGui import *
from PyQt4.QtCore import *

class DetachFromParentGroupCommand(QUndoCommand):

    def __init__(self, group): 
        super(DetachFromParentGroupCommand, self).__init__(
            "Group orphaned")
        self.group = group
        self.parent = group.group()

    def undo(self):
        self.group.temp_group.addToGroup(self.group)

    def redo(self):
        self.group.temporarily_detach_from_parent_group()



