from PyQt4.QtGui import *
from PyQt4.QtCore import *

class AddChildItemCommand(QUndoCommand):

    def __init__(self, parent, child, pos):
        self.parent = parent
        self.child = child
        self.pos = pos
        super(AddChildItemCommand, self).__init__(
            "Insert " + child.__class__.__name__ \
            + " in " + parent.__class__.__name__)
            

    def undo(self):
        self.parent.scene().removeItem(self.child)

    def redo(self):
        self.child.setParentItem(self.parent)
        self.child.setPos(self.pos)
