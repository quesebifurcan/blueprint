from PyQt4.QtGui import *
from PyQt4.QtCore import *

from properties_editor_view import PropertiesEditorView
from properties_editor_model import PropertiesEditorModel

from scene_items.timetext import TimeTextAnchor
from scene_items.textitem import TextItemAnchor

from properties_editor_delegate import LineStyleComboBoxDelegate

from commands.set_property_command import SetPropertyCommand
from properties_editor_delegate import GridOrientationComboBoxDelegate


class PropertiesEditorWidget(QWidget):

    ### INITIALIZER ###

    def __init__(self, scene=None, parent=None):
        super(PropertiesEditorWidget, self).__init__(parent)
        self.scene=scene

        self.model=PropertiesEditorModel(scene)
        self.view=PropertiesEditorView(self.model, self.scene)
        self.view.show()

        self._selected_items = None
        self._active_properties = None

        ### SIGNALS ###
        self.model.itemChanged.connect(self.set_property)
        self.view.new_editor_activated.connect(self.disallow_undo_merge)
        self.view.activated.connect(self.disallow_undo_merge)
        
        self.init_layout()

    def init_layout(self):
        layout = QVBoxLayout()
        layout.addWidget(self.view)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

    ### PUBLIC PROPERTIES ###

    @property
    def selected_items(self):
        return self._selected_items

    @selected_items.setter
    def selected_items(self, items):
        self._selected_items = items

    @property
    def active_properties(self):
        coll = []
        for item in self.selected_items:
            for p in item.properties():
                if not p in coll:
                    coll.append(p)
        return coll

    ### PUBLIC METHODS ### 

    def disallow_undo_merge(self):
        stack = self.scene.parent().undoStack
        count = stack.count()
        last = stack.command(count - 1)
        if last:
            last.allow_merge = False

    def has_grid_editor(self, item):
        from editor_groups import GridEditorGroup
        return isinstance(item.parent(), GridEditorGroup)

    def add_grid(self, grid):
        from editor_groups import GridEditorGroup
        index = len(self.scene.grids)
        name = "Grid " + str(len(self.scene.grids))
        grid_editor = GridEditorGroup(model=self.model,
                                      view=self.view,
                                      name=name,
                                      row=index-1,
                                      parent=self.view.grid_editor,
                                      is_top_level=False)
        grid_editor.grid = grid
        self.view.expandAll()
        return grid

    def get_data(self, property_):
        editor = getattr(self.view, property_)
        return editor.data()

    def set_data(self, property_, value):
        editor = getattr(self.view.itemDelegate(), property_)
        editor.set_value(value)

    def set_property(self, item):
        if hasattr(item, 'editor'):
            if self.has_grid_editor(item):
                self.set_grid_property(item)
            else:
                self.set_item_properties(item)

    def set_grid_property(self, item):
        if hasattr(item.parent(), 'grid'):
            print item, item.property_, 'setting grid'
            grid = item.parent().grid
            setattr(grid, item.property_, item.editor.converted_item_data())

    def set_item_properties(self, item):
        from scene_items.baseitem import BaseItem
        data = item.editor.converted_item_data()
        coll = []
        for scene_item in self.scene.items():
            if isinstance(scene_item, BaseItem) and \
               not isinstance(scene_item, (TextItemAnchor, TimeTextAnchor)) and \
               scene_item.isSelected():
                if hasattr(scene_item, item.property_):
                    coll.append(scene_item)
        stack = self.scene.parent().undoStack
        count = stack.count()
        last = stack.command(count - 1)
        cmd = SetPropertyCommand(coll, item.property_, data)
        cmd.allow_merge = True
        if last and last.mergeWith(cmd):
            last.redo()
        else:
            stack.push(cmd)

    ### TRIGGERED FUNCTIONS ###
    
    def update_selection(self):
        self.selected_items = self.scene.all_selected_items()


    
        
