from PyQt4.QtGui import *
from PyQt4.QtCore import *

from collections import OrderedDict

class BaseEditorDelegate(QWidget):

    def __init__(self, parent):
        super(BaseEditorDelegate, self).__init__(parent) 
        self.set_font_size(11)

    def set_item(self, item):
        self.item = item
        self.item.editor = self
        
    ## comment
    
    def set_font_size(self, value):
        font = self.font()
        font.setPointSize(value)
        self.setFont(font)

    def set_value(self, value):
        self.item.setData(value)

    def update_item_data(self):
        self.item.setData(self.get_editor_data())

    def update_editor_data(self):
        self.blockSignals(True)
        self.set_editor_data()
        self.blockSignals(False)

    def get_editor_data(self):
        raise NotImplementedError

    def set_editor_data(self):
        raise NotImplementedError


class CheckBoxDelegate(QCheckBox, BaseEditorDelegate):

    def __init__(self, item=None, parent=None):
        super(CheckBoxDelegate, self).__init__(parent)
        self.set_item(item)
        self.set_value(0)
        self.stateChanged.connect(self.set_value)

    def converted_item_data(self):
        return self.item.data().toInt()[0]

    def get_editor_data(self):
        return self.checkState()

    def set_editor_data(self):
        self.setCheckState(self.converted_item_data())


class SpinBoxRanges(object):

    ranges = {
        'x y dx dy': (0, 100000, 1),
        'interval ': (1, 100000, 1),
        'linewidth ': (0, 100, 1),
        'pointsize ': (0, 400, 0.1),
        'transparency ': (0, 255, 1),
        }

    def property_to_key(self, property_):
        key = filter(lambda k: property_ in k, self.ranges)[0]
        return key

    def get_range(self, property_):
        key = self.property_to_key(property_)
        return self.ranges[key]


class BaseSpinBoxDelegate(QSpinBox, BaseEditorDelegate):

    ranges = SpinBoxRanges()

    def __init__(self, item=None, parent=None):
        super(BaseSpinBoxDelegate, self).__init__(parent)
        self.set_item(item)

        self.set_value(1)
        curr_range = self.ranges.get_range(item.property_)
        self.set_range(*curr_range)

    def converted_item_data(self):
        return self.item.data().toInt()[0]

    def get_editor_data(self):
        return self.value()

    def set_editor_data(self):
        self.setValue(self.converted_item_data())

    def set_range(self, minimum=0, maximum=100, step=1):
        self.setMinimum(minimum)
        self.setMaximum(maximum)
        self.setSingleStep(step)


class SpinBoxDelegate(BaseSpinBoxDelegate):

    def __init__(self, item=None, parent=None):
        super(SpinBoxDelegate, self).__init__(item, parent)
        self.valueChanged.connect(self.set_value)


class DelayUpdateSpinBoxDelegate(BaseSpinBoxDelegate):

    def __init__(self, item=None, parent=None):
        super(DelayUpdateSpinBoxDelegate, self).__init__(item, parent)
        self.editingFinished.connect(self.send_value)

    def send_value(self):
        self.set_value(self.value())


class DoubleSpinBoxDelegate(QDoubleSpinBox, SpinBoxDelegate):

    def __init__(self, item=None, parent=None):
        super(DoubleSpinBoxDelegate, self).__init__(item, parent)

    def converted_item_data(self):
        return self.item.data().toFloat()[0]


class FontBoxDelegate(QFontComboBox, BaseEditorDelegate):

    def __init__(self, item=None, parent=None):
        super(FontBoxDelegate, self).__init__(parent)
        self.set_item(item)
        self.set_value('Lucida Grande')
        self.init_UI()

    def init_UI(self):
        self.setEditable(False)
        self.setStyleSheet("QComboBox { combobox-popup: 0; }");
        self.setMaxVisibleItems(10)

    def converted_item_data(self):
        return QFont(self.item.data()).family()

    def get_editor_data(self):
        return self.currentFont()

    def set_editor_data(self):
        self.setCurrentFont(QFont(self.converted_item_data()))


class LineStyleComboBoxDelegate(QComboBox, BaseEditorDelegate):

    styles = OrderedDict(
        [('solid', Qt.SolidLine),
         ('dashed', Qt.DashLine),
         ('dotted', Qt.DotLine),
         ('dash-dot', Qt.DashDotLine),
         ('dash-dot-dot', Qt.DashDotDotLine)])

    def __init__(self, item=None, parent=None):
        super(LineStyleComboBoxDelegate, self).__init__(parent)
        self.setEditable(False)

        for style in self.styles.keys():
            self.addItem(style) 

        self.set_item(item)
        self.currentIndexChanged.connect(self.set_value)

    def converted_item_data(self):
        idx = self.item.data().toInt()[0]
        return self.styles.values()[idx]

    def get_editor_data(self):
        return self.currentText()

    def update_item_data(self):
        self.blockSignals(True)
        value = self.styles.keys().index(str(self.get_editor_data()))
        self.item.setData(value)
        self.blockSignals(False)

    def set_editor_data(self):
        self.update_item_data()
        value = self.converted_item_data()
        idx = self.item.data().toInt()[0]
        self.setCurrentIndex(idx)
        self.currentText()


class CapStyleComboxDelegate(LineStyleComboBoxDelegate):

    styles = OrderedDict([
        ('square', Qt.SquareCap),
        ('flat', Qt.FlatCap),
        ('round', Qt.RoundCap),
        ])


class GridOrientationComboBoxDelegate(QComboBox, BaseEditorDelegate):

    def __init__(self, item=None, parent=None):
        super(GridOrientationComboBoxDelegate, self).__init__(parent)
        self.setEditable(False)
        self.set_item(item)

        self.orientations = 'horizontal vertical'.split()

        for orientation in self.orientations:
            self.addItem(orientation)

        self.set_value('vertical')

    def converted_item_data(self):
        return str(self.item.data().toString())

    def get_editor_data(self):
        return self.currentText()

    def set_editor_data(self):
        value = self.converted_item_data()
        idx = self.orientations.index(value)
        self.setCurrentIndex(idx)


class ClickableLabel(QLabel):

    clicked = pyqtSignal()

    def __init__(self, parent=None):
        super(ClickableLabel, self).__init__(parent)
        self.setMaximumWidth(30)
        self.setMinimumWidth(30)
        self.setStyleSheet("border: 1px solid black; background-color: white;")
        self.item = None

    def mousePressEvent(self, e):
        self.clicked.emit()


class ColorEditorDelegate(BaseEditorDelegate):

    def __init__(self, item=None, parent=None):
        super(ColorEditorDelegate, self).__init__(parent)

        self.set_item(item)
        self.init_UI()

        self.label.clicked.connect(self.show_dialog)
        self.set_value(QColor('white'))
        self.dialog = QColorDialog()

    def init_UI(self):
        layout = QHBoxLayout()

        self.label = ClickableLabel()
        space = QLabel()
        layout.addWidget(space)
        layout.addWidget(self.label)

        self.setLayout(layout) 
        self.layout().setContentsMargins(5, 5, 5, 5)
        self.setContentsMargins(1, 1, 1, 1)

    def show_dialog(self):
        self.dialog.setCurrentColor(QColor(self.item.data()))
        self.dialog.setOption(QColorDialog.ShowAlphaChannel)
        self.dialog.currentColorChanged.connect(self.set_value)
        self.dialog.exec_()
        del self.dialog
        self.dialog = QColorDialog()

    def converted_item_data(self):
        return QColor(self.item.data())

    def get_editor_data(self):
        return self.dialog.currentColor()

    def set_editor_data(self):
        color = self.converted_item_data()
        s = "border: 1px solid black; background-color:" + color.name()
        self.label.setStyleSheet(s)
        self.item.setText(self.item.data().toString())

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Return:
            self.show_dialog()
        else:
            super(ColorEditorDelegate, self).keyPressEvent(e)


class Delegate(QStyledItemDelegate):

    def __init__(self, model=None, tree=None, parent=None):
        super(Delegate, self).__init__(parent)
        self.model = model
        self.tree = tree
        self.testing = None

    def get_item(self, index):
        return self.model.itemFromIndex(index)

    def createEditor(self, parent, option, index):
        item = self.get_item(index)
        editor = item.delegate(item, parent)
        return editor

    def setEditorData(self, editor, index): 
        editor.update_editor_data()

    def setModelData(self, editor, model, index):
        item = self.get_item(index)
        editor.update_item_data()
