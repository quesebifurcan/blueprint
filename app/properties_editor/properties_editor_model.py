from PyQt4.QtGui import *
from PyQt4.QtCore import *


from scene_items.textitem import TextItem
from scene_items.timetext import TimeText
from scene_items.ramp import Ramp, RampNode
from scene_items.group import Group
from scene_items.line import HorizontalLine, LineNode

from commands.set_property_command import SetPropertyCommand
from tracing import debug_trace

class PropertiesEditorModel(QStandardItemModel):

    def __init__(self, scene=None, parent=None):
        super(PropertiesEditorModel, self).__init__(parent)
        self.scene = scene
        self.setColumnCount(2)
        # self.itemChanged.connect(self.set_property)
        self.setHorizontalHeaderLabels(["Property", "Value"])

    def get_selected_items(self):
        items = []
        for item in self.scene.selectedItems():
            if isinstance(item, Group):
                for i in item.get_all_items():
                    items.append(i)
            else:
                items.append(item)
        return set(items)

    def set_property(self, data):
        # get currently selected items in scene and apply if hasattr...
        if hasattr(data, "datatype"):
            if data.datatype == "check":
                print data.checkState()
            elif data.datatype == "font":
                items = self.get_selected_items()
                for item in items:
                    if hasattr(item, 'fontfamily'):
                        item.fontfamily = QFont(data.data()).family()
                # print QFont(data.data())
    #         elif data.datatype == "int":
    #             print data.data().toInt()[0]
    #         elif data.datatype == "color":
    #             print 'item changed', data.data().toString()
    #             items = set(self.get_selected_items())
    #             for item in items:
    #                 print item
    #                 if hasattr(item, 'color'):
    #                     cls = item.__class__
    #                     # fn = lambda i: i.co
    #                     # item = item
    #                     # old = item.color()
    #                     # new = QColor(data.data())
    #                     # cmd = SetPropertyCommand(fn, item, old, new)
    #                     # self.scene.parent().undoStack.push(cmd)
    #                     item.color = QColor(data.data())
    #         else:
    #             print data.data().toString()
