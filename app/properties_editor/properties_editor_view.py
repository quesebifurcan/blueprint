import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from properties_editor_delegate import Delegate
from properties_editor_model import PropertiesEditorModel


class PropertiesEditorView(QTreeView):

    new_editor_activated = pyqtSignal()

    def __init__(self, model=None, scene=None, parent=None):
        super(PropertiesEditorView, self).__init__(parent)
        self.scene=scene
        self.model=model
        self.setModel(self.model)

        font = self.font()
        font.setPointSize(11)
        self.setFont(font)
        self.setIndentation(15)

        self.setAlternatingRowColors(True)
        self.setItemDelegate(Delegate(self.model, self)) 

        self.grids = []
        self.init_model(self.model)
        self.expandAll()
        self.resizeColumnToContents(0)

    def add_grid(self):
        index = len(self.grids)
        name = "Grid " + str(len(self.grids) + 1)
        grid = GridEditorGroup(model=self.model, view=self, name=name,
                              row=index, parent=self.grid_root_item)
        self.grids.append(grid)
        return grid

    def currentChanged(self, curr, prev):
        '''
        select next item without updating it. enables free movement in
        the tree, using the arrow keys.
        '''
        item = self.model.itemFromIndex(curr)
        if hasattr(item, 'property_'):
            if item.property_ == "color":
                item.editor.setFocus(True)
        self.selectionModel().setCurrentIndex(curr, QItemSelectionModel.NoUpdate);
        self.scrollTo(curr)
        self.new_editor_activated.emit()

    def init_grid_root_item(self):
        self.grid_editor, blank = QStandardItem("Grids"), QStandardItem()
        self.grid_editor.setEditable(False)
        font = self.grid_editor.font()
        font.setWeight(QFont.Bold)
        self.grid_editor.setFont(font)
        self.model.appendRow([self.grid_editor, blank])

    def init_model(self, model):
        from properties_editor_delegate import CheckBoxDelegate, SpinBoxDelegate, FontBoxDelegate, DoubleSpinBoxDelegate, LineStyleComboBoxDelegate, GridOrientationComboBoxDelegate, ColorEditorDelegate, CapStyleComboxDelegate

        from editor_groups import BaseEditorGroup
        self.geometry_editor = BaseEditorGroup("Geometry", self.model, self)

        self.x = self.geometry_editor.insert_row(
            name='X',
            property_='x',
            delegate=SpinBoxDelegate)

        self.y = self.geometry_editor.insert_row(
            name='Y',
            property_='y',
            delegate=SpinBoxDelegate)

        self.dx = self.geometry_editor.insert_row(
            name='Width',
            property_='dx',
            delegate=SpinBoxDelegate)

        self.dy = self.geometry_editor.insert_row(
            name='Height',
            property_='dy',
            delegate=SpinBoxDelegate)

        self.font_editor = BaseEditorGroup("Font", self.model, self)

        self.fontfamily = self.font_editor.insert_row(
            name='Font family',
            property_='fontfamily',
            delegate=FontBoxDelegate)

        self.pointsize = self.font_editor.insert_row(
            name='Font size',
            property_='pointsize',
            delegate=SpinBoxDelegate)

        self.bold = self.font_editor.insert_row(
            name='Bold',
            property_='bold',
            delegate=CheckBoxDelegate)

        self.italic = self.font_editor.insert_row(
            name='Italic',
            property_='italic',
            delegate=CheckBoxDelegate)

        self.underline = self.font_editor.insert_row(
            name='Underline',
            property_='underline',
            delegate=CheckBoxDelegate)

        self.strikeout = self.font_editor.insert_row(
            name='Strikeout',
            property_='strikeout',
            delegate=CheckBoxDelegate)

        self.line_editor = BaseEditorGroup("Line", self.model, self)

        self.linewidth = self.line_editor.insert_row(
            name='Line width',
            property_='linewidth',
            delegate=SpinBoxDelegate)

        self.linestyle = self.line_editor.insert_row(
            name='Line style',
            property_='linestyle',
            delegate=LineStyleComboBoxDelegate)

        self.linestyle = self.line_editor.insert_row(
            name='Cap style',
            property_='capstyle',
            delegate=CapStyleComboxDelegate)

        self.color_editor = BaseEditorGroup("Color", self.model, self)

        self.color = self.color_editor.insert_row(
            name='Color',
            property_='color',
            delegate=ColorEditorDelegate)

        self.transparency = self.color_editor.insert_row(
            name='Alpha',
            property_='transparency',
            delegate=SpinBoxDelegate)

        self.grid_editor = BaseEditorGroup("Grids", self.model, self)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Return:
            self.new_editor_activated.emit()
        super(PropertiesEditorView, self).keyPressEvent(e)
