from PyQt4.QtGui import *
from PyQt4.QtCore import *


class BaseEditorGroup(QStandardItem):

    def __init__(self, name=None, model=None, view=None, parent=None, is_top_level=True):
        super(BaseEditorGroup, self).__init__(name)
        self.model = model
        self.view = view

        font = self.font()
        font.setWeight(QFont.Bold)
        self.setFont(font)
        self.setEditable(False)
        self.empty = QStandardItem()
        if is_top_level:
            self.insert_self_as_new_row(self.model)
        else:
            self.insert_self_as_new_row(parent)

    def insert_self_as_new_row(self, parent=None):
        parent.appendRow([self, self.empty])

    def insert_row(self, name, property_, delegate):
        label = QStandardItem(name)
        label.setTextAlignment(Qt.AlignCenter)
        item = QStandardItem()
        item.setEditable(False)
        item.property_ = property_
        item.delegate = delegate
        row = self.rowCount()
        self.setChild(row, 0, label)
        self.setChild(row, 1, item)
        self.view.openPersistentEditor(self.model.indexFromItem(item))
        return item

from properties_editor_delegate import GridOrientationComboBoxDelegate, SpinBoxDelegate, DelayUpdateSpinBoxDelegate, CheckBoxDelegate

class GridEditorGroup(BaseEditorGroup):

    def __init__(self, model=None, view=None, name=None,
                 row=0, parent=None, is_top_level=False):
        super(GridEditorGroup, self).__init__(name, model, view, parent, is_top_level)
        self.orientation = self.insert_row(
            name='Orientation',
            property_='orientation',
            delegate=GridOrientationComboBoxDelegate)

        self.interval = self.insert_row(
            name='Interval',
            property_='interval',
            delegate=DelayUpdateSpinBoxDelegate)

        self.is_time_based = self.insert_row(
            name='Time based',
            property_='is_time_based',
            delegate=CheckBoxDelegate)

        self.allow_edits = self.insert_row(
            name='Selectable',
            property_='editable',
            delegate=CheckBoxDelegate)
        # self.orientation = self.new_editor_item(
        #     "Orientation",
        #     1,
        #     fn=lambda item: make_number_item(item, property_='orientation'))
        # self.lines_visible = self.new_editor_item(
        #     "Lines Visible",
        #     2,
        #     fn=lambda item: make_checkbox_item(item, property_='lines_visible'))
        # self.textitems_visible = self.new_editor_item(
        #     "Timetext",
        #     3,
        #     fn=lambda item: make_checkbox_item(item, property_='textitems_visible'))
        # self.textitems_placement = self.new_editor_item(
        #     "Text offset",
        #     4,
        #     fn=lambda item: make_number_item(item, property_='textitems_placement'))
        # self.print_ = self.new_editor_item(
        #     "Print",
        #     5,
        #     fn=lambda item: make_checkbox_item(item, property_='print'))

        self.view.resizeColumnToContents(0)
        self.view.resizeColumnToContents(1)
        

