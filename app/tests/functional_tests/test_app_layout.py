import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from properties_editor.propertieseditor import PropertiesEditor

from ..fixtures.app_fixture import main_fixture as main

#---------------------------------------------------------------------
# add components to main window:
# 1. mouse_mode_selector
# 2. statusbar
# 3. activate fullscreen
#---------------------------------------------------------------------

class TestMainWindowComponents:

    def test_has_status_bar(self, main):
        assert isinstance(main.statusBar(), QStatusBar)

    def test_status_bar_display_text(self, main):
        main.statusBar().showMessage("testing...")
        assert main.statusBar().currentMessage() == "testing..."

    # def test_has_mouse_mode_selector(self, main):
    #     assert isinstance(main.mouseModeSelector, QWidget)


#---------------------------------------------------------------------
# add widgets to dock:
# 1. filter window (for selections)
# 2. properties editor
# 3. graph of all items currently in scene
# 4. hide/show dock
#---------------------------------------------------------------------

class TestItemEditorDock:

    def test_has_selection_filter_dock(self, main):
        assert isinstance(main.selectionFilter, QTreeWidget)
        assert main.selectionFilterDock.widget() == main.selectionFilter

    def test_has_properties_editor(self, main):
        assert isinstance(main.propertiesEditor, QTreeView)
        assert main.propertiesEditorDock.widget() == main.propertiesEditor

    def test_has_item_graph(self, main):
        assert isinstance(main.itemGraph, QTreeWidget)
        assert main.itemGraphDock.widget() == main.itemGraph

    def test_hide_all_dock_windows(self, main):
        main.hide_dock_windows()
        assert main.propertiesEditorDock.isHidden()
        assert main.itemGraphDock.isHidden()
        assert main.selectionFilterDock.isHidden()

    def test_show_all_dock_windows(self, main):
        main.show_dock_windows()
        assert not main.propertiesEditorDock.isHidden()
        assert not main.itemGraphDock.isHidden()
        assert not main.selectionFilterDock.isHidden()

#---------------------------------------------------------------------
# initialize MouseModeSelectorWidget
#---------------------------------------------------------------------

# class TestMouseModeSelector:

#     def test_has_mouse_mode_selector(self, main):
#         assert isinstance(main.mouseModeSelector, QWidget)

#     def test_MMS_hidden_on_init(self, main):
#         assert main.mouseModeSelector.isHidden()

#     def test_MMS_toggle_visibility(self, main):
#         assert main.mouseModeSelector.isHidden()
#         main.mouseModeSelector.toggle_visibility()
#         assert main.mouseModeSelector.isVisible()
#         main.mouseModeSelector.toggle_visibility()

#---------------------------------------------------------------------
# initialize undoStack
#---------------------------------------------------------------------

class TestInitUndo:

    def test_has_undo_stack(self, main):
        assert isinstance(main.undoStack, QUndoStack)

    def test_has_undo_view(self, main):
        assert isinstance(main.undoView, QUndoView)

    def test_undo_view_position(self, main):
        pos_offset = main.undoView.pos_offset
        pos = QCursor().pos()


