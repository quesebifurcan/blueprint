from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from line import HorizontalLine
from ramp import Ramp
from textitem import TextItem
from timetext import TimeText
from rectitem import RectItem
from group import Group

from sequential_editors.transition_editor import TransitionEditorDialog 


class TestTransitionEditor:

    ### HELPER FUNCTIONS ###
    
    def select_and_initiate_sequential_editor(self, scene):
        sel = filter(
            lambda x: isinstance(x, scene.item_classes), scene.items())
        for item in sel:
            item.setSelected(True)
        seq = TransitionEditorDialog(scene=scene)
        return seq, sel

    def test_items(self, main):
        for pos in range(20, 400, 20):
            text = RectItem()
            main.scene.addItem(text)
            text.setPos(pos, pos)
        assert True

    ### TESTS ###

    def test_set_scene(self, main):
        seq, sel = self.select_and_initiate_sequential_editor(main.scene)
        assert seq.scene == main.scene

    def test_save_state(self, main):
        seq, sel = self.select_and_initiate_sequential_editor(main.scene)
        assert seq.saved_state
        assert seq.saved_state.items()

    def test_load_saved_state(self, main):
        seq, sel = self.select_and_initiate_sequential_editor(main.scene)
        prev = seq.saved_state.item_value_tuples

    def set_model_data(self, model, row, column, data):
        item = model.itemFromIndex(model.index(row, column))
        item.setData(data)

    def test_get_values(self, main):
        # todo: set value in model externally
        # seq, sel = self.select_and_initiate_sequential_editor(main.scene)

        # seq.current_parameter = 'Color'
        # property_ = 'color'
        # datatype = QColor

        # model = seq.editors.model
        # self.set_model_data(model, 0, 1, QColor('red'))
        # self.set_model_data(model, 1, 1, QColor('blue'))

        # assert seq.current_property == property_
        # assert seq.current_datatype == datatype
        # assert len(seq.get_values()) == len(seq.saved_state.items()) == len(sel)
        pass

    def test_set_properties_sequentially(self, main):
        # todo: set value in model externally
        pass


            


