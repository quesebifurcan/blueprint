from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main
from ..fixtures.load_items_for_grid import test_grid_items

from grid import Grid

class TestMouseInteraction:

    def test_state_is_default(self, main):
        x = Grid()
        y = Grid()
        x.set_orientation('vertical')
        main.scene.addGrid(x)
        main.scene.addGrid(y)
        assert True 

    def test_stretch_items_marker(self, main):
        pass

    def test_insert_items_marker(self, main):
        pass

    def test_enter_insert_mode(self, main):
        pass

    def test_enter_group_mode(self, main):
        pass

    def test_enter_view_mode(self, main):
        pass

    def test_enter_node_node(self, main):
        pass


