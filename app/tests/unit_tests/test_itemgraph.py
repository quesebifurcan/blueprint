from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from ...commands.insert_item_command import InsertItemCommand
from ...commands.move_item_command import MoveItemCommand
from ...commands.add_to_group_command import AddToGroupCommand, RemoveFromGroupCommand
from ...commands.detach_from_parent_group_command import DetachFromParentGroupCommand
from ...commands.scale_item_commands import ResizeFromRightAndScaleCommand

from line import HorizontalLine
from ramp import Ramp
from textitem import TextItem
from timetext import TimeText
from group import Group

class TestItemGraph:
    group = Group()
    subgroup = Group()
    line = HorizontalLine()
    ramp = Ramp()
    text = TextItem()
    timetext = TimeText()

    def test_load_items(self, main):
        group = self.group

        group.addToGroup(self.line)
        group.addToGroup(self.timetext)

        group.addToGroup(self.subgroup)
        self.subgroup.addToGroup(self.ramp)
        self.subgroup.addToGroup(self.text)
        self.text.setPos(100, 100)

        self.group.setPos(20, 50)
        self.subgroup.setPos(20, 50)
        main.scene.addItem(group)

    def test_initial_state(self, main):
        graph = main.itemGraph
        assert not graph.selectedItems()
        assert graph.isEnabled() 

    def test_update_graph(self, main):
        graph = main.itemGraph
        graph.update_graph()
        assert len(graph.get_all_items()) == len(main.scene.items())

    def test_update_graph_on_selection(self, main):
        graph = main.itemGraph
        for item in main.scene.items():
            item.setSelected(True)
        for item in graph.get_all_items():
            assert item.isSelected()

    def test_clear_selection(self, main):
        graph = main.itemGraph
        main.scene.clearSelection()
        for item in graph.get_all_items():
            assert not item.isSelected()

    def test_scene_selection_to_graph_selection(self, main):
        graph = main.itemGraph
        scene_item = self.ramp
        scene_item.setSelected(True)
        items = graph.get_all_items()
        graph_item = filter(lambda x: x.item == self.ramp, items)[0]
        assert graph_item.isSelected()
        scene_item.setSelected(False)
        assert not graph_item.isSelected()

    def test_graph_selection_to_scene_selection(self, main):
        graph = main.itemGraph
        items = graph.get_all_items() 
        graph_item = filter(lambda x: x.item == self.text, items)[0]
        scene_item = self.text
        graph_item.item.setSelected(True)
        assert self.text.isSelected()
        main.scene.clearSelection()

    def test_add_item_and_update(self, main):
        new_ramp = Ramp()
        new_ramp.setPos(200, 200)
        main.scene.addItem(new_ramp)
        items = [x.item for x in main.itemGraph.get_all_items()]
        assert new_ramp in items
        main.scene.removeItem(new_ramp)
        items = [x.item for x in main.itemGraph.get_all_items()]
        assert not new_ramp in items

    def test_deactivate_groups_in_viewmode_and_node_mode(self, main):
        graph = main.itemGraph
        graph.set_groups_disabled(False)
        graph.update_graph()
        groups = filter(lambda x: isinstance(x.item, Group), graph.get_all_items())
        for g in groups:
            assert g.text(0) == "Group"
        graph.set_groups_disabled(True)
        graph.update_graph()
        groups = filter(lambda x: isinstance(x.item, Group), graph.get_all_items())
        for g in groups:
            assert g.text(0) == "Group (inactive)"
            
        
    # def test_update_item_graph_on_init(self, main):
    #     pass

    # def test_update_item_graph_on_selection(self, main):
    #     pass

    # def test_disable_groups_when_not_in_group_mode(self, main):
    #     pass

    # def test_temporary_selection(self, main):
    #     pass

    # def test_reset_item_graph(self, main):
    #     pass



        
