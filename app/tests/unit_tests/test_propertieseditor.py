from PyQt4.QtGui import *
from PyQt4.QtCore import *

import sys

from ..fixtures.app_fixture import main_fixture as main

from properties_editor.properties_editor_widget import PropertiesEditorWidget
from properties_editor.properties_editor_view import PropertiesEditorView

from rectitem import RectItem


class TestPropertiesEditorWidget:

    def test_init(self, main):
        for pos in range(20, 400, 40):
            r = RectItem()
            r.setPos(pos, pos)
            main.scene.addItem(r)
        assert True 

    def test_set_scene(self, main):
        assert main.propertiesEditorWidget
        assert main.propertiesEditorWidget.scene == main.scene

    # def test_init_view(self, main):
    #     assert isinstance(main.propertiesEditorWidget.view, PropertiesEditorView)
     
    # def test_get_selected_items(self, main):
    #     main.scene.select_all()
    #     items = main.scene.all_selected_items()
    #     assert all([item in main.propertiesEditorWidget.selected_items for item in items])

    # def test_get_all_active_properties(self, main):
    #     rectitem_properties = RectItem().properties()
    #     assert set(main.propertiesEditorWidget.active_properties) == \
    #         set(rectitem_properties)

    # def test_set_color(self, main):
    #     main.propertiesEditorWidget.set_data('color', QColor('dodgerblue'))
    #     assert main.propertiesEditorWidget.get_data('color') == QColor('dodgerblue')

    # def test_set_fontfamily(self, main):
    #     main.propertiesEditorWidget.set_data('fontfamily', QFont('Arial'))
    #     assert main.propertiesEditorWidget.get_data('fontfamily') == QFont('Arial')

    # def test_set_linewidth(self, main):
    #     main.propertiesEditorWidget.set_data('linewidth', 12)
    #     assert main.propertiesEditorWidget.get_data('linewidth') == 12

    # def test_set_pointsize(self, main):
    #     main.propertiesEditorWidget.set_data('pointsize', 12)
    #     assert main.propertiesEditorWidget.get_data('pointsize') == 12

    # def test_set_bold(self, main):
    #     main.propertiesEditorWidget.set_data('bold', 2)
    #     assert main.propertiesEditorWidget.get_data('bold') == 2

    # def test_set_italic(self, main):
    #     main.propertiesEditorWidget.set_data('italic', 2)
    #     assert main.propertiesEditorWidget.get_data('italic').toBool()

    # def test_set_underline(self, main):
    #     main.propertiesEditorWidget.set_data('underline', 2)
    #     assert main.propertiesEditorWidget.get_data('underline').toBool()

    # def test_set_underline(self, main):
    #     main.propertiesEditorWidget.set_data('strikeout', 2)
    #     assert main.propertiesEditorWidget.get_data('strikeout').toBool()

    # def test_set_x(self, main):
    #     main.propertiesEditorWidget.set_data('x', 10)
    #     assert main.propertiesEditorWidget.get_data('x') == 10

    # def test_set_y(self, main):
    #     main.propertiesEditorWidget.set_data('y', 10)
    #     assert main.propertiesEditorWidget.get_data('y') == 10

    # def test_set_width(self, main):
    #     main.propertiesEditorWidget.set_data('dx', 10)
    #     assert main.propertiesEditorWidget.get_data('dx') == 10

    # def test_set_height(self, main):
    #     main.propertiesEditorWidget.set_data('dy', 10)
    #     assert main.propertiesEditorWidget.get_data('dy') == 10

    # def test_set_linestyle(self, main):
    #     main.propertiesEditorWidget.set_data('linestyle', 'dash-dot-dot')
    #     assert main.propertiesEditorWidget.get_data('linestyle').toString() == 'dash-dot-dot'

    # def test_set_transparency(self, main):
    #     main.propertiesEditorWidget.set_data('transparency', 50)
    #     assert main.propertiesEditorWidget.get_data('transparency') == 50

    def test_add_grid(self, main):
        from grid import Grid
        new_grid = Grid()
        new_grid.interval = 3
        main.scene.addGrid(new_grid)
        main.propertiesEditorWidget.add_grid(new_grid)
        # new_grid = Grid()
        # main.scene.addGrid(new_grid)
        # main.propertiesEditorWidget.add_grid(new_grid)
        assert main.propertiesEditorWidget.view.grid_editor.rowCount() == 1

    # def test_new_style(self, main):
    #     main.propertiesEditorWidget.view.testing.editor.set_value(2)
    #     assert main.propertiesEditorWidget.view.testing.editor.converted_item_data() == 2
    #     assert True 

    def test_handle_scene_selection_change(self, main):
        pass

    def test_handle_multiple_selected_items(self, main):
        pass

    def test_handle_apply(self, main):
        pass

    def test_show(self, main):
        pass

    def test_hide(self, main):
        pass

    def test_validate_data(self, main):
        pass

    def test_init_grid(self, main):
        pass

    def test_modify_grid(self, main):
        pass

    def test_delete_grid(self, main):
        pass

       
# 1. delegate has set_value
# 2. create editor follows naming convention
# 3. if unconventional, specify in setModelData and setEditorData
# 4. view has direct access (editor as attribute)
