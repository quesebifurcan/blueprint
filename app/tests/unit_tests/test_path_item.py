from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from pathitem import PathItem, PathNode, ControlNode


class TestPathItem:

    line = PathItem()
    x = 60
    y = 30

    def test_init_line(self, main):
        x, y = self.x, self.y
        line = self.line

        main.scene.addItem(line)

        line.setPos(x, y)

        line.add_point(QPointF())
        line.add_point(QPointF(x, y))
        line.add_point(QPointF(2*x, 2*y))

        nodes = line._nodes

        children = line.childItems()

        assert children[0].pos() == QPointF()
        assert children[1].pos() == QPointF(x, y)
        assert children[2].pos() == QPointF(2*x, 2*y)

        ctrl_points = children[0].childItems()

        assert ctrl_points

        assert ctrl_points[0].pos().x() == x / 3
        assert ctrl_points[1].pos().x() == (x / 3) * 2

        assert ctrl_points[0].pos().y() == y / 3
        assert ctrl_points[1].pos().y() == (y / 3) * 2

    def test_copy_path_node(self, main):
        first_node = self.line.nodes[0]
        copy = first_node.copy()
        assert isinstance(copy, PathNode)
        assert not id(first_node) == id(copy)
        assert first_node.childItems()
        assert all([isinstance(node, ControlNode) for node in first_node.childItems()])

    def test_copy_path_item(self, main):
        copy = self.line.copy()
        assert isinstance(copy, PathItem)
        main.scene.addItem(copy)
        copy.setPos(100, 100)

    def test_dx(self, main):
        new_dx = 200
        self.line.dx = new_dx
        assert self.line.dx == new_dx


