from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from ...commands.insert_item_command import InsertItemCommand
from ...commands.move_item_command import MoveItemCommand
from ...commands.add_to_group_command import AddToGroupCommand, RemoveFromGroupCommand
from ...commands.detach_from_parent_group_command import DetachFromParentGroupCommand
from ...commands.scale_item_commands import ResizeFromRightAndScaleCommand

from line import HorizontalLine
from ramp import Ramp
from textitem import TextItem

from ...group import Group

class TestGroupItem:

    group = Group()
    subgroup1 = Group()
    subgroup2 = Group()
    text = TextItem()
    text.setPlainText("testing")
    line = HorizontalLine()
    ramp = Ramp()

    def test_add_item_to_scene(self, main):
        pos = QPoint(20, 20)
        text = self.text
        cmd = InsertItemCommand(main.scene, text, pos)
        main.undoStack.push(cmd)
        assert text in main.scene.items()
        assert text.scenePos().x() == 20

    def test_add_empty_group_to_scene(self, main):
        pos = QPoint(20, 20)
        group = self.group
        cmd = InsertItemCommand(main.scene, group, pos)
        main.undoStack.push(cmd)
        assert group in main.scene.items()
        assert group.scenePos().x() == 20 

    def test_add_item_to_group(self, main):
        pos = QPoint(20, 20)
        group, text = self.group, self.text
        add = AddToGroupCommand(group, text)
        main.undoStack.push(add)
        assert text in group.childItems()
        assert text.scenePos().x() == 20

    def test_remove_item_from_group(self, main):
        group, text = self.group, self.text
        remove = RemoveFromGroupCommand(group, text)
        main.undoStack.push(remove)
        assert not text in group.childItems()
        assert text.scenePos().x() == 20
        assert group.scenePos().x() == 20

    def test_move_from_group_to_group(self, main):
        pos = QPoint(50, 50)
        group, subgroup1, text = self.group, self.subgroup1, self.text
        cmd = InsertItemCommand(main.scene, subgroup1, pos)
        main.undoStack.push(cmd)
        assert subgroup1 in main.scene.items()
        assert subgroup1.pos().x() == 50
        move = AddToGroupCommand(subgroup1, text)
        main.undoStack.push(move)
        assert text in subgroup1.childItems()
        assert text.scenePos().x() == 20

    def test_move_group(self, main):
        text, subgroup1 = self.text, self.subgroup1

        curr_text_pos = text.scenePos()
        curr_group_pos = subgroup1.pos()
        new_group_pos = QPointF(100, 100)
        diff = new_group_pos.x() - curr_group_pos.x()

        move = MoveItemCommand(subgroup1, new_group_pos)
        main.undoStack.push(move)
        assert text.scenePos().x() == curr_text_pos.x() + diff

    def test_add_group_to_group(self, main):
        group, subgroup1, line = self.group, self.subgroup1, self.line
        cmd = AddToGroupCommand(group, line)
        main.undoStack.push(cmd)
        assert line in group.childItems()
        cmd2 = AddToGroupCommand(subgroup1, self.ramp)
        main.undoStack.push(cmd2)
        assert self.ramp in subgroup1.childItems()
        add = AddToGroupCommand(group, subgroup1)
        main.undoStack.push(add)
        assert subgroup1.group() == group

    def test_detach_subgroup(self, main):
        group = self.group
        subgroup1 = self.subgroup1
        move2 = MoveItemCommand(group, QPoint(100, 100))
        main.undoStack.push(move2)

        items = subgroup1.childItems()

        detach = DetachFromParentGroupCommand(subgroup1)
        assert subgroup1 in group.childItems()
        main.undoStack.push(detach)
        assert not subgroup1 in group.childItems()

    def test_move_subgroup_and_reattach_to_temp_group(self, main): 
        group, subgroup1 = self.group, self.subgroup1

        move = MoveItemCommand(subgroup1, QPoint(200, 300))
        main.undoStack.push(move)
        assert subgroup1.scenePos().x() == 200

        add = AddToGroupCommand(group, subgroup1)
        main.undoStack.push(add)
        assert subgroup1.scenePos().x() == 200
        from timetext import TimeText
        time = TimeText()
        self.group.addToGroup(time)
        time.setPos(50, 20)

    def test_resize_child_items_and_scale_from_left(self, main):
        # group = Group()
        # main.scene.addItem(group)
        # group.addToGroup(self.ramp.start)
        pass

    # def test_join_groups(self, main):
    #     group, subgroup1 = self.group, self.subgroup1




        
        

