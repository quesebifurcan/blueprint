from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from line import HorizontalLine
from ramp import Ramp
from textitem import TextItem
from timetext import TimeText
from rectitem import RectItem
from group import Group

from sequential_editors.sequential_editor import SequentialEditorDialog


class TestSequentialEditor:

    ### HELPER FUNCTIONS ###
    
    def select_and_initiate_sequential_editor(self, scene):
        sel = filter(
            lambda x: isinstance(x, scene.item_classes), scene.items())
        for item in sel:
            item.setSelected(True)
        seq = SequentialEditorDialog(scene=scene)
        return seq, sel

    def test_items(self, main):
        for pos in range(20, 400, 20):
            text = RectItem()
            main.scene.addItem(text)
            text.setPos(pos, pos)
        assert True

    ### TESTS ###

    def test_set_scene(self, main):
        seq = SequentialEditorDialog(scene=main.scene)
        assert seq.scene == main.scene

    def test_set_state_counter(self, main):
        seq = SequentialEditorDialog(scene=main.scene)
        value = 42
        seq.set_state_counter(value)
        assert seq.state_counter.editor.value() == value

    def test_save_state(self, main):
        seq, sel = self.select_and_initiate_sequential_editor(main.scene)
        assert seq.saved_state
        assert seq.saved_state.items()

    def test_load_saved_state(self, main):
        seq, sel = self.select_and_initiate_sequential_editor(main.scene)
        prev = seq.saved_state.item_value_tuples

    def test_update_editors(self, main):
        pass
        
    def test_get_values(self, main):
        seq, sel = self.select_and_initiate_sequential_editor(main.scene)
        seq.current_parameter = 'Text'
        seq.editors.update_count(8)
        property_ = 'text'
        datatype = str
        assert seq.current_property == property_
        assert seq.current_datatype == datatype
        for value in seq.get_values():
            if value:
                assert isinstance(value, datatype)


            


