from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from ...textitem import TextItem
from ...commands.insert_item_command import InsertItemCommand
from ...commands.move_item_command import MoveItemCommand

class TestTextItem:

    text = TextItem()

    def test_insert(self, main):
        text = self.text
        pos = QPointF(50, 50)
        cmd = InsertItemCommand(main.scene, text, pos)
        main.undoStack.push(cmd)
        assert text in main.scene.items()
        assert text.pos() == pos
        
