from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from line import HorizontalLine
from ramp import Ramp
from textitem import TextItem
from timetext import TimeText
from group import Group

class TestSelectionFilter:
    group = Group()
    subgroup = Group()
    line = HorizontalLine()
    ramp = Ramp()
    text = TextItem()
    timetext = TimeText()

    def test_load_items(self, main):
        group = self.group

        group.addToGroup(self.line)
        group.addToGroup(self.timetext)

        group.addToGroup(self.subgroup)
        self.subgroup.addToGroup(self.ramp)
        self.subgroup.addToGroup(self.text)
        self.text.setPos(100, 100)

        self.group.setPos(20, 50)
        self.subgroup.setPos(20, 50)
        main.scene.addItem(group)

    def test_init_state(self, main):
        f = main.selectionFilter
        assert main.scene.mouseModes.groups_enabled
        assert not main.scene.mouseModes.nodes_visible
        assert not f.isEnabled()
        for i in f.get_all_items():
            assert i.checkState(0) == 0

    def test_select_all(self, main):
        f = main.selectionFilter
        f.setEnabled(True)
        for item in f.get_all_items():
            assert item.checkState(0) == 0
        f.select_all()
        for item in f.get_all_items():
            assert item.checkState(0) == 2
        f.deselect_all()
        for item in f.get_all_items():
            assert item.checkState(0) == 0
        
    def test_scene_selection_triggers_new_temp(self, main):
        f = main.selectionFilter
        main.scene.mouseModes.set_node_mode()
        assert not main.scene.mouseModes.groups_enabled
        assert main.scene.mouseModes.nodes_visible
        main.scene.select_all()
        for item in f.get_all_items():
            assert item.checkState(0) == 0
        assert len(main.scene.selectedItems()) == len(f.temp_selection)
        main.scene.clearSelection()
        assert not f.temp_selection

    def test_filter_selection_triggers_new_scene_selection(self, main):
        f = main.selectionFilter
        main.scene.mouseModes.set_node_mode()
        assert not main.scene.mouseModes.groups_enabled
        assert main.scene.mouseModes.nodes_visible
        main.scene.select_all()
        cls = self.ramp.__class__.__name__
        ramp_selector = f.findItems(cls, Qt.MatchFixedString)[0]

        assert ramp_selector.checkState(0) == 0
        ramp_selector.setCheckState(0, 2)
        assert ramp_selector.checkState(0) == 2

        assert main.scene.selectedItems() 
        for item in main.scene.selectedItems():
            assert item.__class__.__name__ == cls

        main.scene.clearSelection()
        for item in f.get_all_items():
            assert item.checkState(0) == 0


        
