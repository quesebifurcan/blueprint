from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from ...line import HorizontalLine
from ...commands.insert_item_command import InsertItemCommand
from ...commands.move_item_command import MoveItemCommand

class TestHorizontalLine:

    line = HorizontalLine()

    def get_new_pos(self, item, increment):
        return QPointF(item.pos().x() + increment, item.pos().y())

    def test_insert_line(self, main):
        line = self.line
        pos = QPointF(20, 20)
        cmd = InsertItemCommand(main.scene, line, pos)
        main.undoStack.push(cmd)
        assert line in main.scene.items()
        assert line.pos() == pos

    # def test_move_right_line_node(self, main):
    #     line = self.line
    #     assert line.line().dx() == 10
    #     move = MoveItemCommand(line.end,
    #                            self.get_new_pos(line.end, 400))
    #     main.undoStack.push(move)
    #     line.update_line()
    #     assert line.line().dx() == 410

    # def test_move_left_line_node(self, main):
    #     line = self.line
    #     assert line.line().dx() == 410
    #     move = MoveItemCommand(line.start,
    #                            self.get_new_pos(line.start, 200))
    #     main.undoStack.push(move)
    #     line.update_line()
    #     assert line.line().dx() == 210
        
