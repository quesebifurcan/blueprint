from PyQt4.QtGui import *
from PyQt4.QtCore import *

import utils


class TestColorScale:

    def test_color_to_rgb(self):
        color = QColor('red')
        result = utils.color_to_rgb_tuple(color)
        assert result == (255, 0, 0)

        color = QColor('green')
        result = utils.color_to_rgb_tuple(color)
        assert result == (0, 128, 0)

    def test_rgb_to_color(self):
        rgb = (255, 0, 0)
        result = utils.rgb_to_color(rgb)
        assert result == QColor('red')

    def test_interpolate(self):
        steps = 10
        start, end = 0, 35
        result = utils.interpolate(start, end, steps)
        assert len(result) == steps
        assert result[-1] == end

    def test_interpolate_tuple(self):
        steps = 42
        a = utils.interpolate(10, 20, steps)
        b = utils.interpolate(10, 30, steps)
        c = utils.interpolate(0, 40, steps)
        expected = zip(*[a,b,c])
        result = utils.interpolate_tuples((10, 10, 0), (20, 30, 40), steps)
        assert expected == result

    def test_interpolate_colors(self):
        start = QColor('red')
        end = QColor('blue')
        result = utils.interpolate_colors(start, end, 12)
        assert result[0] == QColor('red')
        assert result[-1] == QColor('blue')

# interpolate color, interpolate numbers
# get_values = interpolate

