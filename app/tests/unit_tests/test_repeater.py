from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from line import HorizontalLine
from ramp import Ramp
from textitem import TextItem
from timetext import TimeText
from rectitem import RectItem
from group import Group

from repeatdialog import RepeatDialog

from ..fixtures.load_items_for_grid import test_grid_items
from utils import find_elt_by_name, set_editor_value
        
class TestRepeater:

    def test_init_repeater_without_scene(self, main):
        rep = RepeatDialog()
        assert isinstance(rep, QDialog)
        assert not rep.scene

    # def test_set_scene(self, main):
    #     main.scene.select_all()
    #     rep = RepeatDialog()
    #     assert not rep.scene

    #     rep.setScene(main.scene)
    #     assert rep.scene == main.scene
    #     assert not rep.added_items

    # def test_add_items(self, main):
    #     item = TextItem() 
    #     main.scene.addItem(item)
    #     rep = RepeatDialog(scene=main.scene)
    #     dx, dy, count = 50, 10, 15 
    #     rep.repeat_item(item, dx, dy, count)

    #     assert rep.added_items

    #     rep.clear_inserted_items()
    #     assert not rep.added_items

    # def test_repeat_group(self, main):
    #     group = find_elt_by_name(main.scene.items(), 'group')
    #     assert group

    #     rep = RepeatDialog(scene=main.scene)
    #     dx, dy, count = 150, 10, 3 
    #     groups = rep.repeat_item(group, dx, dy, count)

    #     assert len(groups) == count 

    #     assert all([isinstance(item, QGraphicsItemGroup) for item in rep.added_items])

    # def test_clear_items(self, main):
    #     item = TextItem() 
    #     main.scene.addItem(item)
    #     rep = RepeatDialog(scene=main.scene)
    #     dx, dy, count = 50, 10, 15 
    #     rep.repeat_item(item, dx, dy, count)

    #     assert rep.added_items

    #     rep.clear_inserted_items()
    #     assert not rep.added_items

    # def test_scale_insert_intervals_exponentially(self, main):
    #     item = TextItem() 
    #     main.scene.addItem(item)
    #     item.setY(300)
    #     rep = RepeatDialog(scene=main.scene)
    #     dx, dy, count = 10, 10, 15 
    #     rep.repeat_item(item, dx, dy, count, x_scale=1.3, y_scale=1.1)

    #     assert rep.added_items
        
    # def test_handle_reset(self, main):
    #     item = TextItem() 
    #     rep = RepeatDialog(scene=main.scene)
    #     dx, dy, count = 10, 10, 15 
    #     items = rep.repeat_item(item, dx, dy, count) 

    #     assert items
    #     for item in items:
    #         assert item.scene() == main.scene
    #     rep.handle_reset()
    #     assert not rep.added_items
    #     for item in items:
    #         assert not item.scene()

    def test_handle_apply(self, main):
        pass

    def test_handle_close(self, main):
        pass

    def test_change_x_interval(self, main):
        pass

    def test_change_y_interval(self, main):
        pass

    # def test_repeat_item(self, main):
    #     text = RectItem()
    #     main.scene.addItem(text)

    #     pos = 20
    #     count = 30
    #     item_count = len(main.scene.items())

    #     repeat_item(main.scene, text, dx=pos, dy=pos, count=count)
    
        

