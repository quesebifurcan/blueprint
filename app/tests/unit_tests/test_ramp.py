from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ...commands.insert_item_command import InsertItemCommand
from ...commands.add_child_item_command import AddChildItemCommand
from ...commands.move_item_command import MoveItemCommand
from ...commands.set_property_command import SetPropertyCommand
from ...commands.scale_item_commands import (ScaleVerticallyCommand,
                                             ResizeFromRightAndScaleCommand,
                                             ResizeFromLeftAndScaleCommand)

from ..fixtures.app_fixture import main_fixture as main
from ...ramp import Ramp, RampNode 


class TestRamp:

    ramp = Ramp()

    def get_new_pos(self, item, increment):
        return QPointF(item.pos().x() + increment, item.pos().y())

    def test_insert_ramp(self, main):
        ramp = self.ramp
        pos = QPointF(20, 200)
        cmd = InsertItemCommand(main.scene, ramp, pos)
        main.undoStack.push(cmd)
        assert ramp in main.scene.items()
        assert ramp.pos() == pos

    def test_move_right_ramp_node(self, main):
        ramp = self.ramp
        curr_dx = ramp.dx()
        amount = 200
        move = MoveItemCommand(ramp.end,
                               self.get_new_pos(ramp.end, amount))
        main.undoStack.push(move)
        assert ramp.dx() == curr_dx + amount

    def test_add_ramp_node(self, main):
        ramp = self.ramp
        cmd = AddChildItemCommand(ramp, RampNode(), QPoint(400, -50))
        main.undoStack.push(cmd)

    def test_ramp_bend_factor(self, main):
        ramp = self.ramp
        node = self.ramp.childItems()[2]
        cmd = SetPropertyCommand(
            RampNode.set_bend, node, node.bend(), 0)
        main.undoStack.push(cmd)
        assert node.bend() == 0

    def test_ramp_move(self, main):
        ramp = self.ramp
        pos = QPointF(20, 200)
        move = MoveItemCommand(ramp, self.get_new_pos(ramp, 40))
        main.undoStack.push(move)
        assert ramp.pos() == QPointF(60, 200)

    def test_scale_ramp_vertically(self, main):
        ramp = self.ramp
        curr_height = ramp.height
        cmd = ScaleVerticallyCommand(ramp, 20)
        main.undoStack.push(cmd)
        assert ramp.height == curr_height + 20

    # def test_resize_from_right_and_scale(self, main):
    #     ramp = self.ramp
    #     curr_dx = ramp.dx()
    #     cmd = ResizeFromRightAndScaleCommand(ramp, 400)
    #     main.undoStack.push(cmd)
    #     assert ramp.dx() == curr_dx + 400

    # def test_resize_from_left_and_scale(self, main):
    #     ramp = self.ramp
    #     curr_dx = ramp.dx()
    #     cmd = ResizeFromLeftAndScaleCommand(ramp, 100)
    #     main.undoStack.push(cmd)
    #     assert ramp.dx() == curr_dx - 100
    
    def test_reparent_item_to_ramp_node(self, main):
        pass
        
