from __future__ import division

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main
from ..fixtures.load_items_for_grid import test_grid_items

from line import HorizontalLine, LineNode
from textitem import TextItem
from timetext import TimeText
from ramp import Ramp, RampNode

def find_elt_by_name(lst, name):
    for elt in lst:
        if hasattr(elt, 'name'):
            if elt.name == name:
                return elt
    return None 

class TestScene:

    item_classes = (HorizontalLine, TextItem, TimeText, Ramp)

    def test_scale_item_position(self, main):
        pass
    
    def test_set_pixels_per_second_scales_items(self, main):
        scale = 6
        assert main.scene.pixels_per_second == 1
        pos = []
        items = [find_elt_by_name(main.scene.items(), item)
                 for item in 'text timetext line'.split()]
        for item in items:
            pos.append(item.scenePos().x())
        main.scene.set_pixels_per_second(main.scene.pixels_per_second * scale)
        for prev_pos, item in zip(pos, items):
            curr_pos = int(item.scenePos().x())
            assert int(prev_pos) == curr_pos / scale 
            
    def test_delete_items(self, main):
        line = find_elt_by_name(main.scene.items(), 'line')
        rect = find_elt_by_name(main.scene.items(), 'rect')
        text = find_elt_by_name(main.scene.items(), 'text')

        line.dx = 200
        rect.width = 100
        rect.height = 100

        text.text = "testing"

        assert line in main.scene.items()
        assert rect in main.scene.items()

        # line.delete()
        # assert not line in main.scene.items()
        
        # rect.delete()
        # assert not rect in main.scene.items()

        # text.delete()
        # assert not text in main.scene.items() 
