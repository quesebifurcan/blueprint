from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main
from ..fixtures.load_items_for_grid import test_grid_items

from scene_states.paste_items_state import PasteItemsState
from scene_states.default_state import DefaultState
from scene import item_classes

### HELPER FUNCTIONS ###

def find_elt_by_name(lst, name):
    for elt in lst:
        if hasattr(elt, 'name'):
            if elt.name == name:
                return elt
    return None 

### TESTS ###


class TestPasteSelection:

    def test_set_paste_state(self, main):
        main.scene.state = PasteItemsState
        assert isinstance(main.scene.state, PasteItemsState)
        main.scene.state = DefaultState

    def test_copy_and_paste_items(self, main):

        group = find_elt_by_name(main.scene.items(), 'group')
        group.setSelected(True)

        copy = main.scene.copy_selection()
        assert isinstance(copy, QGraphicsItemGroup)

        pos = QPointF(210, 300)
        main.scene.paste_to_position(copy, pos)

        assert copy.pos() == pos

