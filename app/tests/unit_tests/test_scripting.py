from PyQt4.QtGui import *
from PyQt4.QtCore import *

import os

from ..fixtures.app_fixture import main_fixture as main
from rectitem import RectItem

class TestScript:

    script_path = '/'.join([os.path.dirname(os.path.realpath(__file__)), 'scripts'])

    def test_items(self, main):
        for pos in range(20, 400, 20):
            text = RectItem()
            main.scene.addItem(text)
            text.setPos(pos, pos)
        assert True

    def test_set_properties(self, main):
        script = '/'.join([self.script_path, 'color_red.py'])
        execfile(script, {'scene': main.scene})
        items = filter(lambda x: isinstance(x, main.scene.item_classes),
                       main.scene.items())
        for item in items:
            assert item.color == QColor('red')
