from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main
from scene_items.line import HorizontalLine
from scene_items.ramp import Ramp
from scene_items.textitem import TextItem
from scene_items.timetext import TimeText
from scene_items.rectitem import RectItem
from scene_items.group import Group


class TestCopyItems:

    def test_copy_horizontal_line(self, main):
        line = HorizontalLine()
        line.dx = 100
        line.dy = 34
        line.color = QColor('red')

        copy = line.copy()
        assert copy.dx == 100
        assert copy.dy == 34
        assert copy.color == QColor('red')
        main.scene.addItem(copy)
        copy.setPos(20, 30)

    def test_copy_ramp(self, main):
        dx, dy, color = 200, 50, QColor('grey')

        ramp = Ramp()
        ramp.dx = dx
        ramp.dy = dy
        ramp.color = color
        ramp.setPos(200, 200)
        main.scene.addItem(ramp)

        copy = ramp.copy()

        main.scene.addItem(copy)

        assert copy.dx == dx
        assert copy.dy == dy
        assert copy.color == color
        assert ramp.pos() == copy.pos()
        assert ramp.scenePos() == copy.scenePos()
        assert ramp.nodes
        assert copy.nodes
        
    # def test_copy_textitem(self, main):
    #     string = "testing..."
    #     fontfamily = "Helvetica"
    #     fontsize = 12.93
    #     bold = True
    #     italic = True

    #     text = TextItem()
    #     text.text = string
    #     text.fontfamily = fontfamily
    #     text.fontsize = fontsize
    #     text.bold = bold
    #     text.italic = italic

    #     copy = text.copy()

    #     assert copy.text == string
    #     assert copy.fontfamily == fontfamily
    #     assert copy.fontsize == fontsize
    #     assert copy.bold == bold
    #     assert copy.italic == italic
        
    # def test_copy_timetext(self, main):
    #     fontfamily = "Helvetica"
    #     fontsize = 12.93
    #     bold = True
    #     italic = True

    #     text = TimeText()
    #     text.fontfamily = fontfamily
    #     text.fontsize = fontsize
    #     text.bold = bold
    #     text.italic = italic

    #     copy = text.copy()

    #     assert copy.fontfamily == fontfamily
    #     assert copy.fontsize == fontsize
    #     assert copy.bold == bold
    #     assert copy.italic == italic
        
    # def test_copy_rect_item(self, main):
    #     pos, dx, dy = 30, 300, 400

    #     rect = RectItem()

    #     rect.setPos(pos, pos)
    #     rect.dx = dx
    #     rect.dy = dy

    #     copy = rect.copy()

    #     assert copy.pos() == QPointF(pos, pos)
    #     assert copy.dx == dx
    #     assert copy.dy == dy

    #     main.scene.addItem(copy)

    # def test_copy_group(self, main):

    #     def new_group():
    #         group = Group()
    #         ramp, rect, line, text, timetext = (
    #             Ramp(), RectItem(), HorizontalLine(), TextItem(), TimeText())

    #         line.dx = 200
    #         line.dy = 200

    #         pos, width, height = 30, 300, 400
    #         rect.setPos(pos, pos)
    #         rect.width = width
    #         rect.height = height

    #         offset = 30
    #         for item in (line, ramp, rect, text, timetext):
    #             group.addToGroup(item)
    #             item.setPos(offset, offset)
    #             offset += 50
    #         return group

    #     group = new_group()
    #     subgroup = new_group()

    #     group.addToGroup(subgroup)
    #     # group.setPos(200, 200)
    #     subgroup.setPos(200, 200)

    #     main.scene.addItem(group)

    #     copy = group.copy()

    #     assert isinstance(copy, Group)
    #     assert group.pos() == copy.pos()
    #     assert group.scenePos() == copy.scenePos()
    #     assert len(group.get_all_items()) == len(copy.get_all_items())

        
    
