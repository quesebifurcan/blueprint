from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

from line import HorizontalLine
from ramp import Ramp
from textitem import TextItem
from timetext import TimeText
from group import Group
from rectitem import RectItem
from layoutmarkers import PageMarker, SystemMarker, LayoutText

class TestLayoutMarkers:

    group = Group()
    subgroup = Group()
    line = HorizontalLine()
    ramp = Ramp()
    text = TextItem()
    timetext = TimeText()
    rect = RectItem()
    border = HorizontalLine()

    page_marker1 = PageMarker()
    page_marker2 = PageMarker()
    page_marker3 = PageMarker()

    system_marker1 = SystemMarker()
    system_marker2 = SystemMarker()
    system_marker3 = SystemMarker()

    def test_load_items(self, main):
        group = self.group

        group.addToGroup(self.line)
        group.addToGroup(self.timetext)

        group.addToGroup(self.subgroup)
        self.subgroup.addToGroup(self.ramp)
        self.subgroup.addToGroup(self.text)
        self.text.setPos(100, 100)

        self.group.setPos(20, 50)
        self.subgroup.setPos(20, 50)
        main.scene.addItem(group)

        self.group.addToGroup(self.rect) 

    def test_add_page_marker(self, main):
        self.page_marker1.setPos(20, 0)
        self.page_marker2.setPos(320, 0)
        self.page_marker3.setPos(400, 0)
        main.scene.addItem(self.page_marker1)
        main.scene.addItem(self.page_marker2)
        main.scene.addItem(self.page_marker3)
        assert self.page_marker1 in main.scene.items()
        assert self.page_marker1 in main.scene.markers
        assert self.page_marker2 in main.scene.items()
        assert self.page_marker1 in main.scene.markers

    def test_add_system_marker(self, main):
        self.system_marker1.setPos(100, 0)
        self.system_marker2.setPos(180, 0)
        self.system_marker3.setPos(500, 0)
        main.scene.addItem(self.system_marker1)
        main.scene.addItem(self.system_marker2)
        main.scene.addItem(self.system_marker3)
        assert self.system_marker1 in main.scene.items()
        assert self.system_marker1 in main.scene.markers
        assert self.system_marker2 in main.scene.items()
        assert self.system_marker2 in main.scene.items()

    def test_marker_has_text(self, main):
        assert isinstance(self.page_marker1.text, LayoutText)
        assert isinstance(self.system_marker1.text, LayoutText)

    def test_update_marker_borders(self, main):
        main.scene.update_markers()
        main.scene.update_borders()

    # def test_update_marker_names(self, main):
    #     main.scene.update_marker_names()
    #     markers = [self.page_marker1, self.page_marker2, self.page_marker3,
    #                self.system_marker1, self.system_marker2, self.system_marker3]
    #     markers = sorted(markers, key=lambda m: m.pos().x())
    #     systems = filter(lambda m: isinstance(m, SystemMarker), markers)
    #     pages = list(set(markers) - set(systems))

    #     assert self.page_marker1.text.toPlainText() == "1.1"
    #     assert self.page_marker2.text.toPlainText() == "2.1"
    #     assert self.page_marker3.text.toPlainText() == "3.1"

    #     assert self.system_marker1.text.toPlainText() == "1.2"
    #     assert self.system_marker2.text.toPlainText() == "1.3"
    #     assert self.system_marker3.text.toPlainText() == "3.2"

    def test_page_view(self, main):
        assert isinstance(main.pageview, PageView)
        
    # def test_draw_vertical_boundaries(self, main):
    #     top = main.scene.view_border_top
    #     bottom = main.scene.view_border_bottom
    #     # main.view.setSceneRect(0, 15, 2000, 2000)
    #     r = main.view.sceneRect()
    #     assert r.height() == bottom.pos().y()

    # def test_get_vertical_boundaries(self, main):
    #     r = main.scene.itemsBoundingRect()
    #     print r

    def test_page_markers_divide_scene(self, main):
        r = main.view.sceneRect()
        pass

    def test_system_markers_divide_page(self, main):
        pass

    def test_create_page_views(self, main):
        pass

    def test_system_markers_divide_scene(self, main):
        pass

    def test_has_page_view_widget(self, main):
        pass

    def test_build_page_views_from_page_markers(self, main):
        pass

    def test_build_page_views_from_page_and_system_markers(self, main):
        pass

    def test_remove_empty_space_and_stretch_page_view_vertically(self, main):
        pass


        

    

