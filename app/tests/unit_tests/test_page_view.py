from __future__ import division

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main
from ..fixtures.load_items_for_page_view import test_pageview_items


class TestLayoutSettings:

    def test_scene_rect_equals_border(self, main):

        main.set_printable_scene_rect(0, 0, 8000, 800)
        
        b = main.scene.scene_border
        r = main.scene.sceneRect()

        assert b.top.line().dx() == r.width()
        assert b.top.line().dy() == 0
        assert b.bottom.line().dx() == r.width()
        assert b.bottom.line().dy() == 0
        assert b.left.line().dy() == r.height()
        assert b.left.line().dx() == 0
        assert b.right.line().dy() == r.height()
        assert b.right.line().dx() == 0

    def test_margins(self, main):
        pw = main.pageview
        pw.printer = QPrinter()
        assert pw.margins == pw.printer.pageRect().height() / 100

    def test_set_page_layout(self, main):
        pw = main.pageview
        systems_per_page = 3
        pw.set_page_layout(QPrinter.A4, QPrinter.Landscape, systems_per_page)
        assert pw.printer.pageSize() == QPrinter.A4
        assert pw.printer.orientation() == QPrinter.Landscape
        assert pw.systems_per_page == systems_per_page
        assert pw.target_system_height == pw.printer.pageRect().height() \
            / pw.systems_per_page


class TestCalculateSourceAndTargetRects:

    def test_set_target_system_height(self, main):
        pw = main.pageview
        p = pw.printer
        target_system_height = (p.pageRect().height() / pw.systems_per_page)
        assert main.pageview.target_system_height == target_system_height

    def test_set_source_rect_width_and_keep_ratio_intact(self, main):
        pw = main.pageview
        p = pw.printer
        target_ratio = pw.target_system_height / p.pageRect().width()
        source_ratio = main.scene.sceneRect().height() / pw.source_rect_width 
        assert target_ratio == source_ratio

    def test_source_rects_from_auto_divisions(self, main):
        pw = main.pageview
        pw.autoset_page_markers()
        rects = pw.source_rects_from_auto_divisions()
        for r in rects:
            assert r.width() == rects[0].width()
             
    def test_autoset_page_markers(self, main):
        pw = main.pageview
        systems_per_page = 3
        pw.autoset_page_markers(systems_per_page)
        assert pw.systems_per_page == systems_per_page
        assert main.scene.markers
        assert len(main.scene.markers) - 1 == len(pw.source_rects)

    def test_target_system_width(self, main):
        pw = main.pageview
        printer = QPrinter()
        width = printer.pageRect().width()
        pw.printer = printer
        assert pw.target_system_width == width

    def test_export_pdf(self, main):
        pw = main.pageview
        filename = "test.pdf"
        pw.autoset_page_markers(systems_per_page=3)
        pw.export_pdf(filename)
        assert isinstance(pw.printer, QPrinter)
        assert pw.printer.outputFileName() == filename

    def test_split_scene_into_source_rects(self, main):
        pw = main.pageview
        pw.autoset_page_markers(systems_per_page=3)
        r = pw.source_rects[0]
        source_ratio = r.height() / r.width()
        target_ratio = pw.target_system_height / pw.target_system_width
        hi = max([source_ratio, target_ratio])
        lo = min([source_ratio, target_ratio])
        assert (hi - lo) < 0.000001

    def test_group_source_rects(self, main):
        pw = main.pageview
        systems_per_page = 4
        pw.set_page_layout(QPrinter.A4, QPrinter.Landscape, systems_per_page)
        pw.autoset_page_markers(systems_per_page=systems_per_page)
        assert pw.systems_per_page == systems_per_page
        assert main.scene.markers
        assert pw.source_rects


class TestBuildPageViewWidget:

    def test_init_pageview_widget(self, main):
        pw = main.pageview
        systems_per_page = 3
        pw.set_page_layout(QPrinter.A4, QPrinter.Portrait, systems_per_page)
        pw.autoset_page_markers(systems_per_page)
        pw.render_to_image()
        pw.export_pdf("test.pdf")
        assert isinstance(pw, QGraphicsView)
        main.tabWidget.setCurrentIndex(1)

    def test_background_color(self, main):
        assert main.pageview.backgroundBrush() == QBrush(QColor('Gainsboro'))

    def test_grouper(self):
        from pageview import grouper
        assert list(grouper(3, [0,1,2,3,4,5,6,7,8,9])) == \
            [(0,1,2), (3,4,5), (6,7,8), (9,None,None)]



    

    


        
