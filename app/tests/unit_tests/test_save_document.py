from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main

import json
from save_and_load import Encoder, Decoder

from line import HorizontalLine, LineNode
from ramp import Ramp
from group import Group

### HELPER FUNCTIONS ###

def find_elt_by_name(lst, name):
    for elt in lst:
        if hasattr(elt, 'name'):
            if elt.name == name:
                return elt
    return None 

def save_and_load_json(obj, mode='w', encoder=Encoder, decoder=Decoder):
    with open('data.txt', mode) as outfile:
        json.dump(obj, outfile, cls=Encoder, indent=4)
    with open('data.txt', 'r') as infile:
        objects = json.load(infile, cls=Decoder)
    return objects

### TESTS ###

def test_(main):
    assert True

# class TestHorizontalLineProperties:

#     def test_horizontal_line(self, main):
#         line = find_elt_by_name(main.scene.items(), 'line')
#         line.color = QColor('dodgerblue')
#         line.dx = 100
#         line.dy = 30
#         line.linewidth = 5
#         line.linestyle = 3
#         line.capstyle = Qt.RoundCap
#         result = save_and_load_json(line)

#         assert isinstance(result, HorizontalLine)
#         assert result.dx == 100
#         assert result.dy == 30
#         assert result.linewidth == 5
#         assert result.linestyle == 3
#         assert result.capstyle == Qt.RoundCap

#         end = result.end
#         assert isinstance(end, LineNode)
#         assert end.pos().x() == line.dx
#         assert end.pos().y() == line.dy

#     def test_ramp(self, main):
#         ramp = find_elt_by_name(main.scene.items(), 'ramp')
#         ramp.color = QColor('dodgerblue')
#         ramp.dx = 300
#         ramp.dy = 98
#         result = save_and_load_json(ramp)

#         assert isinstance(result, Ramp)
#         assert result.dx == 300
#         assert result.dy == 98
#         assert ramp.color == QColor('dodgerblue')

#     def test_textitem(self, main):
#         text = find_elt_by_name(main.scene.items(), 'text')
#         text.setPos(20, 40)
#         text.text = "testing"
#         text.fontfamily = QFont("Arial").family()
#         text.fontsize = 12.3
#         text.bold = True
#         text.italic = True
#         text.underline = True
#         text.strikeout = True
#         text.color = QColor('green')

#         result = save_and_load_json(text)

#         assert result.pos() == QPointF(20, 40)
#         assert result.text == "testing"
#         assert result.fontfamily == QFont("Arial").family()
#         assert result.fontsize == 12.3
#         assert result.bold
#         assert result.italic
#         assert result.underline
#         assert result.strikeout
#         assert result.color == QColor('green')

#     def test_decode_child_items(self, main):
#         group = {'class': 'Group',
#                  'child_items': [{'class': 'HorizontalLine'},
#                                  {'class': 'Group',
#                                   'child_items': [{'class': 'HorizontalLine',
#                                                    'pos': [20, 50]
#                                                }]
#                                   }]
#                  }
#         from save_and_load import Decoder
#         decoder = Decoder()
#         result = decoder.decode(group)
#         assert isinstance(result, Group)
#         result.setPos(200, 300)
#         main.scene.addItem(result)

#     # def test_rect_item(self, main):
#     #     rect = find_elt_by_name(main.scene.items(), 'rect')
#     #     assert rect
                                  
#     def test_group(self, main):
#         group = find_elt_by_name(main.scene.items(), 'group')
#         children = group.childItems()

#         result = save_and_load_json(group)

#         assert isinstance(result, Group)

#         main.scene.addItem(result)
#         result.setPos(250, 250)
#         assert len(children) == len(result.childItems()), (result.childItems(), children)

#     def test_load_settings_from_file(self, main):
#         save = main.save_settings('settings.txt')
#         load = main.restore_settings('settings.txt')
#         assert save.value("geometry").toByteArray() == load.value("geometry").toByteArray()
#         assert save.value("windowState").toByteArray() == load.value("windowState").toByteArray()

#     def test_encode_scene(self, main):
#         from save_and_load import SceneEncoder, SceneDecoder
#         items = main.scene.items()

#         from save_and_load import SceneEncoder
#         from sceneborder import SceneBorder, SceneBorderLine

#         with open('data.txt', 'w') as outfile:
#             json.dump(main.scene, outfile, cls=SceneEncoder, indent=2)

#         main.scene.delete_items()
#         for item in main.scene.items():
#             assert isinstance(item, (SceneBorder, SceneBorderLine))

#         with open('data.txt', 'r') as infile:
#             result = json.load(infile, cls=Decoder)

#         for item in result:
#             main.scene.addItem(item)

    # def test_build_scene_items_from_file(self, main):
    #     pass

    # def test_read_from_file(self, main):
    #     pass

    # def test_load_empty_scene(self, main):
    #     pass

    # def test_save_scene(self, main):
    #     pass


