from PyQt4.QtGui import *
from PyQt4.QtCore import *

from ..fixtures.app_fixture import main_fixture as main
from ..fixtures.load_items_for_grid import test_grid_items

from grid import Grid, HorizontalGridLine, VerticalGridLine, GridText
from utils import format_time_string


class TestGrid:

    grid = Grid()

    def test_scene_has_grids_container(self, main):
        assert hasattr(main.scene, 'grids') 

    def test_add_grid(self, main):
        main.scene.addGrid(self.grid)
        items = main.scene.items()
        assert self.grid.scene == main.scene


    # def test_set_grid_orientation_vertical(self, main):
    #     self.grid.set_orientation('vertical')
    #     assert self.grid.orientation == 'vertical'
    #     items = main.scene.items()
    #     for line in self.grid.lines:
    #         assert isinstance(line, VerticalGridLine)
    #         assert line in items

    # def test_remove_lines_from_scene(self, main):
    #     lines = self.grid.lines
    #     self.grid.remove_lines_from_scene()
    #     items = main.scene.items()
    #     for line in lines:
    #         assert not line in items
    #     assert not self.grid.lines

    # def test_set_grid_orientation_horizontal(self, main):
    #     assert self.grid.orientation == 'vertical'
    #     self.grid.set_orientation('horizontal')
    #     assert self.grid.orientation == 'horizontal'
    #     items = main.scene.items()
    #     for line in self.grid.lines:
    #         assert isinstance(line, HorizontalGridLine)
    #         assert line in items

    # def test_set_line_interval(self, main):
    #     interval = self.grid.interval - 20
    #     multiplier = 3
    #     curr_count = len(self.grid.lines)
    #     self.grid.set_interval(interval)

    #     assert self.grid.interval == interval
    #     assert self.grid.lines[multiplier].pos().y() == interval * 3
    #     assert not len(self.grid.lines) == curr_count

    # def test_remove_grid_from_scene(self, main):
    #     lines = self.grid.lines
    #     main.scene.removeGrid(self.grid)
    #     items = main.scene.items()
    #     assert not self.grid in main.scene.grids
    #     assert not self.grid.scene
    #     for line in lines:
    #         assert not line in items

    # def test_set_orientation_without_drawing_lines(self, main):
    #     self.grid.set_orientation('horizontal')
    #     # lines should not be initiated since grid.scene is not set
    #     assert not self.grid.lines
    #     main.scene.addGrid(self.grid)
    #     assert self.grid.lines

    # def test_set_grid_color(self, main):
    #     color = QColor('Gainsboro')
    #     self.grid.set_color(color)
    #     for line in self.grid.lines:
    #         assert line.pen().color() == color

    # def test_set_grid_line_style(self, main):
    #     linestyle = Qt.DashDotLine
    #     self.grid.set_linestyle(linestyle)
    #     for line in self.grid.lines:
    #         assert line.pen().style() == linestyle

    # def test_select_grid(self, main):
    #     self.grid.setSelected(True)
    #     color = self.grid.selection_color
    #     for line in self.grid.lines:
    #         assert line.pen().color() == color
    #     self.grid.setSelected(False)
    #     color = self.grid.color
    #     for line in self.grid.lines:
    #         assert line.pen().color() == color

    # def test_change_grid_visibility(self, main):
    #     self.grid.set_line_visibility(False)
    #     for line in self.grid.lines:
    #         assert not line.isVisible()
    #     self.grid.set_line_visibility(True)
    #     for line in self.grid.lines:
    #         assert line.isVisible()

    # def test_grid_is_in_background(self, main):
    #     for line in self.grid.lines:
    #         line.setSelected(True)
    #         assert not line.isSelected()

    # def test_only_vertical_lines_have_textitems(self, main):
    #     self.grid.set_orientation('horizontal')
    #     for line in self.grid.lines:
    #         assert not hasattr(line, 'text')
    #     self.grid.set_orientation('vertical')
    #     for line in self.grid.lines:
    #         assert hasattr(line, 'text')

    # def test_set_textitem_visibility(self, main):
    #     assert self.grid.orientation == 'vertical'
    #     self.grid.set_textitem_visibility(False)
    #     for line in self.grid.lines:
    #         assert not line.textitem_is_visible

    # def test_all_vertical_lines_have_textitem_children(self, main):
    #     self.grid.set_orientation('vertical')
    #     self.grid.set_textitem_visibility(True)
    #     for line in self.grid.lines:
    #         assert isinstance(line.text, GridText)
    #         assert line.text.parentItem() == line
    #         assert isinstance(line, VerticalGridLine)

    # def test_pixel_based_textitem_contents_correspond_to_line_position(self, main):
    #     assert self.grid.orientation == 'vertical'
    #     for line in self.grid.lines:
    #         assert line.text.toPlainText() == str(int(line.pos().x()))
        
    # def test_vertical_lines_extend_above_scene_rect(self, main):
    #     extension = 10
    #     self.grid.set_line_extension(extension)
    #     for line in self.grid.lines:
    #         assert line.line().dy() == main.scene.sceneRect().height() + extension

    # def test_centered_text(self, main):
    #     assert self.grid.text_is_centered
    #     extension = 4
    #     self.grid.set_line_extension(extension)
    #     for line in self.grid.lines:
    #         rect = line.text.boundingRect()
    #         assert line.text.pos().x() == -(rect.width() / 2)

    # def test_orientation_change_hides_textitems(self, main):
    #     self.grid.set_orientation('horizontal')
    #     assert self.grid.lines
    #     for line in self.grid.lines:
    #         assert isinstance(line, HorizontalGridLine)
    #         assert line.line().dx() == main.scene.sceneRect().width()
    #         assert not hasattr(line, 'text')

    # def test_set_is_time_based(self, main):
    #     self.grid.set_interval(100)
    #     main.scene.pixels_per_second = 5
    #     self.grid.set_orientation('vertical')
    #     self.grid.set_time_based(True)
    #     for line in self.grid.lines:
    #         print line.pos().x()
    #     assert self.grid.is_time_based
    #     for line in self.grid.lines:
    #         s = format_time_string(line.pos().x(), main.scene.pixels_per_second)
    #         assert line.is_time_based
    #         assert line.text.toPlainText() == s

    # def test_unset_is_time_based(self, main):
    #     self.grid.set_time_based(False)
    #     assert self.grid.line_extension == 0
    #     self.grid.set_line_extension(-400)
    #     for line in self.grid.lines:
    #         assert line.text.toPlainText() == str(int(line.pos().x()))
