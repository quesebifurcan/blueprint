from PyQt4.QtGui import *
from PyQt4.QtCore import *

from app_fixture import main_fixture as main
from scene_items.line import HorizontalLine
from scene_items.ramp import Ramp
from scene_items.textitem import TextItem
from scene_items.timetext import TimeText
from scene_items.group import Group
from scene_items.rectitem import RectItem


def test_pageview_items(main):

    def items(offset=0):
        group = Group()
        subgroup = Group()
        line = HorizontalLine()
        ramp = Ramp()
        text = TextItem()
        timetext = TimeText()
        rect = RectItem()
        border = HorizontalLine()

        group.addToGroup(line)
        group.addToGroup(timetext)

        group.addToGroup(subgroup)
        subgroup.addToGroup(ramp)
        subgroup.addToGroup(text)
        text.setPos(100, 100)
        rect.setPos(150, 20)

        group.setPos(20, 50)
        subgroup.setPos(20, 50)
        main.scene.addItem(group)

        group.addToGroup(rect) 
        group.setPos(offset, 100)

    for x in range(0, 7000, 350):
        items(x)
