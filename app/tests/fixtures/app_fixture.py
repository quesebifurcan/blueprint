import os
import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

import pytest
from mainwindow import MainWindow

@pytest.fixture(scope='module')
def main_fixture(request, items_to_load=None):
    app = QApplication(sys.argv)
    w = MainWindow()
    if items_to_load:
        for fn in items_to_load:
            fn(w)
    w.show()
    w.activateWindow()
    w.raise_()
    # w.undoView.show()
    def fin():
        app.exec_()
    request.addfinalizer(fin)
    return w




