from __future__ import division

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from inspect import getmembers, ismethod
import struct

def format_time_string(s, pixel_scale):
    import math

    s = s / pixel_scale
    minutes = math.floor(s / 60)
    seconds = s - (minutes * 60)

    return """{minutes}\'{seconds}\"""".format(minutes=int(minutes),
                                               seconds=str(int(seconds)).zfill(2))

def move_item_horizontally(item, amount):
    new_x = item.pos().x() + (-1 * amount)
    item.setPos(new_x, item.pos().y())

def rotate(l,n=1):
    return l[n:] + l[:n]

def color_to_rgb_tuple(color):
    string = str(color.name()).strip('#')
    rgb = struct.unpack('BBB', string.decode('hex'))
    return rgb

def rgb_to_color(rgb):
    string = struct.pack('BBB', *rgb).encode('hex')
    return QColor('#' + string)
    
def interpolate(start, end, steps):
    steps = steps - 1
    step_size = (end - start) / steps

    coll = [start]
    for i in range(steps):
        start += step_size
        coll.append(start)

    return coll

def interpolate_tuples(start, end, steps):
    assert len(start) == len(end)
    ranges = zip(*[start, end])
    coll = []
    for r in ranges:
        coll.append(interpolate(r[0], r[1], steps))
    return zip(*coll)

def interpolate_colors(start, end, steps):
    start = color_to_rgb_tuple(start)
    end = color_to_rgb_tuple(end)
    rgbs = interpolate_tuples(start, end, steps)
    colors = [rgb_to_color(rgb) for rgb in rgbs]
    return colors
    
def find_elt_by_name(lst, name):
    for elt in lst:
        if hasattr(elt, 'name'):
            if elt.name == name:
                return elt
    return None 

def set_editor_value(editor, value):
    editor.blockSignals(True)
    editor.setValue(value)
    editor.blockSignals(False)

def grouper(n, iterable, fillvalue=None):
    import itertools
    args = [iter(iterable)] * n
    return itertools.izip_longest(fillvalue=fillvalue, *args)

def page_size_to_page_view_paper_size(r):
    return QRectF(0, 0, r.width() / 10, r.height() / 10)
