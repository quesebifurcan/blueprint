import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *


class DockWidget(QDockWidget):

    def __init__(self, parent=None):
        super(DockWidget, self).__init__(parent)
        self.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)


class ItemEditorWidget(QScrollArea):

    def __init__(self, parent=None):
        super(ItemEditorWidget, self).__init__(parent)

        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.setContentsMargins(0, 0, 0, 0)

