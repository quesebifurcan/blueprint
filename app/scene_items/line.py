import sys
import time

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.baseitem import BaseItem, NodeItem


class LineNode(QGraphicsPathItem, BaseItem):

    def __init__(self, parent=None):
        super(LineNode, self).__init__(parent)

        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setAcceptHoverEvents(True)

        pen = QPen()
        color = QColor('crimson')
        color.setAlpha(128)
        pen.setColor(color)
        pen.setWidth(2)
        self.setPen(pen)
        self.capstyle = Qt.RoundCap

        self.init_path()
        self.hide()

    def init_path(self):
        path = QPainterPath()
        dm = 8
        path.addEllipse((dm / 2) * -1,
                        (dm / 2) * -1,
                        dm,
                        dm)
        self.setPath(path)

    def properties(self):
        return ['pos']

    def copy(self):
        instance = LineNode()
        instance.setPos(self.pos())
        return instance

    def boundingRect(self):
        r = super(LineNode, self).boundingRect()
        w = 4
        return QRectF(r.x()-w, r.y()-w, r.width()+(w*2), r.height()+(w*2)) 

    def mouseMoveEvent(self, e):
        super(LineNode, self).mouseMoveEvent(e)
        if e.modifiers() == Qt.ShiftModifier:
            self.setY(self.curr_y)
        elif e.modifiers() == Qt.ControlModifier:
            self.setX(self.curr_x)
        self.scene().check_item_positions(self)

    def mousePressEvent(self, e):
        if self.group():
            self.scene().set_groups_selectable(False)
        self.curr_x = self.pos().x()
        self.curr_y = self.pos().y()
        super(LineNode, self).mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        if self.group():
            self.scene().set_groups_selectable(True)
        super(LineNode, self).mouseReleaseEvent(e)

    def check_position(self):
        self.setPos(self.pos().x(), 0)


class HorizontalLine(QGraphicsPathItem, BaseItem, NodeItem):

    def __init__(self,
                 color=QColor('black'),
                 transparency=255,
                 linestyle=Qt.SolidLine,
                 linewidth=3,
                 capstyle=Qt.RoundCap,
                 parent=None,
                 nodes=None):
        super(HorizontalLine, self).__init__(parent)

        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setAcceptHoverEvents(True)

        self.default_dx = 20
        self._nodes = []
        self.nodes = self._nodes

        self.color = color
        self.transparency = transparency
        self.linewidth = linewidth
        self.linestyle = linestyle
        self.capstyle = capstyle

    def copy(self, new_pos=None):

        instance = HorizontalLine()

        for p in self.properties():
            if not p == 'pos':
                value = getattr(self, p)
                setattr(instance, p, value)

        if new_pos:
            instance.setPos(new_pos)
        else:
            instance.setPos(self.pos())

        return instance

    ### PROPERTIES ###

    def properties(self):
        return 'pos color transparency linestyle linewidth nodes'.split()

    def get_start_and_end_points(self):
        nodes = sorted(self._nodes, key=lambda x: x.pos().x())
        return nodes[0], nodes[1]

    def remove_nodes(self):
        for node in self._nodes:
            node.setParentItem(None)
            if node.scene():
                node.scene().removeItem(node)
        self._nodes = []

    def init_default_nodes(self):
        self.nodes = LineNode(self), LineNode(self)
        for node in self._nodes:
            node.setParentItem(self)
        self.start, self.end = self.get_start_and_end_points()
        self.end.setPos(self.default_dx, 0)

    @property
    def dx(self):
        return self.end.pos().x() - self.start.pos().x()

    @dx.setter
    def dx(self, value):
        start_x = self.start.pos().x()
        y = self.end.pos().y()
        self.end.setPos(start_x + value, y)

    @property
    def dy(self):
        return self.end.pos().y() - self.start.pos().y()

    @dy.setter
    def dy(self, value):
        start_y = self.start.pos().y()
        x = self.end.pos().x()
        self.end.setPos(x, start_y + value)

    def paint(self, a, b, c):
        pen = self.pen()
        if not self.isSelected():
            self.border_color = 'black'
        else:
            self.border_color = 'black'
        # pen.setColor(QColor(self.border_color))
        self.setPen(pen)
        path = QPainterPath()
        path.moveTo(self.start.pos())
        path.lineTo(self.end.pos())
        self.setPath(path)
        super(HorizontalLine, self).paint(a,b,c)

    def mousePressEvent(self, e):
        if self.group():
            e.setAccepted(True)
            self.mousePressedPos = e.scenePos()
        else:
            super(HorizontalLine, self).mousePressEvent(e)
        # self.resize_self_from_right_and_scale(5)

    def mouseMoveEvent(self, e):
        if self.group():
            diff_X = e.scenePos().x() - self.mousePressedPos.x()
            diff_Y = e.scenePos().y() - self.mousePressedPos.y()
            self.setPos(self.pos().x() + diff_X, 
                        self.pos().y() + diff_Y)
            e.setAccepted(True) # avoid propagating event to parent
            self.mousePressedPos = e.scenePos()
        else:
            super(HorizontalLine, self).mouseMoveEvent(e)

    def resize_from_right_and_scale(self, ratio):
        self.dx = self.dx * ratio
        x = self.pos().x()
        self.setPos(x * ratio, self.y())

    def resize_from_top_to_bottom(self, ratio):
        self.dy = self.dy * ratio
        y = self.pos().y()
        self.setPos(self.pos().x(), y * ratio)
