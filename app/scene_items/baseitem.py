import sys
import unittest 
import time

from PyQt4.QtGui import *
from PyQt4.QtCore import *


class BaseItem(QGraphicsItem):

    def __init__(self, parent=None):
        super(BaseItem, self).__init__(parent)

        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        # self.setCacheMode(QGraphicsItem.ItemCoordinateCache)

    def set_font_property(self, fn, value):
        font = self.font()
        fn(font, value)
        self.setFont(font) 

    @property
    def transparency(self):
        return self.color.alpha()

    @transparency.setter
    def transparency(self, value=255):
        color = self.color
        color.setAlpha(value)
        self.color = color

    @property
    def color(self):
        return self.pen().color()

    @color.setter
    def color(self, value):
        pen = self.pen()
        pen.setColor(QColor(value))
        self.setPen(pen) 
        if hasattr(self, 'setBrush'):
            self.setBrush(QBrush(self.pen().color()))

    ### LINE ###

    @property
    def linewidth(self):
        return self.pen().width()

    @linewidth.setter
    def linewidth(self, value):
        pen = self.pen()
        pen.setWidth(value)
        self.setPen(pen)

    @property
    def linestyle(self):
        return self.pen().style()

    @linestyle.setter
    def linestyle(self, value):
        pen = self.pen()
        pen.setStyle(value)
        self.setPen(pen)

    @property
    def capstyle(self):
        return self.pen().capStyle()

    @capstyle.setter
    def capstyle(self, value):
        pen = self.pen()
        pen.setCapStyle(value)
        self.setPen(pen)

    ### TEXT ###

    @property
    def text(self):
        return self.toPlainText()

    @text.setter
    def text(self, string):
        self.setPlainText(string)

    @property
    def fontfamily(self):
        return self.font().family()

    @fontfamily.setter
    def fontfamily(self, value):
        self.set_font_property(QFont.setFamily, value)

    @property
    def fontsize(self):
        return self.font().pointSizeF()

    @fontsize.setter
    def fontsize(self, value):
        self.set_font_property(QFont.setPointSizeF, value)

    @property
    def bold(self):
        return self.font().bold()

    @bold.setter
    def bold(self, value):
        self.set_font_property(QFont.setBold, value)

    @property
    def italic(self):
        return self.font().italic()

    @italic.setter
    def italic(self, value):
        self.set_font_property(QFont.setItalic, value)

    @property
    def underline(self):
        return self.font().underline()

    @underline.setter
    def underline(self, value):
        self.set_font_property(QFont.setUnderline, value)

    @property
    def strikeout(self):
        return self.font().strikeOut()

    @strikeout.setter
    def strikeout(self, value):
        self.set_font_property(QFont.setStrikeOut, value)


class NodeItem(QGraphicsItem):

    def __init__(self, parent=None):
        super(NodeItem, self).__init__(parent)

    @property
    def nodes(self):
        return self._nodes

    @nodes.setter
    def nodes(self, nodes):
        if not nodes:
            self.init_default_nodes()
        else:
            self.remove_nodes()
            for node in nodes:
                cp = node.copy()
                cp.setPos(node.pos())
                cp.setParentItem(self)
                self._nodes.append(cp)
            self.start, self.end = self.get_start_and_end_points()

    def delete(self):
        if self.scene():
            group = self.group()
            scene = self.scene()
            self.scene().removeItem(self)
            assert not self in scene.items()
            if group and len(group.childItems()) -1 <= 1:
                for child in group.childItems():
                    child.setParentItem(group.parentItem())
                scene.removeItem(group)

    def hoverEnterEvent(self, e):
        e.setAccepted(True)
        for node in self.nodes:
            node.show()

    def hide_nodes_when_grouped(self):
        selected = self.group().isSelected()
        for node in self.nodes:
            node.setSelected(False)
            node.update()
            node.hide()
        self.group().setSelected(selected)

    def get_all_nodes(self):
        result = []
        for node in self.nodes:
            result.append(node)
            children = node.childItems()
            if children:
                for child in children:
                    result.append(child)
        return result

    def hide_nodes_when_top_level(self):
        if not any([node.isSelected() for node in self.get_all_nodes()]):
            if not self.isSelected():
                for node in self.nodes:
                    node.setSelected(False)
                    node.update()
                    node.hide()

    def hoverLeaveEvent(self, e):
        if self.group():
            self.hide_nodes_when_grouped()
        else:
            self.hide_nodes_when_top_level()
