from __future__ import division

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.line import HorizontalLine, LineNode
from scene_items.baseitem import BaseItem, NodeItem


class SignalObject(QObject):

    signal = pyqtSignal()
     
    def __init__(self, parent=None):
        super(SignalObject, self).__init__(parent)

    def emit(self):
        self.signal.emit()


class PathNode(LineNode):

    itemSelectedChanged = SignalObject()

    def __init__(self,
                 color=QColor('black'),
                 transparency=80,
                 parent=None):

        super(PathNode, self).__init__(parent)

        self.color=color
        self.transparency=transparency

    def properties(self):
        return ['pos', 'color', 'transparency']

    def copy(self):
        instance = PathNode()
        instance.setPos(self.pos())
        for child in self.childItems():
            copy = child.copy()
            copy.setPos(child.pos())
            copy.setParentItem(instance)
            copy.show()
        return instance

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemSelectedHasChanged:
            if not value.toBool():
                self.itemSelectedChanged.emit()
        result = super(PathNode, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result


class ControlNode(LineNode):

    itemSelectedChanged = SignalObject()

    def __init__(self,
                 color=QColor('dodgerblue'),
                 transparency=80,
                 parent=None):

        super(ControlNode, self).__init__(parent)

        self.diameter=6
        self.color=color
        self.transparency=transparency
        self.init_path()
        self.show()

    def init_path(self):
        path = QPainterPath()
        dm = 6
        path.addEllipse((dm / 2) * -1,
                        (dm / 2) * -1,
                        dm,
                        dm)
        self.setPath(path)

    def properties(self):
        return ['pos', 'color', 'transparency']

    def copy(self):
        instance = ControlNode()
        instance.setPos(self.pos())
        return instance

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemSelectedHasChanged:
            if not value.toBool():
                self.itemSelectedChanged.emit()
        result = super(ControlNode, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result


class PathItem(QGraphicsPathItem, BaseItem, NodeItem):

    def __init__(self,
                 color=QColor('black'),
                 transparency=255,
                 linestyle=Qt.SolidLine,
                 linewidth=2,
                 capstyle=Qt.RoundCap,
                 parent=None):
        super(PathItem, self).__init__(parent)

        self._nodes = []
        self.color=QColor('black')
        self.transparency=transparency
        self.linestyle=linestyle
        self.linewidth=linewidth
        self.capstyle = Qt.RoundCap
        self.setAcceptHoverEvents(True)

    ### PROPERTIES ###

    def properties(self):
        return ['pos', 'nodes']

    @property
    def nodes(self):
        return self._nodes

    @nodes.setter
    def nodes(self, nodes):
        if nodes:
            for node in nodes:
                cp = node.copy()
                cp.setPos(node.pos())
                # connect signals
                cp.itemSelectedChanged.signal.connect(self.check_node_visibility)
                for child in cp.childItems():
                    child.itemSelectedChanged.signal.connect(self.check_node_visibility)
                cp.setParentItem(self)
                self._nodes.append(cp)

    @property
    def dx(self):
        return self.path().controlPointRect().width()

    @dx.setter
    def dx(self, value):
        ratio = value / self.dx
        for node in self.nodes:
            x = node.pos().x() * ratio
            y = node.pos().y()
            node.setPos(x, y)
            for control_node in node.childItems():
                x = control_node.pos().x() * ratio
                control_node.setX(x)
        self.calculate_path()

    @property
    def dy(self):
        return self.path().controlPointRect().height()

    @dy.setter
    def dy(self, value):
        ratio = value / self.dy
        for node in self.nodes:
            x = node.pos().x()
            y = node.pos().y() * ratio
            node.setPos(x, y)
            for control_node in node.childItems():
                y = control_node.pos().y() * ratio
                control_node.setY(y)
        self.calculate_path()

    ### PUBLIC ###

    def copy(self, new_pos=None):
        instance = PathItem()
        for p in self.properties():
            if not p == 'pos':
                value = getattr(self, p)
                setattr(instance, p, value)
        if new_pos:
            instance.setPos(new_pos)
        else:
            instance.setPos(self.pos())
        return instance

    def bounding_rect_contains_cursor_position(self):
        scene = self.scene()
        view = scene.view
        view_pos = view.mapFromGlobal(QCursor().pos())
        scene_pos = view.mapToScene(view_pos)
        return self.sceneBoundingRect().contains(scene_pos)

    def check_node_visibility(self):
        if not self.bounding_rect_contains_cursor_position():
            self.hide_nodes_when_top_level()

    def remove_nodes(self):
        for node in self._nodes:
            node.setParentItem(None)
            if node.scene():
                node.scene().removeItem(node)
        self._nodes = []

    def add_point(self, pos):
        point = PathNode(parent=self)
        point.itemSelectedChanged.signal.connect(self.check_node_visibility)
        point.setPos(pos)
        self._nodes.append(point)
        self.add_control_points(point)
        self.calculate_path()

    def add_control_points(self, point):
        if len(self._nodes) > 1:
            curr_index = self._nodes.index(point)
            curr = point
            prev = self._nodes[curr_index - 1]
            x_mul = (curr.scenePos().x() - prev.scenePos().x()) / 3
            y_mul = (curr.scenePos().y() - prev.scenePos().y()) / 3
            x_points = x_mul * 1 + prev.scenePos().x(), x_mul * 2 + prev.scenePos().x()
            y_points = y_mul * 1 + prev.scenePos().y(), y_mul * 2 + prev.scenePos().y()

            for x,y in zip(x_points, y_points):
                control_point = ControlNode()
                control_point.setParentItem(prev)
                pos = prev.mapFromScene(QPointF(x,y))
                control_point.setPos(pos)
                control_point.itemSelectedChanged.signal.connect(self.check_node_visibility)

    def calculate_path(self):
        self.setBrush(QBrush(Qt.transparent))
        path = QPainterPath()
        for curr, nxt in zip(self._nodes[:],
                             self._nodes[1:]):
            path.moveTo(curr.pos())
            control_points = curr.childItems()
            path.cubicTo(self.mapFromScene(control_points[0].scenePos()),
                         self.mapFromScene(control_points[1].scenePos()),
                         self.mapFromScene(nxt.scenePos()))
        self.setPath(path)

    def resize_from_right_and_scale(self, ratio):
        self.dx = self.dx * ratio
        x = self.pos().x()
        self.setPos(x * ratio, self.y())

    def resize_from_top_to_bottom(self, ratio):
        self.dy = self.dy * ratio
        y = self.pos().y()
        self.setPos(self.pos().x(), y * ratio)

    ### OVERRIDDEN ###

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemSelectedHasChanged:
            if not value.toBool():
                self.check_node_visibility()
        result = super(PathItem, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result

    def boundingRect(self):
        return self.childrenBoundingRect()

    def paint(self, a, b, c):
        self.calculate_path()
        super(PathItem, self).paint(a,b,c)
        
    def mousePressEvent(self, e):
        for node in self._nodes:
            print node.pos()
        for node in self._nodes:
            print node.scenePos()

