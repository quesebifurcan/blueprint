from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.textitem import TextItem


class LayoutText(TextItem):

    def __init__(self, parent=None):
        super(LayoutText, self).__init__(parent)
        self.setFlag(QGraphicsItem.ItemIsMovable, False)
        self._color = QColor('red')
        self.set_color(self._color)
        self.prints = False
        self.hide()

    def color(self):
        return self._color

    def set_color(self, color):
        self._color = QColor(color)
        self.setDefaultTextColor(self._color)

    def mousePressEvent(self, e):
        self.parentItem().mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        self.parentItem().mouseReleaseEvent(e)

    def mouseMoveEvent(self, e):
        self.parentItem().mouseMoveEvent(e)


class PageMarker(QGraphicsLineItem):

    def __init__(self, color='brown', parent=None):
        super(PageMarker, self).__init__(parent)
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)

        self.base_color = QColor(color)
        self.selection_color = QColor('DodgerBlue')
        self._color = QColor(color)
        pen = QPen()
        pen.setWidth(3)
        # pen.setCapStyle(Qt.RoundCap)
        # pen.setColor(self._color)
        self.setLine(0, -15, 0, 2000)
        self.set_color(self._color)
        self.prints = False
        self.init_text()

    def update_line(self, height):
        self.setLine(0, -15, 0, height)

    def init_text(self):
        self.text = LayoutText()
        self.text.setFlag(QGraphicsItem.ItemIsSelectable, False)
        self.text.setPlainText("Page 2")
        self.text.setParentItem(self)
        self.text.setPos(self.pos().x() + 1,
                         self.pos().y() + -20)
        self.text.set_color(self.color())

    def color(self):
        return self._color

    def paint(self, a, b, c):
        if self.isSelected():
            self.set_color(self.selection_color)
            self.text.set_color(self.selection_color)
        else:
            self.set_color(self.base_color)
            self.text.set_color(self.base_color)
        super(PageMarker, self).paint(a, b, c)

    def mousePressEvent(self, e):
        super(PageMarker, self).mousePressEvent(e)

    def set_color(self, color):
        self._color = color
        pen = self.pen()
        pen.setColor(self.color())
        self.setPen(pen) 

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemPositionChange:
            pos = value.toPointF()
            if pos.x() < 0:
                pos.setX(0)
            pos.setY(self.pos().y())
            value = pos
            if self.scene():
                self.scene().update_markers()
        if change == QGraphicsItem.ItemSceneHasChanged:
            scene = self.scene()
            if scene:
                if not self in scene.markers:
                    scene.markers.append(self)
        result = super(PageMarker, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result


class SystemMarker(PageMarker):

    def __init__(self, color='coral', parent=None):
        super(SystemMarker, self).__init__(color, parent)
        self.text.setPlainText("System 1")
        self.base_color = QColor(color)
        self.selection_color = QColor('LightBlue')
        pen = self.pen()
        pen.setStyle(Qt.DashLine)
        self.setPen(pen)

