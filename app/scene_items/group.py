from PyQt4.QtGui import *
from PyQt4.QtCore import *


class GroupBorderRect(QGraphicsRectItem):

    def __init__(self, parent=None):
        super(GroupBorderRect, self).__init__(parent)
        self.setAcceptHoverEvents(False)
        self.setFlag(QGraphicsItem.ItemIsMovable, False)
        self.setFlag(QGraphicsItem.ItemIsSelectable, False)
        self.setZValue(-130)
        self.setRect(QRectF())

    def boundingRect(self):
        return QRectF()

    def paint(self, painter, style, widget):
        pen = self.pen()
        parent = self.parentItem()
        if parent:
            self.setRect(parent.boundingRect())
            if not parent.isSelected():
                color = 'black'
                pen.setDashPattern([3,3])
                pen.setWidth(1.5)
                pen.setColor(QColor(color))
            else:
                color = 'black'
                pen.setDashPattern([6,6])
                pen.setWidth(1.5)
                pen.setColor(QColor(color))
            self.setPen(pen)
            super(GroupBorderRect, self).paint(painter, style, widget)

    def mousePressEvent(self, e):
        self.parentItem().mousePressEvent(e)

    def mouseMoveEvent(self, e):
        self.parentItem().mouseMoveEvent(e)

    def mouseReleaseEvent(self, e):
        self.parentItem().mouseReleaseEvent(e)


class Group(QGraphicsItemGroup):

    def __init__(self, parent=None):
        super(Group, self).__init__(parent)

        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setHandlesChildEvents(False) # lets interactions slip through to children
        self.setZValue(-5) 
        self.rep = GroupBorderRect()
        self.rep.setParentItem(self)

        self.temp_group = None

        # self.setAcceptHoverEvents(True)
        self.border_color = 'black'
        self.paint_borders = True
        self.stretch = False

    def properties(self):
        return ['pos',
                'child_items']

    def copy(self):
        instance = Group()
        for item in self.childItems():
            if isinstance(item, self.scene().item_classes):
                copy = item.copy()
                instance.addToGroup(copy)
                copy.setPos(item.pos())
        return instance

    def move_to_scene(self):
        while self.group():
            self.group().removeFromGroup(self)

    def temporarily_detach_from_parent_group(self):
        self.scene().temp = self # keep self alive when reparented
        self.temp_group = self.group()
        self.move_to_scene()

    def mousePressEvent(self, e):
        self.click_pos = e.scenePos()
        if self.group():
            self.temporarily_detach_from_parent_group()
        else:
            super(Group, self).mousePressEvent(e)

    def mouseMoveEvent(self, e):
        if e.modifiers() == Qt.ShiftModifier:
            curr_pos = e.scenePos()
            x_diff = curr_pos.x() - self.click_pos.x()
            y_diff = curr_pos.y() - self.click_pos.y()
            self.click_pos = curr_pos
            self.scene().activate_stretch_cursor()
            self.resize_from_right_and_scale(x_diff)
            self.resize_from_top_to_bottom(y_diff)
        else:
            self.scene().reset_cursor()
            super(Group, self).mouseMoveEvent(e)
        
    def mouseReleaseEvent(self, e):
        self.scene().reset_cursor()
        if self.temp_group:
            self.temp_group.addToGroup(self)
            self.temp_group = None
        super(Group, self).mouseReleaseEvent(e)

    def paint(self, a, b, c):
        if hasattr(self, 'rep'):
            self.rep.paint(a, b, c)

    def boundingRect(self):
        return self.childrenBoundingRect()

    def get_all_items(self):

        def collect(item, coll):
            children = item.childItems()
            coll.append(item)
            if children:
                for child in children:
                    collect(child, coll)
            return coll

        coll = []
        return collect(self, coll)

    def resize_from_right_and_scale(self, amount):
        stretch = 240
        ratio = (stretch + amount) / stretch

        children = self.get_all_items()
        children = filter(lambda x: isinstance(x, self.scene().item_classes), children)

        for child in children:
            if not isinstance(child, (QGraphicsItemGroup)):
                if hasattr(child, 'resize_from_right_and_scale'):
                    child.resize_from_right_and_scale(ratio)
                elif hasattr(child, 'relative_horizontal_move'):
                    child.relative_horizontal_move(ratio)

        for child in children:
            if child.group() and child.parentItem() and child.pos().x() > 0:
                if isinstance(child, QGraphicsItemGroup):
                    pos = child.pos()
                    child.setX(pos.x() * ratio)

    def resize_from_top_to_bottom(self, amount):
        stretch = 240
        ratio = (stretch + amount) / stretch
        y_pos = self.sceneBoundingRect().y()

        children = self.get_all_items()

        for child in children:
            if not isinstance(child, QGraphicsItemGroup):
                if hasattr(child, 'resize_from_top_to_bottom'):
                    child.resize_from_top_to_bottom(ratio)
                elif hasattr(child, 'relative_vertical_move'):
                    child.relative_vertical_move(ratio)

        for child in children:
            if child.group() and child.parentItem() and child.pos().y() > 0:
                if isinstance(child, QGraphicsItemGroup):
                    pos = child.pos()
                    child.setY(pos.y() * ratio)

                




    
