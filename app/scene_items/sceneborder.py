from PyQt4.QtGui import *
from PyQt4.QtCore import *


class SceneBorderLine(QGraphicsLineItem):

    def __init__(self, parent=None):
        super(SceneBorderLine, self).__init__(parent)
        pen = self.pen()
        pen.setColor(QColor('black'))
        pen.setWidth(2)
        self.setPen(pen)


class SceneBorder(QGraphicsItem):

    def __init__(self, scene=None, parent=None):
        super(SceneBorder, self).__init__(parent)
        self.scene = scene
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, False)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges, False)
        self.setFlag(QGraphicsItem.ItemIsMovable, False)
        self.setFlag(QGraphicsItem.ItemIsSelectable, False)
        self.setEnabled(False)
        self.setZValue(-300)

        self.init_lines()
        self.draw_lines()

    def init_lines(self):
        self.left = SceneBorderLine()
        self.right = SceneBorderLine()
        self.top = SceneBorderLine()
        self.bottom = SceneBorderLine()
        for line in (self.left, self.right, self.top, self.bottom):
            self.scene.addItem(line)

    def draw_lines(self):
        r = self.scene.sceneRect()
        x, y, width, height = r.x(), r.y(), r.width(), r.height()
        self.left.setLine(x, y, x, height+y)
        self.right.setLine(width, y, width, height+y)
        self.top.setLine(x, y, width, y)
        self.bottom.setLine(x, height+y, width, height+y)

    def paint(self, painter, style, widget):
        pass

    def boundingRect(self):
        return QRectF()


