from PyQt4.QtGui import *
from PyQt4.QtCore import *

class RectNode(QGraphicsPathItem):

    def __init__(self):
        super(RectNode, self).__init__()

        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setAcceptHoverEvents(True)

        pen = QPen()
        pen.setColor(QColor('crimson'))
        pen.setWidth(2)
        self.setPen(pen)

        path = QPainterPath()
        dm = 6
        path.addEllipse((dm / 2) * -1,
                        (dm / 2) * -1,
                        dm,
                        dm)
        self.setPath(path)
        self.hide()
        self.restrain = False

    def boundingRect(self):
        r = super(RectNode, self).boundingRect()
        w = 4
        return QRectF(r.x()-w, r.y()-w, r.width()+(w*2), r.height()+(w*2)) 

    def setPos(self, pos):
        super(RectNode, self).setPos(pos)

    def mouseMoveEvent(self, e):
        super(RectNode, self).mouseMoveEvent(e)

    def mousePressEvent(self, e):
        self.restrain = True
        if self.group():
            self.scene().set_groups_selectable(False)
        super(RectNode, self).mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        self.restrain = False
        if self.group():
            self.scene().set_groups_selectable(True)
        super(RectNode, self).mouseReleaseEvent(e)

    def check_position(self):
        self.setPos(self.pos().x(), 0)

class HRectNode(RectNode):

    def __init__(self, parent=None):
        super(HRectNode, self).__init__()

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemPositionChange:
            if self.restrain:
                pos = value.toPointF()
                pos.setX(self.pos().x())
                value = pos
        result = super(RectNode, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result

class VRectNode(RectNode):

    def __init__(self, parent=None):
        super(VRectNode, self).__init__()

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemPositionChange:
            if self.restrain:
                pos = value.toPointF()
                pos.setY(self.pos().y())
                value = pos
        result = super(RectNode, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result

from scene_items.baseitem import BaseItem

class RectItem(QGraphicsRectItem, BaseItem):

    def __init__(self, parent=None):
        super(RectItem, self).__init__(parent)

        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)

        self.setZValue(-10)

        self.setAcceptHoverEvents(True)
        pen = QPen()
        pen.setStyle(Qt.DashDotLine)
        self._color = QColor('black')
        pen.setWidth(1)
        pen.setCapStyle(Qt.RoundCap)
        pen.setColor(self._color)
        self._x_incr = 0
        self.setPen(pen)
        self.setRect(0, 0, 20, 20)
        self.init_nodes()

    def properties(self):
        return ['pos',
                'color',
                'x',
                'y',
                'dx',
                'dy',
                'linestyle',
                'linewidth',
                'capstyle', 
                'transparency']

    def get_grid_points(self):
        if self.scene() and self.scene().grids:
            grid_points = []
            for grid in self.scene().grids:
                for line in grid.lines:
                    grid_points.append(line.pos().x())
            return grid_points

    def copy(self, new_pos=None):
        instance = RectItem()
        for p in self.properties():
            if not p == 'pos':
                value = getattr(self, p)
                setattr(instance, p, value)
        if new_pos:
            instance.setPos(new_pos)
        else:
            instance.setPos(self.pos())
        return instance

    @property
    def color(self):
        return self.pen().color()

    @color.setter
    def color(self, value):
        pen = self.pen()
        pen.setColor(QColor(value))
        self.setPen(pen) 

    @property
    def x(self):
        return self.pos().x()

    @x.setter
    def x(self, value):
        self.setPos(value, self.pos().y())

    @property
    def y(self):
        return self.pos().y()

    @y.setter
    def y(self, value):
        self.setPos(self.pos().x(), value)

    @property
    def x_incr(self):
        return self._x_incr

    @x_incr.setter
    def x_incr(self, value):
        # self.setPos(self.pos().x() - self._x_incr,
        #             self.pos().y())
        if not value == self.x_incr:
            self._x_incr = value
            self.setPos(self.pos().x() + value,
                        self.pos().y())

    @property
    def dx(self):
        return self.rect().width()

    @dx.setter
    def dx(self, value):
        rect = self.rect()
        rect.setWidth(value)
        self.setRect(rect)
        self.update_node_positions()

    @property
    def dy(self):
        return self.rect().height()

    @dy.setter
    def dy(self, value):
        rect = self.rect()
        rect.setHeight(value)
        self.setRect(rect)
        self.update_node_positions()

    def init_nodes(self):
        self.top, self.bottom = HRectNode(), HRectNode()
        self.left, self.right = VRectNode(), VRectNode()
        for node in [self.top, self.bottom, self.left, self.right]:
            node.setParentItem(self)
        self.update_node_positions()

    def hoverEnterEvent(self, e):
        e.setAccepted(True)
        for node in self.childItems():
            if isinstance(node, RectNode):
                node.show()

    def get_nodes(self):
        return filter(lambda child: isinstance(child, RectNode), self.childItems())

    def hide_nodes_when_grouped(self):
        selected = self.group().isSelected()
        for node in self.get_nodes():
            node.update()
            node.hide()
        self.group().setSelected(selected)

    def hide_nodes_when_top_level(self):
        nodes = self.get_nodes()
        if not any([node.isSelected() for node in nodes]):
            for node in nodes:
                node.setSelected(False)
                node.update()
                node.hide()

    def hoverLeaveEvent(self, e):
        if self.group():
            self.hide_nodes_when_grouped()
        else:
            self.hide_nodes_when_top_level()

    def update_node_positions(self):
        r = self.rect()
        top_pos = QPointF((r.width() / 2) + r.x(),
                          r.y())
        bottom_pos = QPointF((r.width() / 2) + r.x(),
                             r.y() + r.height())
        left_pos = QPointF(r.x(),
                           (r.height() / 2) + r.y())
        right_pos = QPointF(r.x() + r.width(),
                           (r.height() / 2) + r.y())
        self.top.setPos(top_pos)
        self.bottom.setPos(bottom_pos)
        self.left.setPos(right_pos)
        self.right.setPos(left_pos) 

    def paint(self, a, b, c):
        h = sorted([self.left, self.right], key=lambda point: point.x())
        v = sorted([self.top, self.bottom], key=lambda point: point.y())
        self.right, self.left = h
        self.top, self.bottom = v
        x = h[0].pos().x()
        y = v[0].pos().y()
        width = h[1].pos().x() - h[0].pos().x()
        height = v[1].pos().y() - v[0].pos().y()
        self.setRect(x, y, width, height)
        super(RectItem, self).paint(a,b,c)
        self.update_node_positions()

    def mousePressEvent(self, e):
        if self.group():
            e.setAccepted(True)
            self.mousePressedPos = e.scenePos()
        else:
            super(RectItem, self).mousePressEvent(e)
        # self.resize_self_from_right_and_scale(5)

    def mouseMoveEvent(self, e):
        if self.group():
            diff_X = e.scenePos().x() - self.mousePressedPos.x()
            diff_Y = e.scenePos().y() - self.mousePressedPos.y()
            self.setPos(self.pos().x() + diff_X, 
                        self.pos().y() + diff_Y)
            e.setAccepted(True) # avoid propagating event to parent
            self.mousePressedPos = e.scenePos()
        else:
            super(RectItem, self).mouseMoveEvent(e)

    def resize_from_right_and_scale(self, ratio):
        self.dx = self.dx * ratio
        x = self.pos().x()
        self.setPos(x * ratio, self.pos().y()) 

    def resize_from_top_to_bottom(self, ratio):
        self.dy = self.dy * ratio
        pos = self.pos()
        self.setPos(pos.x(), pos.y() * ratio)

    def scale_vertically(self, amount):
        height = self.boundingRect().height() + amount
        for node in self.childItems():
            if node in (self.top,) and not node.pos().y() == height:
                node.setPos(QPointF(node.pos().x(), (node.pos().y() - amount)))

