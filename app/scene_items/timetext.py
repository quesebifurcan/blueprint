import sys
import unittest 
import time

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.baseitem import BaseItem
from utils import format_time_string

import sip

from scene_items.line import LineNode


class TimeTextAnchor(LineNode):

    def __init__(self, parent=None):
        super(TimeTextAnchor, self).__init__(parent)
        self.textitem = TimeText()
        self.textitem.setParentItem(self)
        self.setZValue(1)
        self.show()
        self.setOpacity(0.001)
        self.draw_ellipse()
        color = QColor('dodgerblue')
        color.setAlpha(128)
        self.color = color

    def draw_ellipse(self):
        path = QPainterPath()
        dm = 6
        path.addEllipse((dm / 2) * -1,
                        (dm / 2) * -1,
                        dm,
                        dm)
        self.setPath(path)

    def relative_horizontal_move(self, ratio):
        pos = self.pos()
        self.setPos(pos.x() * ratio, pos.y())

    def relative_vertical_move(self, ratio):
        pos = self.pos()
        self.setPos(pos.x(), pos.y() * ratio)

    def copy(self):
        cp = TimeTextAnchor()
        cp.setPos(self.pos())
        return cp

    def mousePressEvent(self, e):
        # e.setAccepted(True)
        # super(TimeTextAnchor, self).mousePressEvent(e)
        pass



class TimeText(QGraphicsTextItem, BaseItem):

    def __init__(self, parent=None):
        super(TimeText, self).__init__(parent)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        self.setFlag(QGraphicsItem.ItemIgnoresParentOpacity)
        self.setAcceptHoverEvents(True)

        self.setZValue(-100)

        self.update_time_string()
        self.set_font()

    @property
    def color(self):
        return self.defaultTextColor()

    @color.setter
    def color(self, value):
        self.setDefaultTextColor(value)

    def properties(self):
        return ('pos',
                'color',
                'text',
                'fontfamily',
                'fontsize',
                'bold',
                'italic',
                'transparency',
                'underline',
                'strikeout')

    def check_position(self):
        pass

    def itemChange(self, change, value):
        self.update_time_string()
        if change == QGraphicsItem.ItemSelectedChange:
            is_selected = value.toBool()
            if is_selected:
                self.parentItem().setOpacity(1)
            else:
                # if not self.parentItem().isSelected() and not self.isSelected():
                if not is_selected:
                    self.parentItem().setOpacity(0.001)
            # self.parentItem().setSelected(is_selected)
        result = super(TimeText, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result

    def set_font(self, font_family='Futura'):
        font = QFont()
        font.setFamily(font_family)
        font.setPointSize(14)
        self.setTextWidth(14 * 4)
        self.setFont(font)

    def get_pixels_per_second(self):
        if self.scene():
            return self.scene().pixels_per_second
        else:
            pixels_per_second = 1
            return pixels_per_second

    def update_time_string(self):
        if self.parentItem():
            pixels_per_second = self.get_pixels_per_second()
            pos = self.parentItem().scenePos().x()
            time = format_time_string(pos, pixels_per_second) 
            self.setPlainText(time)
        
    def update_time_data(self):
        self.update_time_string()
            
    def hoverEnterEvent(self, e):
        print 'hover'
        self.parentItem().setOpacity(1)

    def hoverLeaveEvent(self, e):
        if not self.parentItem().isSelected() and not self.isSelected():
            self.parentItem().setOpacity(0.001)

    def mousePressEvent(self, e):
        if self.group():
            self.scene().set_groups_selectable(False)
        if e.modifiers() == Qt.ShiftModifier:
            self.scene().clearSelection()
            super(TimeText, self).mousePressEvent(e)
            self.parentItem().setOpacity(1)
            self.parentItem().color = QColor('crimson')
            self.parentItem().setSelected(False)
        else:
            # self.parentItem().mousePressEvent(e)
            super(TimeText, self).mousePressEvent(e)
            self.parentItem().setOpacity(1)

    def mouseMoveEvent(self, e):
        if e.modifiers() == Qt.ShiftModifier:
            super(TimeText, self).mouseMoveEvent(e)
        else:
            self.parentItem().mouseMoveEvent(e)

    def mouseReleaseEvent(self, e):
        if self.group():
            self.scene().set_groups_selectable(True)
        if e.modifiers() == Qt.ShiftModifier:
            super(TimeText, self).mouseReleaseEvent(e)
            self.parentItem().color = QColor('dodgerblue')
        else:
            # self.parentItem().mouseReleaseEvent(e)
            self.parentItem().color = QColor('dodgerblue')
            super(TimeText, self).mouseReleaseEvent(e)

