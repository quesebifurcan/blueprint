from PyQt4.QtGui import *
from PyQt4.QtCore import *

import sip

from scene_items.baseitem import BaseItem
from scene_states.text_edit_state import TextEditState

from scene_items.line import LineNode


class TextItemAnchor(LineNode):

    def __init__(self, parent=None):
        super(TextItemAnchor, self).__init__(parent)
        self.textitem = TextItem()
        self.textitem.setParentItem(self)
        self.setZValue(1)
        self.setOpacity(0.001)
        self.show()

    def relative_horizontal_move(self, ratio):
        pos = self.pos()
        self.setPos(pos.x() * ratio, pos.y())

    def relative_vertical_move(self, ratio):
        pos = self.pos()
        self.setPos(pos.x(), pos.y() * ratio)

    def copy(self):
        cp = TextItemAnchor()
        for p in self.textitem.properties():
            if not p == 'pos':
                value = getattr(self.textitem, p)
                setattr(cp.textitem, p, value)
        cp.textitem.setPos(self.textitem.pos())
        cp.setPos(self.pos())
        cp.show()
        return cp


class TextItem(QGraphicsTextItem, BaseItem):

    def __init__(self, parent=None):
        super(TextItem, self).__init__(parent)
        self.setFlag(QGraphicsItem.ItemIgnoresParentOpacity)
        self.setZValue(-100)
        self.text = "Text"

    def properties(self):
        return ('pos',
                'color',
                'text',
                'fontfamily',
                'fontsize',
                'bold',
                'italic',
                'transparency',
                'underline',
                'strikeout')

    def copy(self, new_pos=None):

        instance = TextItem()

        for p in self.properties():
            if not p == 'pos':
                value = getattr(self, p)
                setattr(instance, p, value)

        if new_pos:
            instance.setPos(new_pos)
        else:
            instance.setPos(self.pos())

        return instance

    @property
    def color(self):
        return self.defaultTextColor()

    @color.setter
    def color(self, value):
        self.setDefaultTextColor(value)

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemSelectedChange:
            is_selected = value.toBool()
            if is_selected:
                self.parentItem().setOpacity(1)
            else:
                self.parentItem().setOpacity(0.001)
                self.setTextInteractionFlags(Qt.TextEditorInteraction)
        result = super(TextItem, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result

    def hoverEnterEvent(self, e):
       print 'hover'
       self.parentItem().setOpacity(1)

    def hoverLeaveEvent(self, e):
        if not self.parentItem().isSelected() and not self.isSelected():
            self.parentItem().setOpacity(0.001)

    def mousePressEvent(self, e):
        if self.group():
            self.scene().set_groups_selectable(False)
        if e.modifiers() == Qt.ShiftModifier:
            self.scene().clearSelection()
            super(TextItem, self).mousePressEvent(e)
            self.parentItem().setOpacity(1)
            self.parentItem().color = QColor('crimson')
            self.parentItem().setSelected(False)
        else:
            # self.parentItem().mousePressEvent(e)
            super(TextItem, self).mousePressEvent(e)
            self.parentItem().setOpacity(1)

    def mouseMoveEvent(self, e):
        if e.modifiers() == Qt.ShiftModifier:
            super(TextItem, self).mouseMoveEvent(e)
        else:
            self.parentItem().mouseMoveEvent(e)

    def mouseReleaseEvent(self, e):
        if self.group():
            self.scene().set_groups_selectable(True)
        if e.modifiers() == Qt.ShiftModifier:
            super(TextItem, self).mouseReleaseEvent(e)
            self.parentItem().color = QColor('dodgerblue')
        else:
            # self.parentItem().mouseReleaseEvent(e)
            self.parentItem().color = QColor('dodgerblue')
            super(TextItem, self).mouseReleaseEvent(e)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Return:
            self.setTextInteractionFlags(Qt.NoTextInteraction)
            self.setSelected(False)
        else:
            super(TextItem, self).keyPressEvent(e)

    def mouseDoubleClickEvent(self, e):
        self.scene().state = TextEditState
        self.setTextInteractionFlags(Qt.TextEditorInteraction)
        super(TextItem, self).mousePressEvent(e)



