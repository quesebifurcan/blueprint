from __future__ import division

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.line import LineNode


class RampNode(QGraphicsPathItem):

    def __init__(self, parent=None):
        super(RampNode, self).__init__(parent)

        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setAcceptHoverEvents(True)

        pen = QPen()
        color = QColor('crimson')
        color.setAlpha(128)
        pen.setColor(color)
        pen.setWidth(2)
        self.setPen(pen)

        # draw ellipse; offsets need to be compensated for
        # in Ramp.getWidth and Ramp.getHeight
        path = QPainterPath()
        dm = 6
        path.addEllipse((dm / 2) * -1,
                        (dm / 2) * -1,
                        dm,
                        dm)
        self.setPath(path)

        # bend value controls slope of the bezier curve starting
        # from this RampNode
        self._bend = 0
        self.hide()

    ### PROPERTIES ###

    def properties(self):
        return ['pos', 'bend']

    def copy(self, new_pos=None):

        instance = RampNode()

        for p in self.properties():
            if not p == 'pos':
                value = getattr(self, p)
                setattr(instance, p, value)

        if new_pos:
            instance.setPos(new_pos)
        else:
            instance.setPos(self.pos())

        return instance

    @property
    def bend(self):
        return self._bend

    @bend.setter
    def bend(self, value):
        self._bend = value
        return self._bend

    def setSelected(self, value):
        if value:
            self.show()
        else:
            self.hide()
        super(RampNode, self).setSelected(value)

    def boundingRect(self):
        r = super(RampNode, self).boundingRect()
        w = 4
        return QRectF(r.x()-w, r.y()-w, r.width()+(w*2), r.height()+(w*2)) 

    def mousePressEvent(self, e):
        if e.modifiers() == Qt.ShiftModifier:
            parent = self.parentItem()
            parent.paint_path()
            if not self == parent.start and not self == parent.end: 
                scene = self.scene()
                self.setParentItem(None)
                scene.removeItem(self)
                parent.paint_path()
                del self
        else:
            self.click_pos = e.pos()
            if self.group():
                self.scene().set_groups_selectable(False)
            super(RampNode, self).mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        if self.group():
            self.scene().set_groups_selectable(True)
        super(RampNode, self).mouseReleaseEvent(e)

    def increment_bend(self, value):
        new_value = self.bend + (value / 100)
        if 0 <= new_value <= 1:
            self.bend = new_value
            self.parentItem().paint_path()

    def mouseMoveEvent(self, e):
        if e.modifiers() == Qt.ControlModifier:
            y_diff = self.click_pos.y() - e.pos().y()
            self.increment_bend(y_diff)
            self.click_pos = e.pos()
        else:
            super(RampNode, self).mouseMoveEvent(e)
            self.parentItem().paint_path()
            self.scene().check_item_positions(self)

    def check_position(self):
        if self.pos().y() > 0:
            self.setPos(self.pos().x(), 0) 


from scene_items.baseitem import BaseItem, NodeItem

class Ramp(QGraphicsPathItem, BaseItem, NodeItem):

    def __init__(self, nodes=None):
        super(Ramp, self).__init__()

        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges)
        self.setAcceptHoverEvents(True)
        self.setZValue(-1) 

        self._nodes = []
        self._dy = 50

        self.main_color = 'black'

        self.nodes = nodes
        self.paint_path()

    ### PROPERTIES ###

    def properties(self):
        return 'pos dx dy color nodes transparency'.split()

    def get_start_and_end_points(self):
        nodes = sorted(self._nodes, key=lambda x: x.pos().x())
        return nodes[0], nodes[1]

    def remove_nodes(self):
        for node in self._nodes:
            node.setParentItem(None)
            if node.scene():
                node.scene().removeItem(node)
        self._nodes = []

    def init_default_nodes(self):
        width, height = 100, 50
        for x,y in zip((0, 100), (0, -50)):
            node = RampNode(self)
            node.setPos(x, y)
            self._nodes.append(node)

    def copy(self, new_pos=None):

        instance = Ramp()

        for p in self.properties():
            if not p == 'pos':
                value = getattr(self, p)
                setattr(instance, p, value)

        if new_pos:
            instance.setPos(new_pos)
        else:
            instance.setPos(self.scenePos())

        instance.paint_path()

        return instance

    @property
    def dx(self):
        return self.path().controlPointRect().width()

    @dx.setter
    def dx(self, value):
        ratio = value / self.dx
        for node in self.childItems():
            x = node.pos().x() * ratio
            y = node.pos().y()
            node.setPos(x, y)
        self.paint_path()

    @property
    def dy(self):
        return self.path().controlPointRect().height()

    @dy.setter
    def dy(self, value):
        if value > 0:
            ratio = value / self.dy
            for node in self.childItems():
                x = node.pos().x()
                y = node.pos().y() * ratio
                if y < 0:
                    node.setPos(x, y)
                else:
                    node.setPos(x, 0)
            self.paint_path()
            self.update()

    def mousePressEvent(self, e):
        if e.modifiers() == Qt.ShiftModifier:
            node = RampNode(self)
            node.setPos(e.pos())
            node.show()
            self._nodes.append(node)
            self.paint_path()
        if self.group():
            e.setAccepted(True)
            self.mousePressedPos = e.scenePos()
        else:
            super(Ramp, self).mousePressEvent(e)
        # self.resize_self_from_right_and_scale(5)

    def mouseMoveEvent(self, e):
        if self.group():
            diff_X = e.scenePos().x() - self.mousePressedPos.x()
            diff_Y = e.scenePos().y() - self.mousePressedPos.y()
            self.setPos(self.pos().x() + diff_X, 
                        self.pos().y() + diff_Y)
            e.setAccepted(True) # avoid propagating event to parent
            self.mousePressedPos = e.scenePos()
        else:
            super(Ramp, self).mouseMoveEvent(e)

    def scale_vertically(self, amount):
        self.dy = self.dy + amount

    def resize_from_top_to_bottom(self, ratio):
        self.dy = self.dy * ratio
        self.setPos(self.pos().x(), self.pos().y() * ratio)
        self.paint_path()

    def resize_from_right_and_scale(self, ratio):
        self.setPos(self.pos().x() * ratio, self.pos().y())
        self.dx = self.dx * ratio
        self.paint_path()

    def scale_nodes(self):
        pass

    def scale_bezier_data(self, val, out_min, out_max, in_min=0.0, in_max=1.0):
        return ((val - in_min) / (in_max - in_min)) * (out_max - out_min) + out_min
    
    # the following three methods take care of showing the RampNodes when hovering:
    def shape(self):
        r = super(Ramp, self).childrenBoundingRect()
        path = QPainterPath()
        path.addRect(r)
        return path

    def calculate_bezier_curve(self, node1, node2):
        '''
        calculate a bezier curve between two points. the slope of the
        curve is determined by node1.bend -- a value between 0 and 1
        where 0 is a straight line and 1 is an 'exponential' curve.
        returns a 3-value tuple used by QPainterPath.cubicTo in self.paint_path

        '''
        # get max and min values (straight line and curved line) for
        # line between node1 and node2 --

        # x-axis:
        width = node2.x() - node1.x()
        min_slope_x = node2.x() - (width / 2)
        if node2.y() < node1.y():
            max_slope_x = node2.x() - 3 
        else:
            max_slope_x = node1.x() + 3

        # y-axis
        y_points = node1.y(), node2.y()
        min_slope_y = sum(y_points) / 2
        max_slope_y = max(y_points)

        # interpolate 
        bend = node1.bend
        point_x = self.scale_bezier_data(bend, min_slope_x, max_slope_x)
        point_y = self.scale_bezier_data(bend, min_slope_y, max_slope_y)

        point = QPointF(point_x, point_y)

        return (point, point, node2.pos())

    def paint_path(self):
        path = QPainterPath()
        path.setFillRule(Qt.WindingFill)

        points = self.childItems()
        points = sorted(points, key=lambda x: x.pos().x())
        self.start, self.end = points[0], points[-1]
        start = points[0].pos()

        # start drawing
        path.moveTo(start.x(),0)
        path.lineTo(start)

        # draw all curves
        for a,b in zip(points[:], points[1:]):
            bezier = self.calculate_bezier_curve(a, b)
            path.cubicTo(bezier[0], bezier[1], bezier[2])

        # close shape
        path.lineTo(points[-1].x(), 0)
        path.connectPath(path)

        pen = QPen()
        pen.setColor(QColor(self.color))
        self.setBrush(QBrush(self.color))
        pen.setWidth(1)
        self.setPath(path)
        self.update()

    def paint(self, a, b, c):
        super(Ramp, self).paint(a,b,c)
