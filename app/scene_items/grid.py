from __future__ import division

import sys

import unittest 
import time

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.baseitem import BaseItem
from utils import format_time_string


class Grid(object):

    def __init__(self, interval=100):
        self._interval=interval
        self.lines=[]
        self._orientation = 'vertical'
        self._lines_visible = True
        self._textitems_visible = True
        self._is_time_based = True
        self._editable = False
        # self.color = QColor('black')
        # self.selection_color = QColor('DodgerBlue')
        self.linestyle = Qt.SolidLine
        self.lines_are_visible = True
        self.textitems_are_visible = True
        self.line_extension = 0
        self.text_is_centered = True
        self.scene = None

    # TODO: keep list of properties when changing
    # TODO: store interval in scene_coordinates, not in seconds
    # TODO: when setting interval in seconds, update  pixel_interval correctly
    def properties(self):
        return 'orientation interval is_time_based'.split()

    @property
    def orientation(self):
        return self._orientation

    @orientation.setter
    def orientation(self, value):
        if not value == self.orientation:
            if self.lines:
                self.remove_lines_from_scene()
            if value == 'horizontal':
                self._orientation = 'horizontal'
            elif value == 'vertical':
                self._orientation = 'vertical'
            if self.scene:
                self.draw_grid_lines()

    @property
    def lines_visible(self):
        return self._visible

    @lines_visible.setter
    def lines_visible(self, value):
        print 'setting'
        self._lines_visible = value
        for line in self.lines:
            if value:
                line.setOpacity(1)
            else:
                line.setOpacity(0.0001)

    @property
    def textitems_visible(self):
        return self._textitems_visible

    @textitems_visible.setter
    def textitems_visible(self, value):
        if self.orientation == 'vertical' and not value == self.textitems_are_visible:
            self._textitems_visible = value
            for line in self.lines:
                line.text.hide()
        else:
            for line in self.lines:
                line.text.show()

    @property
    def is_time_based(self):
        return self._is_time_based

    @is_time_based.setter
    def is_time_based(self, value):
        if self.orientation == 'vertical':
            print value, '*****'
            if value == 0:
                value = False
            else:
                value = True
            self._is_time_based = value
            for line in self.lines:
                line.is_time_based = value 
                line.update_text()

    def setScene(self, scene):
        self.scene = scene
        self.draw_grid_lines()

    def set_orientation(self, value):
        if self.lines:
            self.remove_lines_from_scene()
        if value == 'horizontal':
            self.orientation = 'horizontal'
        elif value == 'vertical':
            self.orientation = 'vertical'
        if self.scene:
            self.draw_grid_lines()

    # def set_line_visibility(self, value):
    #     if not value == self.lines_are_visible:
    #         self.lines_are_visible = value
    #         for line in self.lines:
    #             if value:
    #                 line.show()
    #             else:
    #                 line.hide()

    # def set_textitem_visibility(self, value):
    #     if self.orientation == 'vertical' and not value == self.textitems_are_visible:
    #         self.textitems_are_visible = value
    #         for line in self.lines:
    #             line.set_textitem_visibility(value)

    def setSelected(self, value):
        self.isSelected = value
        if value:
            self.set_selection_color()
        else:
            self.set_color(self.color)

    def set_color(self, color):
        self.color = color
        for line in self.lines:
            line.set_color(color)

    def set_selection_color(self):
        for line in self.lines:
            line.set_color(self.selection_color)

    def set_linestyle(self, style):
        self.linestyle = style
        for line in self.lines:
            line.set_linestyle(style)

    def set_line_extension(self, value):
        if self.orientation == 'vertical':
            height = self.scene.height()
            for line in self.lines:
                line.line_extension = value
                line.draw_line(height + value) 
                line.set_text_pos()

    def remove_lines_from_scene(self):
        for line in self.lines:
            self.scene.removeItem(line)
        self.lines = []

    def hide(self):
        for line in self.lines:
            line.setVisible(False)
            
    def show(self):
        for line in self.lines:
            line.setVisible(True)

    @property
    def interval(self):
        if self.is_time_based:
            return self.scene.pixels_per_second * self._interval
        else:
            return self._interval

    @interval.setter
    def interval(self, value):
        self._interval = value
        if self.lines:
            self.remove_lines_from_scene()
            self.draw_grid_lines()

    def draw_grid_lines(self):
        if self.orientation == 'horizontal':
            self.draw_horizontal_grid_lines()
        if self.orientation == 'vertical':
            self.draw_vertical_grid_lines()
        self.set_line_extension(4)

    def draw_vertical_grid_lines(self):
        width = int(self.scene.width())
        height = self.scene.height()
        for x in range(0, width, self.interval):
            print x
            line = VerticalGridLine()
            self.lines.append(line)
            line.add_to_scene(self.scene, x)
            line.update_text()
            line.draw_line(height)

    def draw_horizontal_grid_lines(self):
        height = int(self.scene.height())
        width = self.scene.width()
        for y in range(0, height, self.interval):
            line = HorizontalGridLine()
            self.lines.append(line)
            line.add_to_scene(self.scene, y)
            line.draw_line(width)

    @property
    def editable(self):
        return self._editable

    @editable.setter
    def editable(self, value):
        self._editable = value
        for line in self.lines:
            line.set_editable(value)

from scene_items.baseitem import BaseItem

class GridLine(QGraphicsLineItem, BaseItem):

    def __init__(self, parent=None):
        super(BaseItem, self).__init__(parent)
        self.setZValue(-1000)
        self.set_editable(False)

    def set_editable(self, value):
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, value)
        self.setFlag(QGraphicsItem.ItemSendsScenePositionChanges, value)
        self.setFlag(QGraphicsItem.ItemIsMovable, False)
        self.setFlag(QGraphicsItem.ItemIsSelectable, value)
        if hasattr(self, 'textitem'):
            self.textitem.setFlag(QGraphicsItem.ItemSendsGeometryChanges, value)
            self.textitem.setFlag(QGraphicsItem.ItemSendsScenePositionChanges, value)
            self.textitem.setFlag(QGraphicsItem.ItemIsMovable, False)
            self.textitem.setFlag(QGraphicsItem.ItemIsSelectable, value)

    def set_textitem_visibility(self, value):
        self.textitem_is_visible = value


class VerticalGridLine(GridLine):

    def __init__(self, parent=None):
        super(VerticalGridLine, self).__init__(parent)
        self.textitem_is_visible = True
        self.textitem = None
        self.is_time_based = False
        self.text_is_centered = True
        self.line_extension = 0
        self.init_text()

    def init_text(self):
        self.textitem = GridText()
        self.textitem.setParentItem(self)
        self.set_text_pos()

    def set_text_pos(self):
        y = self.line_extension + 20
        self.textitem.setPos(0 - (self.textitem.textWidth() / 4), -y)

    def update_text(self):
        if not self.is_time_based:
            self.set_text_pos()
            self.textitem.setPlainText(str(int(self.scenePos().x())))
        else:
            self.textitem.update_time_string()

    def draw_line(self, height, extension=0):
        self.setLine(0, 0, 0, height)
        if self.textitem:
            self.set_text_pos()

    def add_to_scene(self, scene, x_pos):
        scene.addItem(self)
        self.setPos(x_pos, 0)


class HorizontalGridLine(GridLine):

    def __init__(self, parent=None):
        super(HorizontalGridLine, self).__init__(parent)
        
    def draw_line(self, width):
        self.setLine(0, 0, width, 0)

    def add_to_scene(self, scene, y_pos):
        scene.addItem(self)
        self.setPos(0, y_pos)

from scene_items.timetext import TimeText

class GridText(TimeText):

    def __init__(self, parent=None):
        super(GridText, self).__init__(parent)
        self.setAcceptHoverEvents(False)

    def hoverLeaveEvent(self, e):
        pass

    def itemChange(self, change, value):
        self.update_time_string()
        result = super(TimeText, self).itemChange(change, value)
        if isinstance(result, QGraphicsItem):
            result = sip.cast(result, QGraphicsItem)
        return result

    def mousePressEvent(self, e):
        QGraphicsTextItem.mousePressEvent(self, e)

    def mouseMoveEvent(self, e):
        QGraphicsTextItem.mouseMoveEvent(self, e)

    def mouseReleaseEvent(self, e):
        QGraphicsTextItem.mouseReleaseEvent(self, e)
