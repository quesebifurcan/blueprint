import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.group import Group
from scene_items.line import HorizontalLine, LineNode
from scene_items.textitem import TextItem, TextItemAnchor
from scene_items.timetext import TimeText, TimeTextAnchor
from scene_items.ramp import Ramp, RampNode
from scene_items.rectitem import RectItem
from scene_items.pathitem import PathItem

from scene_items.sceneborder import SceneBorder
from scene_items.layoutmarkers import SystemMarker, PageMarker

from scene_states.default_state import DefaultState
from scene_states.insert_item_state import InsertItemState
from scene_states.paste_items_state import PasteItemsState

def item_classes():
    return (Group,
            HorizontalLine,
            TextItemAnchor,
            TimeTextAnchor,
            Ramp,
            RectItem,
            PathItem)

def editable_item_classes():
    return (Group,
            HorizontalLine,
            TextItemAnchor,
            TimeTextAnchor,
            Ramp,
            RectItem,
            PathItem)

class Scene(QGraphicsScene):

    ### CLASS VARIABLES ###

    grid = None
    item_added = pyqtSignal(QGraphicsItem)
    item_removed = pyqtSignal(QGraphicsItem)
    item_classes = item_classes()
    stretchable_item_classes = (HorizontalLine, Ramp, RectItem)

    ### INITIALIZER ### 

    def __init__(self, parent=None):
        super(Scene, self).__init__(parent)
        self.pixels_per_second = 30
        self.groups = []
        self.grids = []
        self.cursorText = None
        self.markers = []
        self.scene_border = SceneBorder(scene=self)
        self.addItem(self.scene_border)
        self.insert_mode = False
        self.current_insert = None
        self._state = None
        self.state = DefaultState

    ### PUBLIC PROPERTIES ###

    def properties(self):
        return ['scene_items']

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value(scene=self)

    ### PUBLIC METHODS ###

    def activate_insert_cursor(self, text=None):
        self.view.setCursor(Qt.CrossCursor)

    def activate_stretch_cursor(self, text=None):
        self.view.setCursor(Qt.SizeFDiagCursor)

    def activate_text_cursor(self):
        self.view.setCursor(Qt.IBeamCursor)

    def reset_cursor(self):
        self.view.setCursor(Qt.ArrowCursor)

    def addGrid(self, grid):
        self.grids.append(grid)
        grid.setScene(self)

    def removeGrid(self, grid):
        grid.remove_lines_from_scene()
        grid.scene = None
        self.grids.remove(grid)

    def setGrid(self, grid):
        if self.grid:
            self.grid.remove_lines_from_scene()
        self.grid = grid
        grid.scene = self
        grid.draw_grid_lines()

    def all_selected_items(self):
        result = []
        for item in self.items():
            if item.isSelected():
                result.append(item)
        return result

    def delete_selected_items(self):
        for item in self.selectedItems():
            if isinstance(item, self.item_classes):
                self.removeItem(item)
                del item

    def copy_selection(self):
        self.temp_group = self.group_selected_items()
        self.copy = self.temp_group.copy()
        self.dissolve_group(self.temp_group, recursive=False)
        self.clearSelection()
        self.state = lambda scene: PasteItemsState(scene, self.copy)

    def select_all(self):
        for item in self.items():
            if isinstance(item, self.item_classes):
                item.setSelected(True)

    def get_top_level_items_in_selection(self):
        coll = []
        for item in self.selectedItems():
            if not item.parentItem():
                coll.append(item)
        return coll

    def paste_to_position(self, group, pos):
        self.addItem(group)
        group.setPos(pos)
        for item in group.childItems():
            group.removeFromGroup(item)
            self.addItem(item)
        del group

    def check_item_positions(self, item):
        pass

    def set_groups_selectable(self, value):
        for item in self.items():
            if isinstance(item, QGraphicsItemGroup):
                item.setFlag(QGraphicsItem.ItemIsSelectable, value)

    def scale_item_positions(self, ratio):
        temp_group = Group()
        self.addItem(temp_group)
        for item in self.items():
            if not item.parentItem() and not item == temp_group:
                if isinstance(item, self.item_classes):
                    temp_group.addToGroup(item)
        width = temp_group.boundingRect().width()
        amount = (width * ratio) - width
        temp_group.resize_from_right_and_scale(amount)
        items = temp_group.childItems()
        self.removeItem(temp_group)
        for item in items:
            self.addItem(item)

    def fit_in_view(self):
        self.view.fitInView(self.itemsBoundingRect())

    def set_pixels_per_second(self, value):
        ratio = value / self.pixels_per_second
        self.pixels_per_second = value
        self.scale_item_positions(ratio)
        for item in self.items():
            if hasattr(item, 'update_time_data'):
                item.update_time_data()

    def update_markers(self):
        markers = sorted(self.markers, key=lambda m: m.pos().x())
        curr_page = 0
        curr_system = 0
        for m in markers:
            if isinstance(m, SystemMarker):
                curr_system += 1
            else:
                curr_page += 1
                curr_system = 1
            s = '.'.join([str(curr_page), str(curr_system)])
            m.text.setPlainText(s) 

    def update_borders(self):
        height = self.sceneRect().height()
        for m in self.markers:
            m.update_line(height)

    def align_items_horizontally(self): 
        items = sorted(self.selectedItems(),
                       key=lambda item: item.scenePos().x())
        x = items[0].scenePos().x()
        for item in items[1:]:
            if item.parentItem():
                pos = item.parentItem().mapFromScene(x, item.scenePos().y())
                item.setPos(pos)
            else:
                item.setX(x)

    def align_items_vertically(self): 
        items = sorted(self.selectedItems(),
                       key=lambda item: item.scenePos().y())
        y = items[0].scenePos().y()
        for item in items[1:]:
            if item.parentItem():
                pos = item.parentItem().mapFromScene(item.scenePos().x(), y)
                item.setPos(pos)
            else:
                item.setY(y)

    def delete_selected_groups(self):
        from scene_items.group import GroupBorderRect
        # keep track of modified items
        self.temp = self.items()
        coll = []
        for item in self.selectedItems():
            if not item.parentItem():
                coll.append(item)
        self.temp = []
        for item in coll:
            if isinstance(item, Group):
                for child in item.childItems():
                    if child.parentItem() == item:
                        item.removeFromGroup(child)
                        self.temp.append(child)
        for item in coll:
            print item.childItems() 
            self.removeItem(item.rep)
            del item.rep
            del item
        for item in self.items():
            item.setSelected(False)

    def cleanup_empty_groups(self):
        for item in self.items():
            if isinstance(item, Group) and not item.childItems():
                self.removeItem(item)
                del item
        print self.items()

    def group_selected_items(self):
        if len(self.selectedItems()) > 0:
            group = Group()
            x = 2000
            y = 2000
            for item in self.selectedItems():
                if item.scenePos().x() < x:
                    x = item.scenePos().x()
                if item.scenePos().y() < y:
                    y = item.scenePos().y()
            group.setPos(x,y)
            top = self.get_top_level_items_in_selection()
            for item in self.selectedItems():
                item.setSelected(False)
            for item in top:
                group.addToGroup(item)
            self.addItem(group)
            self.clearSelection()
            return group

    def reparent_group_items(self, group):
        pass

    def join_nested_groups(self):
        coll = []
        for item in self.selectedItems():
            if isinstance(item, QGraphicsItemGroup):
                for child in item.get_all_items():
                    coll.append(child)
                else:
                    coll.append(item)
        filtered = []
        for item in coll:
            if (isinstance(item, self.item_classes) and
                not isinstance(item, QGraphicsItemGroup)):
                filtered.append(item)
        for item in self.selectedItems():
            if isinstance(item, QGraphicsItemGroup):
                self.dissolve_group(item, recursive=True)
        group = Group()
        x = 2000
        y = 2000
        for item in filtered:
            if item.scenePos().x() < x:
                x = item.scenePos().x()
            if item.scenePos().y() < y:
                y = item.scenePos().y()
        group.setPos(x, y)
        self.addItem(group) 
        for item in filtered:
            item.setSelected(False)
            group.addToGroup(item)

    def remember_groups(self):
        self.groups = []
        for item in self.items():
            if isinstance(item, QGraphicsItemGroup):
                group = {'group': item, 'children': []}
                for child in item.childItems():
                    group['children'].append(child)
                self.groups.append(group)

    def rebuild_groups(self):
        for item in self.items():
            item.setSelected(False)
        for group in self.groups:
            self.addItem(group['group'])
            for item in group['children']:
                # triggers warning: item has already been added:
                if item in self.items():
                    group['group'].addToGroup(item)


    def dissolve_group(self, group, recursive=True):
        if isinstance(group, QGraphicsItemGroup):
            items, positions = [], []
            for item in group.childItems():
                if isinstance(item, QGraphicsItemGroup) and recursive:
                    self.dissolve_group(item)
                positions.append(item.scenePos())
                items.append(item)
                group.removeFromGroup(item)
                self.removeItem(item)
            for item, pos in zip(items, positions):
                item.setPos(pos)
                item.update()
                self.addItem(item)
                # when group is dissolved, select all items?
                item.setSelected(True) 
                # superfluous?:
                #self.scene.destroyItemGroup(group) 

    def dissolve_groups(self, recursive=True):
        for group in self.items():
            if isinstance(group, QGraphicsItemGroup):
                self.dissolve_group(group, recursive)

    ### OVERRIDDEN METHODS ###
        
    def addItem(self, item):
        super(Scene, self).addItem(item)
        self.item_added.emit(item)

    def removeItem(self, item):
        super(Scene, self).removeItem(item)
        self.item_removed.emit(item)

    def mousePressEvent(self, e):
        self.state.mousePressEvent(e)

    def mouseMoveEvent(self, e):
        self.state.mouseMoveEvent(e)

    def mouseReleaseEvent(self, e):
        self.state.mouseReleaseEvent(e)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_6:
            self.cleanup_empty_groups()
        self.state.keyPressEvent(e)

    

