from PyQt4.QtGui import *
from PyQt4.QtCore import *

from properties_editor.properties_editor_delegate import ClickableLabel


class ColorEditorWidget(QWidget):

    def __init__(self, parent=None):
        super(ColorEditorWidget, self).__init__(parent)
        layout = QHBoxLayout()
        self.label = ClickableLabel()
        self.label.setMaximumWidth(30)
        self.label.setMinimumWidth(30)
        self.label.setStyleSheet("border: 1px solid black; background-color: white;")

        space = QLabel()
        layout.addWidget(space)
        layout.addWidget(self.label)
        self.setLayout(layout) 
        self.layout().setContentsMargins(5, 5, 5, 5)
        self.setContentsMargins(1, 1, 1, 1)

        self.dialog = QColorDialog()
        self.label.clicked.connect(self.dialog.exec_)


def new_labeled_editor_group(labeltext, editor=QSpinBox):
    layout = QHBoxLayout()
    layout.setAlignment(Qt.AlignCenter)
    label = QLabel(labeltext)
    label.setAlignment(Qt.AlignCenter)
    layout.addWidget(label)
    editor = editor()
    layout.addWidget(editor)
    return layout, editor

def group_layouts_horizontally(layouts):
    parent = QHBoxLayout()
    for layout in layouts:
        parent.addLayout(layout)
    return parent


class RepeatDialog(QDialog):

    def __init__(self, scene=None, parent=None):
        super(RepeatDialog, self).__init__(parent)
        self.setWindowTitle("Repeat Selected Items")
        self.setModal(False)
        self.setScene(scene)
        self.items = []

        ### LAYOUT ###

        layout = QVBoxLayout()

        group = QGroupBox()
        group.setStyleSheet("QGroupBox {border: 1px solid gray}")
        group_layout = QVBoxLayout()

        count_layout = QHBoxLayout()

        count, self.count_editor = new_labeled_editor_group("Count")
        
        x_delta, self.x_delta_editor = new_labeled_editor_group("Delta-X")
        x_scale, self.x_scale_editor = new_labeled_editor_group(
            "X-Scale", QDoubleSpinBox)
        y_delta, self.y_delta_editor = new_labeled_editor_group("Delta-Y")
        y_scale, self.y_scale_editor = new_labeled_editor_group(
            "Y-Scale", QDoubleSpinBox)
        x_layout = group_layouts_horizontally([x_delta, x_scale])
        y_layout = group_layouts_horizontally([y_delta, y_scale])

        for sub_layout in [count, x_layout, y_layout]:
            group_layout.addLayout(sub_layout)

        group.setLayout(group_layout)
        layout.addWidget(group)

        button_row = QHBoxLayout()
        button_row.addWidget(QPushButton("Cancel"))
        default = QPushButton("Apply")
        default.setDefault(True)
        button_row.addWidget(default)
        layout.addLayout(button_row)

        self.setLayout(layout)

        ### SET MINIMUM/MAXIMUM VALUES ###
        for delta in (self.x_delta_editor, self.y_delta_editor):
            delta.setMinimum(0.0)
        for scale in (self.x_scale_editor, self.y_scale_editor):
            scale.setMinimum(1.0)
            scale.setSingleStep(0.01)
            scale.setMaximum(10)
        self.count_editor.setMinimum(0)

        ### SIGNALS ###
        self.count_editor.valueChanged.connect(self.update_count)
        self.x_delta_editor.valueChanged.connect(self.update_x)
        self.x_scale_editor.valueChanged.connect(self.update_x)
        self.y_delta_editor.valueChanged.connect(self.update_y)
        self.y_scale_editor.valueChanged.connect(self.update_y)

    def setScene(self, scene):
        self.scene = scene
        if scene:
            items = filter(lambda item: isinstance(item, self.scene.item_classes),
                           scene.selectedItems())
            self.added_items = items

    ### HANDLERS ###

    def handle_reset(self):
        self.clear_inserted_items()
        # self.reset_counters()

    ### PROPERTIES ### 

    @property
    def added_items(self):
        return self._added_items

    @added_items.setter
    def added_items(self, items):
        d = {}
        for item in items:
            d[item] = []
        self._added_items = d
    
    ### METHODS ###

    def clear_inserted_items(self):
        for item in self.added_items:
            self.scene.removeItem(item)
        self.added_items = None

    def get_x_pos_tuple(self):
        dx, x_scale = (self.x_delta_editor.value(),
                       self.x_scale_editor.value())
        return dx, x_scale

    def get_y_pos_tuple(self):
        dy, y_scale = (self.y_delta_editor.value(),
                       self.y_scale_editor.value())
        return dy, y_scale

    def update_x(self):
        import time
        dx, x_scale = self.get_x_pos_tuple()
        for item in self.added_items.keys():
            start_x = item.scenePos().x()
            for i, child in enumerate(self.added_items[item]):
                x = ((dx * (i + 1)) ** x_scale)
                x = x + start_x
                child.setX(x)

    def update_y(self):
        dy, y_scale = self.get_y_pos_tuple()
        for item in self.added_items.keys():
            start_y = item.scenePos().y()
            for i, child in enumerate(self.added_items[item]):
                y = ((dy * (i + 1)) ** y_scale)
                child.setY(y + start_y)

    def update_count(self, value):
        dx, dy, x_scale, y_scale = (self.x_delta_editor.value(),
                                    self.y_delta_editor.value(),
                                    self.x_scale_editor.value(),
                                    self.y_scale_editor.value())
        for item in self.added_items.keys():
            temp_val = value
            group = self.added_items[item]
            # length = len(group)

            if temp_val > len(group):
                while temp_val > len(group):
                    if not item.parentItem(): 
                        copy = item.copy()
                        self.scene.addItem(copy)
                        group.append(copy)
                        temp_val -= 1
            elif temp_val < len(group):
                while temp_val < len(group):
                    last = group.pop()
                    self.scene.removeItem(last)
                    del last
        self.update_x()
        self.update_y()

