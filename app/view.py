import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *


class View(QGraphicsView):

    def __init__(self, parent=None):
        super(View, self).__init__(parent)
        self.mode = 'stretch'
        self.click_pos = QPoint(0, 0) 
        self.setDragMode(QGraphicsView.RubberBandDrag)
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)

        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setRenderHint(QPainter.Antialiasing)
        self.setRenderHint(QPainter.TextAntialiasing)

        self.show()
        self.activateWindow()
        self.raise_()
        self.setMouseTracking(True)
        self.zoom_point = QPointF()

    def init_UI(self):
        self.setGeometry(QRect(0, 200, 1000, 500))

    def mousePressEvent(self, e):
        super(View, self).mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        super(View, self).mouseReleaseEvent(e)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Shift:
            self.zoom_point = self.mapToScene(self.mapFromGlobal(QCursor().pos()))
        else:
            super(View, self).keyPressEvent(e)

    def set_click_pos(self, e):
        self.click_pos = e.pos() 

    def move_cursor_to_zoom_point(self):
        cursor = QCursor()
        cursor.setPos(self.mapToGlobal(self.mapFromScene(self.zoom_point)))

    def wheelEvent(self, event):
        if event.modifiers() == Qt.ShiftModifier:
            factor = 1.41 ** (-event.delta() / 240.0)
            self.scale(factor, factor)
            self.centerOn(self.zoom_point)
            self.move_cursor_to_zoom_point()
        else:
            super(View, self).wheelEvent(event)
    
