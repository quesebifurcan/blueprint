import sys
import time

from PyQt4.QtGui import *
from PyQt4.QtCore import *


class ButtonGroup(QButtonGroup):

    def __init__(self, parent):
        super(ButtonGroup, self).__init__(parent)
        self.setExclusive(True)
        

class MenuButton(QToolButton):

    def __init__(self, parent=None):
        super(MenuButton, self).__init__(parent)
        self.setFocusPolicy(Qt.NoFocus)
        self.setCheckable(True)


    
