import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from scene_items.line import LineNode, HorizontalLine
from scene_items.ramp import RampNode, Ramp
from scene_items.textitem import TextItem

def create_item(cls):
    item = CheckableTreeWidgetItem()
    item.setCheckState(0, 0)
    item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsUserCheckable)
    item.setText(0, cls().__class__.__name__)
    item.cls = cls
    font = item.font(0)
    font.setStyle(QFont.StyleItalic)
    item.setFont(0, font)
    return item


class CheckableTreeWidgetItem(QTreeWidgetItem, QObject):

    def setData(self, column, role, value):
        state = self.checkState(column)
        QTreeWidgetItem.setData(self, column, role, value)
        if (role == Qt.CheckStateRole and
            state != self.checkState(column)):
            treewidget = self.treeWidget()
            state = self.checkState(column)
            treewidget.itemChecked.emit(self, column, state)


class ItemSelectionFilter(QTreeWidget):

    itemChecked = pyqtSignal(QTreeWidgetItem, int, int)

    def __init__(self, parent=None):
        super(ItemSelectionFilter, self).__init__(parent)

        self.setIndentation(15)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setAlternatingRowColors(True)
        font = self.font()
        font.setPointSize(11)
        self.setFont(font)

        line = create_item(HorizontalLine)
        linenode = create_item(LineNode)
        self.insertTopLevelItem(0, line)
        self.insertTopLevelItem(1, linenode)

        ramp = create_item(Ramp)
        rampnode = create_item(RampNode)
        self.insertTopLevelItem(2, ramp)
        self.insertTopLevelItem(3, rampnode)

        text = create_item(TextItem)
        self.insertTopLevelItem(4, text)

        self.expandAll()
        self.temp_selection = []

        self.itemChecked.connect(self.update_scene_selection)

    def update_scene_selection(self):
        curr_sel = []
        self.scene.blockSignals(True)

        for item in self.get_all_items():
            if item.checkState(0) == 2:
                curr_sel.append(item.text(0))
        if self.temp_selection:
            for item in self.temp_selection:
                if item.__class__.__name__ in curr_sel:
                    item.setSelected(True)
                else:
                    item.setSelected(False)

        self.scene.blockSignals(False)

    def update_filter(self):
        self.deselect_all()
        self.set_temp_selection()

    def set_temp_selection(self):
        self.temp_selection = []
        for item in self.scene.items():
            if item.isSelected():
                self.temp_selection.append(item)

    def focusInEvent(self, e):
        self.set_temp_selection()
        super(ItemSelectionFilter, self).focusInEvent(e)

    def select_all(self):
        for item in self.get_all_items():
            item.setCheckState(0, 2)

    def deselect_all(self):
        for item in self.get_all_items():
            item.setCheckState(0, 0)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_R:
            self.set_temp_selection()
        else:
            super(ItemSelectionFilter, self).keyPressEvent(e)

    def get_all_items(self):

        def visit_tree(item, coll):
            if not item == self.invisibleRootItem():
                coll.append(item)
            for i in range(item.childCount()):
                curr = item.child(i)
                visit_tree(curr, coll)
            return coll

        coll = []
        return visit_tree(self.invisibleRootItem(), coll)


