from PyQt4.QtGui import *
from PyQt4.QtCore import *

from collections import namedtuple

def new_labeled_editor(text, editor):
    label = QLabel(text)
    label.setAlignment(Qt.AlignCenter)
    editor_tuple = namedtuple('EditorTuple', ['label', 'editor'])
    return editor_tuple(label, editor)

def new_parameter_selector(labeltext, parameters):
    selector = QComboBox()
    selector.addItems(parameters)
    editor = new_labeled_editor(labeltext, selector)
    return editor

def new_state_counter(labeltext):
    selector = QSpinBox()
    editor = new_labeled_editor(labeltext, selector)
    return editor

def add_editor_tuples_to_layout(layout, editor_tuples):
    for i, editor_tuple in enumerate(editor_tuples):
        label, editor = editor_tuple
        layout.addWidget(label, i, 0)
        layout.addWidget(editor, i, 1)
