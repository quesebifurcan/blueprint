from PyQt4.QtGui import *
from PyQt4.QtCore import *

from sequential_editor_view import SequentialEditorView
from sequential_editor_model import SequentialEditorModel

from parameter_to_model_item_map import (parameter_to_model_item_map,
                                         model_item_data_conversions)

from editor_group_box import EditorGroupBox
from command_button_row_layout import CommandButtonRowLayout
from header_widgets import (new_parameter_selector,
                            new_state_counter,
                            add_editor_tuples_to_layout)

# todo: inherit sequentialeditor-base-functionality and define interactions with scene.

class SaveState(object):

    def __init__(self, items, property_):
        self.property_ = property_
        self.current_order = lambda item: item.scenePos().x()
        self.item_value_tuples = []
        self.get_values(items)

    def sorted_items(self, items):
        return sorted(items, key=self.current_order)

    def get_values(self, items):
        for item in self.sorted_items(items):
            if hasattr(item, self.property_):
                self.item_value_tuples.append(
                    (item, getattr(item, self.property_)))

    def items(self):
        return [tpl[0] for tpl in self.item_value_tuples]


class SequentialEditorDialog(QDialog):

    editor_map = parameter_to_model_item_map()
    conversions = model_item_data_conversions()

    def __init__(self, scene=None, parent=None):
        super(SequentialEditorDialog, self).__init__(parent)
        self.setWindowTitle("Sequential Edit")
        self.setModal(False)
        self.setFocusPolicy(Qt.StrongFocus)

        self._current_parameter = None
        self._seed_value = 0

        ### HEADER ###
        header = EditorGroupBox()

        self.parameters = new_parameter_selector(
            "Parameter", self.editor_map.keys()) 
        self.order = new_parameter_selector(
            "Order", "Sequential Random".split())
        self.seed = new_state_counter("Seed Value")
        self.seed.editor.setMaximum(100000)
        self.state_counter = new_state_counter("No. of states")
        add_editor_tuples_to_layout(header.layout(),
                                    [self.parameters,
                                     self.order,
                                     self.seed,
                                     self.state_counter])

        ### DATA ###
        self.editors = SequentialEditorView(model=SequentialEditorModel())

        ### APPLY / CANCEL ###
        self.button_row = CommandButtonRowLayout()

        ### SIGNALS ###
        self.parameters.editor.currentIndexChanged.connect(self.handle_set_editor)
        self.state_counter.editor.valueChanged.connect(self.handle_counter_change)
        self.order.editor.currentIndexChanged.connect(self.handle_set_order)
        self.seed.editor.valueChanged.connect(self.handle_set_seed)

        self.editors.editorCountChanged.connect(self.handle_item_count_change)
        self.editors.model.itemChanged.connect(self.handle_edit)

        self.button_row.reset.clicked.connect(self.handle_reset)
        self.button_row.cancel.clicked.connect(self.handle_cancel)
        self.button_row.apply_.clicked.connect(self.handle_apply)

        ### MAIN LAYOUT ###
        layout = QVBoxLayout()
        layout.addWidget(header)
        layout.addWidget(self.editors)
        layout.addLayout(self.button_row)
        self.setLayout(layout)
        [widget.hide() for widget in [self.seed.label, self.seed.editor]]

        self.saved_state = None
        self.init_model()
        self.setScene(scene)

    ### INITIALIZER ### 

    def init_model(self):
        first_key = self.editor_map.keys()[0]
        self.current_parameter = first_key

    ### PROPERTIES ###
    
    @property
    def current_parameter(self):
        return self._current_parameter

    @current_parameter.setter
    def current_parameter(self, parameter):
        self._current_parameter = parameter
        self.update_editors(self._current_parameter)

    @property
    def current_property(self):
        return self.editor_map[self.current_parameter].property_

    @property
    def current_datatype(self):
        return self.editor_map[self.current_parameter].datatype

    @property
    def seed_value(self):
        return self._seed_value

    @seed_value.setter
    def seed_value(self, value):
        self._seed_value = value

    ### HANDLERS ### 

    def handle_set_order(self):
        order = str(self.order.editor.currentText())
        if order == 'Random':
            [widget.show() for widget in [self.seed.label, self.seed.editor]]
        else:
            [widget.hide() for widget in [self.seed.label, self.seed.editor]]
        self.handle_edit()

    def handle_set_seed(self):
        self.seed_value = str(self.seed.editor.value())
        self.handle_edit()

    def handle_reset(self):
        self.load_saved_state()
        self.handle_set_editor()

    def handle_cancel(self):
        self.load_saved_state()
        self.close()

    def handle_apply(self):
        self.save_state()
        self.handle_set_editor()

    def handle_edit(self):
        order = str(self.order.editor.currentText())
        if order == "Random":
            self.set_properties_randomly()
        else:
            self.set_properties_sequentially()

    def handle_item_count_change(self, count):
        self.set_state_counter(count)

    def handle_counter_change(self, value):
        self.load_saved_state()
        self.editors.update_count(value)
        self.set_properties_sequentially()

    def handle_set_editor(self):
        self.current_parameter = str(self.parameters.editor.currentText())
        self.save_state()
        self.update_editors(self.current_parameter)

    ### PUBLIC ###

    def setScene(self, scene):
        self.scene = scene
        self.save_state()

    def set_state_counter(self, value=0):
        self.state_counter.editor.setValue(value)

    def save_state(self):
        from scene_items.textitem import TextItemAnchor
        items = []
        for item in self.scene.items():
            if item.isSelected() and isinstance(item, self.scene.item_classes):
                items.append(item)
        self.saved_state = SaveState(items, self.current_property)

    def load_saved_state(self):
        if self.saved_state:
            for tpl in self.saved_state.item_value_tuples:
                setattr(tpl[0],
                        self.saved_state.property_,
                        tpl[1])

    def update_editors(self, parameter):
        self.editors.set_editor_type(parameter, self.editor_map[parameter].editor)
        self.editors.update_count(1)

    def get_seed_value(self):
        return self.seed.editor.value()

    def get_values(self):
        values = self.editors.get_values()
        conversion_fn = self.conversions[self.current_datatype]
        result = []
        for value in values:
            if value:
                result.append(conversion_fn(value))
            else:
                result.append(None)
        return result

    def set_properties_sequentially(self):
        from utils import rotate
        values = self.get_values()
        if self.saved_state:
            for item in self.saved_state.items():
                if values[0]:
                    setattr(item, self.current_property, values[0])
                values = rotate(values)

    def set_properties_randomly(self):
        import random
        values = self.get_values()
        random.seed(self.seed_value)
        if self.saved_state:
            for item in self.saved_state.items():
                value = random.choice(values)
                if value:
                    setattr(item, self.current_property, value)
    

