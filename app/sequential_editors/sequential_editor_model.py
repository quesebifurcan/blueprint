from PyQt4.QtGui import *
from PyQt4.QtCore import *

class SequentialEditorModel(QStandardItemModel):

    def __init__(self, scene=None, parent=None):
        super(SequentialEditorModel, self).__init__(parent)
        self.scene = scene
        self.setColumnCount(2)
        self.setHorizontalHeaderLabels(["Property", "Value"])

