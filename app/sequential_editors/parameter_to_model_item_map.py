from PyQt4.QtGui import *
from PyQt4.QtCore import *

from model_items import (make_colorselect_item,
                         make_number_item,
                         make_font_item,
                         make_linewidth_item,
                         make_textedit_item,
                         make_linestyle_item)

class EditorMapping(object):

    def __init__(self, description, property_, editor, datatype=int):
        self.description=description
        self.property_=property_
        self.editor=editor
        self.datatype=datatype

def parameter_to_model_item_map():
    parameters = {
        'X-pos': EditorMapping('X position', 'x', make_number_item, int),
        'Delta-X': EditorMapping('X delta', 'dx', make_number_item, int),
        'Delta-Y': EditorMapping('Y delta', 'dy', make_number_item),
        'Text': EditorMapping('Text', 'text', make_textedit_item, str),
        'Color': EditorMapping('Color', 'color', make_colorselect_item, QColor),
        'Line-width': EditorMapping('Line width', 'linewidth', make_linewidth_item, int),
        'Line-style': EditorMapping('Line style', 'linestyle', make_linestyle_item),
        'Font': EditorMapping('Font', 'fontfamily', make_font_item)}
    return parameters

def model_item_data_conversions():
    return {
        int: lambda v: v.toInt()[0],
        float: lambda v: v.toFloat()[0],
        QColor: lambda v: QColor(v),
        str: lambda v: v.toString()
        }

def transitionable_properties():
    return ['X-pos',
            'Color',
            'Line-width',
            'Delta-X',
            'Delta-Y']
