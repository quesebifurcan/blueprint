from PyQt4.QtGui import *
from PyQt4.QtCore import *

class EditorGroupBox(QGroupBox):

    def __init__(self, parent=None):
        super(EditorGroupBox, self).__init__(parent)
        self.setStyleSheet("QGroupBox {border: 1px solid gray}")
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignTop)
        self.setLayout(layout)
