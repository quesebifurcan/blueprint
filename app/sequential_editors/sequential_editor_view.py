import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from properties_editor_delegate import Delegate

import model_items


class SequentialEditorView(QTreeView):

    editorCountChanged = pyqtSignal(int)

    def __init__(self, model=None, parent=None):
        super(SequentialEditorView, self).__init__(parent)
        self.model = model
        self.setModel(self.model)

        self.current_property = None
        self.current_editor = None

        font = self.font()
        font.setPointSize(11)
        self.setFont(font)

        self.setAlternatingRowColors(True)
        self.setUniformRowHeights(True)
        self.setItemDelegate(Delegate(self.model, self)) 

        self.expandAll()
        self.resizeColumnToContents(0)

    ### PUBLIC ###

    def set_editor_type(self, property_, editor):
        self.current_property = property_
        self.current_editor = editor
        self._reset()
        self.update_count(1)

    def update_count(self, count):
        curr_count = self.model.rowCount()

        if count > curr_count:
            self._add_editors(count)
        elif count < curr_count:
            self._remove_editors(count)

        self.expandAll()
        self.resizeColumnToContents(0)
        self.editorCountChanged.emit(self.model.rowCount())

    def get_values(self):
        result = []
        for row in range(self.model.rowCount()):
            item = self.model.item(row, 1)
            if item.dirty:
                result.append(item.data())
            else:
                result.append(None)
        return result

    ### OVERRIDDEN ###

    def currentChanged(self, curr, prev):
        item = self.model.itemFromIndex(curr)
        if hasattr(item, 'datatype'):
            if item.datatype == "color":
                item.editor.setFocus(True)
        self.selectionModel().setCurrentIndex(curr, QItemSelectionModel.NoUpdate);
        self.scrollTo(curr)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Plus:
            self.update_count(self.model.rowCount() + 1)
        elif e.key() == Qt.Key_Minus:
            self.update_count(self.model.rowCount() - 1)
        else:
            super(SequentialEditorView, self).keyPressEvent(e)

    ### PRIVATE ###

    def _new_editor_item(self, name, fn, persistent=True):
        from model_items import new_labeled_row

        label, item = new_labeled_row(name)
        fn(item)
        self.model.appendRow([label, item])
        if persistent:
            self.openPersistentEditor(self.model.indexFromItem(item))
        return item

    def _reset(self):
        count = self.model.rowCount()
        self.model.removeRows(0, count)

    def _add_editors(self, count):
        while count > self.model.rowCount():
            index = self.model.rowCount() + 1
            self._new_editor_item(
                name=' '.join([self.current_property, str(index)]),
                fn=self.current_editor)

    def _remove_editors(self, count):
        while count < self.model.rowCount():
            index = self.model.rowCount() - 1
            self.model.removeRow(index)

