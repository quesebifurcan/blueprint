from PyQt4.QtGui import *
from PyQt4.QtCore import *

class TransitionEditorModel(QStandardItemModel):

    def __init__(self, scene=None, parent=None):
        super(TransitionEditorModel, self).__init__(parent)
        self.scene = scene
        self.setColumnCount(2)
        self.setRowCount(2)
        self.setHorizontalHeaderLabels(["", "Value"])

