from PyQt4.QtGui import *
from PyQt4.QtCore import *

from delegate_editor_widgets import (FontBox,
                                     LineStyleComboBox,
                                     ColorEditorWidget)


class Delegate(QStyledItemDelegate):

    def __init__(self, model=None, view=None, parent=None):
        super(Delegate, self).__init__(parent)
        self.model = model

    def get_item(self, index):
        return self.model.itemFromIndex(index)

    ### OVERRIDDEN ###

    def createEditor(self, parent, option, index):
        item = self.get_item(index)

        if item.datatype == "font":
            editor = FontBox(parent)
            editor.setCurrentFont(QFont(item.data()))
            return editor

        if item.datatype == "text":
            editor = QLineEdit(parent)
            # editor.setCurrentFont(QFont(item.data()))
            return editor

        elif item.datatype == "color":
            editor = ColorEditorWidget(parent)
            editor.set_item(item)
            return editor

        elif item.datatype == "int":
            editor = QSpinBox(parent)
            editor.setMaximum(100000000)
            editor.setMinimum(0)
            editor.setFrame(True)
            return editor

        elif item.datatype == "linestyle":
            editor = LineStyleComboBox(parent)
            return editor

        elif item.datatype == "linewidth":
            editor = QSpinBox(parent)
            editor.setMaximum(20)
            editor.setMinimum(1)
            editor.setFrame(True)
            return editor

        else:
            return super(Delegate, self).createEditor(parent, option, index)

    def setEditorData(self, editor, index): 
        item = self.get_item(index)
        if item.datatype == "font":
            editor.setCurrentFont(QFont(item.data()))
        else:
            super(Delegate, self).setEditorData(editor, index)

    def setModelData(self, editor, model, index):
        item = self.get_item(index)

        if item.datatype == "font":
            item.setData(editor.currentFont().family())

        elif item.datatype == "check":
            super(Delegate, self).setModelData(editor, model, index)
            item.setData(item.checkState())

        elif item.datatype == 'text':
            super(Delegate, self).setModelData(editor, model, index)
            item.setData(item.text())

        elif item.datatype == "color":
            return

        elif item.datatype in ["linestyle", "linewidth"]: 
            if item.datatype == "linestyle":
                item.setData(editor.currentText())

        else:
            super(Delegate, self).setModelData(editor, model, index)
            item.setData(editor.value())

