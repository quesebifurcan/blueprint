from PyQt4.QtGui import *
from PyQt4.QtCore import *


class FontBox(QFontComboBox):

    def __init__(self, parent=None):
        super(FontBox, self).__init__(parent)
        self.setStyleSheet("QComboBox { combobox-popup: 0; }");
        self.setEditable(False)
        self.setMaxVisibleItems(10)
        self.setFrame(True)

        font = self.font()
        font.setPointSize(11)
        self.setFont(font)

        self.edit_mode = False
        self.setFrame(True)


class LineStyleComboBox(QComboBox):

    def __init__(self, parent=None):
        super(LineStyleComboBox, self).__init__(parent)
        self.setEditable(False)

        styles = {'solid': Qt.SolidLine,
                  'dashed': Qt.DashLine,
                  'dotted': Qt.DotLine,
                  'dash-dot': Qt.DashDotLine,
                  'dash-dot-dot': Qt.DashDotDotLine}

        for style in styles.keys():
            self.addItem(style) 

        font = self.font()
        font.setPointSize(11)
        self.setFont(font)


class ClickableLabel(QLabel):

    clicked = pyqtSignal()

    def __init__(self, parent=None):
        super(ClickableLabel, self).__init__(parent)
        self.setMaximumWidth(30)
        self.setMinimumWidth(30)
        self.setStyleSheet("border: 1px solid black; background-color: white;")

    def mousePressEvent(self, e):
        self.clicked.emit()


class ColorEditorWidget(QWidget):

    def __init__(self, parent=None):
        super(ColorEditorWidget, self).__init__(parent)

        ### LAYOUT ###
        layout = QHBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        self.setContentsMargins(1, 1, 1, 1)

        self.label = ClickableLabel()
        space = QLabel()
        layout.addWidget(space)
        layout.addWidget(self.label)
        self.setLayout(layout) 
        self.dirty = False

        ### SIGNALS ###
        self.label.clicked.connect(self.show_dialog)

    def set_item(self, item):
        self.item = item
        self.item.editor = self
        self.item.setData(QColor('white'))
        item.setText(item.data().toString())

    def show_dialog(self):
        self.dialog = QColorDialog()
        self.dialog.setCurrentColor(QColor(self.item.data()))
        self.dialog.setOption(QColorDialog.ShowAlphaChannel)
        self.dialog.currentColorChanged.connect(self.set_color)
        self.dialog.colorSelected.connect(self.set_color)
        self.dialog.exec_()
        del self.dialog

    def set_color(self, color):
        s = "border: 1px solid black; background-color:" + color.name()
        self.label.setStyleSheet(s)
        self.item.setData(color)
        self.item.setText(self.item.data().toString())
        self.item.dirty = True

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Return:
            self.show_dialog()
        else:
            super(ColorEditorWidget, self).keyPressEvent(e)
