from PyQt4.QtGui import *
from PyQt4.QtCore import *


class CommandButtonRowLayout(QHBoxLayout):

    def __init__(self, parent=None):
        super(CommandButtonRowLayout, self).__init__(parent)

        self.reset = QPushButton("Reset")
        self.cancel = QPushButton("Cancel")
        self.apply_ = QPushButton("Apply")

        self.addWidget(self.reset)
        self.addWidget(self.cancel)
        default = self.apply_
        default.setDefault(True)
        self.addWidget(default)
