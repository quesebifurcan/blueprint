from PyQt4.QtGui import *
from PyQt4.QtCore import *

#------------------------------------------------------------------------------
# helper functions -- set Roles and datatypes of QStandardItems used in model
#------------------------------------------------------------------------------

def new_bold_label(name):
    label = QStandardItem(name)
    font = label.font()
    font.setWeight(QFont.Bold)
    label.setFont(font)
    label.setEditable(False)
    label.setTextAlignment(Qt.AlignCenter)
    return label

def new_labeled_row(name):
    label = new_bold_label(name)
    item = QStandardItem()
    item.dirty = False
    return label, item

def make_number_item(item, initial_data=0):
    item.datatype = "int"
    item.setData(initial_data, Qt.EditRole)
    return item

def make_font_item(item, initial_data="Lucida Grande"):
    item.datatype = "font"
    item.setData(initial_data, Qt.FontRole)
    return item

def make_checkbox_item(item, initial_data=0):
    item.datatype = "check"
    item.setEditable(False)
    item.setCheckable(True)
    item.setData(initial_data, Qt.CheckStateRole)
    return item

def make_linestyle_item(item, initial_data='one'):
    item.datatype = "linestyle"
    item.setEditable(False)
    item.setData(initial_data, Qt.EditRole)
    return item

def make_linewidth_item(item, initial_data=1):
    item.datatype = "linewidth"
    item.setEditable(False)
    item.setData(initial_data, Qt.EditRole)
    return item

def make_textedit_item(item, initial_data=1):
    item.datatype = "text"
    item.setEditable(False)
    item.setData(initial_data, Qt.EditRole)
    return item

def make_colorselect_item(item, initial_data=QColor('white')):
    item.datatype = "color"
    item.setData(initial_data, Qt.EditRole)
    font = item.font()
    font.setStyle(QFont.StyleItalic)
    item.setFont(font)
    return item



        

