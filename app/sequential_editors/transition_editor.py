from PyQt4.QtGui import *
from PyQt4.QtCore import *

from transition_editor_view import TransitionEditorView
from transition_editor_model import TransitionEditorModel

from parameter_to_model_item_map import (parameter_to_model_item_map,
                                         transitionable_properties,
                                         model_item_data_conversions)

from editor_group_box import EditorGroupBox
from command_button_row_layout import CommandButtonRowLayout
from header_widgets import (new_parameter_selector,
                            new_state_counter,
                            add_editor_tuples_to_layout)

class SaveState(object):

    def __init__(self, items, property_):
        self.property_ = property_
        self.current_order = lambda item: item.scenePos().x()
        self.item_value_tuples = []
        self.get_values(items)

    def sorted_items(self, items):
        return sorted(items, key=self.current_order)

    def get_values(self, items):
        for item in self.sorted_items(items):
            if hasattr(item, self.property_):
                self.item_value_tuples.append(
                    (item, getattr(item, self.property_)))

    def items(self):
        return [tpl[0] for tpl in self.item_value_tuples]


class TransitionEditorDialog(QDialog):

    editor_map = parameter_to_model_item_map()
    conversions = model_item_data_conversions()

    def __init__(self, scene=None, parent=None):
        super(TransitionEditorDialog, self).__init__(parent)
        self.setWindowTitle("Transition Editor")
        self.setModal(False)
        self.setFocusPolicy(Qt.StrongFocus)
        self.setMaximumHeight(250)
        self.scene = None

        ### HEADER ###
        header = EditorGroupBox()

        self.parameters = new_parameter_selector(
            "Parameter", transitionable_properties())
        add_editor_tuples_to_layout(header.layout(), [self.parameters, ])

        ### DATA ###
        self.editors = TransitionEditorView(model=TransitionEditorModel())

        ### APPLY / CANCEL ###
        self.button_row = CommandButtonRowLayout()

        ### SIGNALS ###
        self.button_row.reset.clicked.connect(self.handle_reset)
        self.button_row.cancel.clicked.connect(self.handle_cancel)
        self.button_row.apply_.clicked.connect(self.handle_apply)
        self.parameters.editor.currentIndexChanged.connect(self.handle_set_editor)

        self.editors.model.itemChanged.connect(self.handle_edit)

        ### MAIN LAYOUT ###
        layout = QVBoxLayout()
        layout.addWidget(header)
        layout.addWidget(self.editors)
        layout.addLayout(self.button_row)
        self.setLayout(layout)

        self.init_model()
        self.setScene(scene)

    def init_model(self):
        first_key = self.editor_map.keys()[0]
        self.current_parameter = first_key

    ### PROPERTIES ###
    
    @property
    def current_parameter(self):
        return self._current_parameter

    @current_parameter.setter
    def current_parameter(self, parameter):
        self._current_parameter = parameter
        self.update_editors(self._current_parameter)

    @property
    def current_property(self):
        return self.editor_map[self.current_parameter].property_

    @property
    def current_datatype(self):
        return self.editor_map[self.current_parameter].datatype

    ### HANDLERS ### 

    def handle_reset(self):
        self.load_saved_state()
        self.init_model()
        self.handle_set_editor()

    def handle_cancel(self):
        self.load_saved_state()
        self.close()

    def handle_apply(self):
        self.handle_set_editor()

    def handle_edit(self):
        self.set_properties_sequentially()

    def handle_set_editor(self):
        self.current_parameter = str(self.parameters.editor.currentText())
        self.save_state()
        self.update_editors(self.current_parameter)

    ### PUBLIC ###

    def setScene(self, scene):
        self.scene = scene
        self.save_state()

    def save_state(self):
        items = []
        for item in self.scene.items():
            if item.isSelected() and isinstance(item, self.scene.item_classes):
                items.append(item)
        self.saved_state = SaveState(items, self.current_property)

    def load_saved_state(self):
        if self.saved_state:
            for tpl in self.saved_state.item_value_tuples:
                setattr(tpl[0],
                        self.saved_state.property_,
                        tpl[1])

    def update_editors(self, parameter):
        self.editors.set_editor_type(parameter, self.editor_map[parameter].editor)
        self.editors.init_data()

    def get_values(self):
        from utils import interpolate_colors
        item_count = len(self.saved_state.items())
        values = self.editors.get_values()
        if len(values) >= 2 and any(values):
            conversion_fn = self.conversions[self.current_datatype]
            start, end = [conversion_fn(value) for value in values]
            return interpolate_colors(start, end, item_count)

    def set_properties_sequentially(self):
        from utils import rotate
        values = self.get_values()
        if values and self.saved_state:
            for item in self.saved_state.items():
                if values[0]:
                    setattr(item, self.current_property, values[0])
                values = rotate(values)
            return self.saved_state.items()

    

